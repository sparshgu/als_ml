module fa (a, b, c, d, e , f, G);

         input a,b,c,d,e,f;
         output G;

         assign G = (a & b & !c & !d)|(a & b & !e & !f)|(!a & !b & c & d)|(!a & !b & e & f)|(c & d & !e & !f)|(!c & !d & e & f);

endmodule
