module fa (a, b, c, d, e , f, G);

         input a,b,c,d,e,f;
         output G;

         //assign G = (a & b)|(b & !e & !f)|(!a & !b)|(e & !a & !b )|(c & d)|(f & !c & !d);
         assign G = (c|!a|!b)&(f|e|!b)&(!d|!c|b)&(!e|a|b)&(e|!c|!d)&(!f|!e|c);
endmodule
