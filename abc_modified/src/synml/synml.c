/**CFile****************************************************************

 FileName    []

 SystemName  [ABC: Logic synthesis and verification system.]

 PackageName []

 Synopsis    []

 Author      []

 Affiliation []

 Date        []

 Revision    []

 ***********************************************************************/

#include "base/main/main.h"
#include "base/abc/abc.h"
#include "bool/dec/dec.h"
#include "bool/kit/kit.h"
#include "base/io/ioAbc.h"
#include "misc/util/utilSignal.h"
#ifdef WIN32
#include <process.h>
#else
#include <unistd.h>
#endif
ABC_NAMESPACE_IMPL_START

////////////////////////////////////////////////////////////////////////
///                        DECLARATIONS                              ///
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
///                     FUNCTION DEFINITIONS                         ///
////////////////////////////////////////////////////////////////////////

/**Function*************************************************************

 Synopsis    []

 Description []

 SideEffects []

 SeeAlso     []

 ***********************************************************************/
int TestFirst_SecondFunctionAbc(Abc_Ntk_t * pNtk, int errorThreshold) {
	Abc_AigCleanup((Abc_Aig_t *) pNtk->pManFunc);
	int i, nNodes;
	Abc_Obj_t * pNode;
	ProgressBar * pProgress;
	abctime clk, clkStart = Abc_Clock();
	Abc_ManCut_t * pManCut;
	pManCut = Abc_NtkManCutStart(10, 16, 2, 1000);
	nNodes = Abc_NtkObjNumMax(pNtk);
	pProgress = Extra_ProgressBarStart( stdout, nNodes);
	Abc_NtkForEachNode( pNtk, pNode, i )
	{
		Extra_ProgressBarUpdate(pProgress, i, NULL);
		// skip persistant nodes
		if (Abc_NodeIsPersistant(pNode))
			continue;
		// skip the nodes with many fanouts
		if (Abc_ObjFanoutNum(pNode) > 1000)
			continue;
		// stop if all nodes have been tried once
		if (i >= nNodes)
			break;
		clk = Abc_Clock();

	}
}
int TestFirst_ThirdFunctionAbc(Abc_Ntk_t * pNtk, int fVerbose,
		int errorThreshold) {
	char* fileName = "temp.blif";
	Io_Write(pNtk, fileName, IO_FILE_BLIF);
	char cwd[1024];
	if (getcwd(cwd, sizeof(cwd)) != NULL) {
		fprintf(stdout, "Current working dir: %s\n", cwd);
		char Command[10000];
	}
}

/**Function*************************************************************

 Synopsis    []

 Description []

 SideEffects []

 SeeAlso     []

 ***********************************************************************/
int TestFirst_FirstFunctionAbc(Abc_Ntk_t * pNtk, int nNodeSizeMax,
		int nConeSizeMax, int fUpdateLevel, int fUseZeros, int fUseDcs,
		int fVerbose, int errorThreshold) {
			int result;
			char* fileName = "temp.blif";
		Io_Write(pNtk, fileName, IO_FILE_BLIF);
		char cwd[1024];
		if (getcwd(cwd, sizeof(cwd)) != NULL) {
			fprintf(stdout, "Current working dir: %s\n", cwd);
			char Command[10000];
			sprintf(Command, "make -C %s/../CppLayer clean", cwd);
			char buf[128];
			FILE *fp;
			if ((fp = popen(Command, "r")) == NULL) {
				printf("Error opening pipe!\n");
				return -1;
			}
			while (fgets(buf, 128, fp) != NULL) {
				// Do whatever you want here...
				printf("OUTPUT: %s", buf);
			}

			sprintf(Command, "make -C %s/../CppLayer main", cwd);
			if ((fp = popen(Command, "r")) == NULL) {
				printf("Error opening pipe!\n");
				return -1;
			}
			while (fgets(buf, 128, fp) != NULL) {
				// Do whatever you want here...
				printf("OUTPUT: %s", buf);
			}

			sprintf(Command, "%s/../CppLayer/main", cwd);
			fp = popen(1,"w");
			if ((fp = popen(Command, "r")) == NULL) {
				printf("Error opening pipe!\n");
				return -1;
			}
			while (fgets(buf, 128, fp) != NULL) {
				// Do whatever you want here...
				printf("OUTPUT: %s", buf);
			}

			//int result = Util_SignalSystem(Command);
		} else {
			Abc_Print(2, "getcwd() error");
			return -1;
		}

	return result;

}
////////////////////////////////////////////////////////////////////////
///                       END OF FILE                                ///
////////////////////////////////////////////////////////////////////////

ABC_NAMESPACE_IMPL_END
