/**CFile****************************************************************

 FileName    []

 SystemName  [ABC: Logic synthesis and verification system.]

 PackageName []

 Synopsis    []

 Author      []

 Affiliation []

 Date        []

 Revision    []

 ***********************************************************************/
#include "base/main/main.h"
#include "synml.h"
ABC_NAMESPACE_IMPL_START

////////////////////////////////////////////////////////////////////////
///                        DECLARATIONS                              ///
////////////////////////////////////////////////////////////////////////
static int TestFirst_CommandTestFirst(Abc_Frame_t *pAbc, int argc, int** argv);

/**Function*************************************************************

 Synopsis    []

 Description []

 SideEffects []

 SeeAlso     []

 ***********************************************************************/

////////////////////////////////////////////////////////////////////////
///                     FUNCTION DEFINITIONS                         ///
////////////////////////////////////////////////////////////////////////
void TestFirst_Int(Abc_Frame_t *pAbc) {
	Cmd_CommandAdd(pAbc, "Various", "alsSub", TestFirst_CommandTestFirst, 0);
}

static int TestFirst_CommandTestFirst(Abc_Frame_t *pAbc, int argc, int** argv) {
    Abc_Ntk_t * pNtk = Abc_FrameReadNtk(pAbc);
    int c;
    int nNodeSizeMax;
    int nConeSizeMax;
    int fUpdateLevel;
    int fUseZeros;
    int fUseDcs;
    int fVerbose;
    int errorThreshold;
    extern int Abc_NtkRefactor( Abc_Ntk_t * pNtk, int nNodeSizeMax, int nConeSizeMax, int fUpdateLevel, int fUseZeros, int fUseDcs, int fVerbose );

    // set defaults
    nNodeSizeMax = 10;
    nConeSizeMax = 16;
    fUpdateLevel =  1;
    fUseZeros    =  0;
    fUseDcs      =  0;
    fVerbose     =  0;
    errorThreshold = 20;
    Extra_UtilGetoptReset();
    while ( ( c = Extra_UtilGetopt( argc, argv, "Nlzvhe" ) ) != EOF )
    {
        switch ( c )
        {
        case 'N':
            if ( globalUtilOptind >= argc )
            {
                Abc_Print( -1, "Command line switch \"-N\" should be followed by an integer.\n" );
                goto usage;
            }
            nNodeSizeMax = atoi(argv[globalUtilOptind]);
            globalUtilOptind++;
            if ( nNodeSizeMax < 0 )
                goto usage;
            break;
        case 'C':
            if ( globalUtilOptind >= argc )
            {
                Abc_Print( -1, "Command line switch \"-C\" should be followed by an integer.\n" );
                goto usage;
            }
            nConeSizeMax = atoi(argv[globalUtilOptind]);
            globalUtilOptind++;
            if ( nConeSizeMax < 0 )
                goto usage;
            break;
        case 'l':
            fUpdateLevel ^= 1;
            break;
        case 'z':
            fUseZeros ^= 1;
            break;
        case 'd':
            fUseDcs ^= 1;
            break;
        case 'v':
            fVerbose ^= 1;
            break;
        case 'h':
            goto usage;
        case 'e':
        	if ( globalUtilOptind >= argc )
        	            {
        	                Abc_Print( -1, "Command line switch \"-e\" should be followed by an integer.\n" );
        	                goto usage;
        	            }
        	            errorThreshold = atoi(argv[globalUtilOptind]);
        	            globalUtilOptind++;
        	            if ( errorThreshold > 100)
        	                goto usage;
        	            break;
        default:
            goto usage;
        }
    }
	if (pNtk == NULL) {
		Abc_Print(-1, "Empty network.\n");
		return 1;
	}
	if (fUseDcs && nNodeSizeMax >= nConeSizeMax) {
		Abc_Print(-1,
				"For don't-care to work, containing cone should be larger than collapsed node.\n");
		return 1;
	}

	int result = TestFirst_FirstFunctionAbc(pNtk, nNodeSizeMax, nConeSizeMax,
			fUpdateLevel, fUseZeros, fUseDcs, fVerbose, errorThreshold);
	if (fVerbose) {
		Abc_Print(2, "Verbose mode is on.\n");
		if (result)
			Abc_Print(1, "Execution successful.\n");
		else
			Abc_Print(-1, "Execution failed.\n");

	}
	return 0;
	usage: Abc_Print(2, "usage: \n");
	return 1; /* error exit */

}

////////////////////////////////////////////////////////////////////////
///                       END OF FILE                                ///
////////////////////////////////////////////////////////////////////////

ABC_NAMESPACE_IMPL_END
