import numpy as rr
def reward_function(n,coeffList):
    p=10
    counter=0
    d=int(n*p)
    R=rr.matrix(rr.zeros([d,d]))
    for x in range(0,d):
        for y in range (0,d):
            if(x>y):
                R[x,y]=0
            else:
                R[x,y]=-1
########################################################################################
    for pt in range(0,p*n-p,p):
        for x in range(pt,pt+p):
            for y in range(pt,pt+p):
                if(x<y):
                    R[x,y]=0
        for x in range(pt,pt+p):
            for y in range(pt+p,pt+2*p):
                if y==pt+p:
                    R[x,y]=(coeffList[y % (pt+p)])*int(2**counter)
                if y==pt+p+1:
                    R[x,y]=(coeffList[y % (pt+p)])*int(2**counter)
                if y==pt+p+2:
                    R[x,y]=(coeffList[y % (pt+p)])*int(2**counter)
                if y==pt+p+3:
                    R[x,y]=(coeffList[y % (pt+p)])*int(2**counter)
                if y==pt+p+4:
                    R[x,y]=(coeffList[y % (pt+p)])*int(2**counter)
                if y==pt+p+5:
                    R[x,y]=(coeffList[y % (pt+p)])*int(2**counter)
                if y==pt+p+6:
                    R[x,y]=(coeffList[y % (pt+p)])*int(2**counter)
                if y==pt+p+7:
                    R[x,y]=(coeffList[y % (pt+p)])*int(2**counter)
                if y==pt+p+8:
                    R[x,y]=(coeffList[y % (pt+p)])*int(2**counter)
                if y==pt+p+9:
                    R[x,y]=(coeffList[y % (pt+p)])*int(2**counter)
        counter=counter+1
################################################################################################
    for x in range (pt+p,pt+2*p):
        for y in range (pt+p,pt+2*p):
            if x<y:
                R[x,y]=0

    for x in range (0,d):
        for y in range (0,d):
            if (x==d-1 and y==d-1):
                R[x,y]=(100)*int(2**(counter-1))
            elif (x==d-2 and y==d-2):
                R[x,y]=(200)*int(2**(counter-1))
            elif (x==d-3 and y==d-3):
                R[x,y]=(300)*int(2**(counter-1))
            elif (x==d-4 and y==d-4):
                R[x,y]=(400)*int(2**(counter-1))
            elif (x==d-5 and y==d-5):
                R[x,y]=(500)*int(2**(counter-1))
            elif (x==d-6 and y==d-6):
                R[x,y]=(600)*int(2**(counter-1))
            elif (x==d-7 and y==d-7):
                R[x,y]=(700)*int(2**(counter-1))
            elif (x==d-8 and y==d-8):
                R[x,y]=(800)*int(2**(counter-1))
            elif (x==d-9 and y==d-9):
                R[x,y]=(900)*int(2**(counter-1))
            elif (x==d-10 and y==d-10):
                R[x,y]=(1000)*int(2**(counter-1))
    return R
