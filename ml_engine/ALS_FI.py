import numpy as pp
from reward_using_pt import reward_function

R=None
Q=None
IterationCount = 100000
defaultCoeffList = range(4000,0,-400)
def getSelectedStates(subExpessionSize,maxSubStateSize,coeffList=None):
    global R
    if (coeffList):
        R = reward_function(subExpessionSize,coeffList)
    else:
        R = reward_function(subExpessionSize,defaultCoeffList)
    print(R)
    #####################################################SIZE can be different as per factored forms###############################################################
    global Q
    Q = pp.matrix(pp.zeros([(subExpessionSize*maxSubStateSize),(subExpessionSize*maxSubStateSize)]))
    print(Q)
    gamma = 0.90
    ########################################################################################################################
    current_state = initial_state = 1
    current_state_row = R[initial_state,]
    print(current_state_row)
    available_act = available_actions(initial_state)
    print(available_act)
    action = sample_next_action(available_act)
    print(action)
    update(initial_state,action,gamma)
    for pt in range(IterationCount):
        current_state =pp.random.randint(0, int(Q.shape[0]))
        available_act = available_actions(current_state)
        action = sample_next_action(available_act)
        update(current_state, action, gamma)
    print("Obtained Q matrix after training:")
    print(Q / pp.max(Q) * 100)
    current_state = 0
    steps = [current_state]
    print(steps)
    terminationStateList = range((subExpessionSize*maxSubStateSize)-maxSubStateSize,(subExpessionSize*maxSubStateSize))
    # while (current_state!=15 and current_state!=16 and current_state!=17 ):
    while (not(current_state in terminationStateList)):
        # print(current_state)
        next_step_index = pp.where(Q[current_state,] == pp.max(Q[current_state,]))[1]
        if next_step_index.shape[0] > 1:
            next_step_index = int(pp.random.choice(next_step_index, size=1))
        else:
            next_step_index = int(next_step_index)
        steps.append(next_step_index)
        current_state = next_step_index
    print("Path for solving the current problem :")
    print(steps)
    return steps

def available_actions(state):
    current_state_row = R[state,]
    print(current_state_row)
    av_act = pp.where(current_state_row >= 0)[1]
    return av_act
########################################################################################################################3
def sample_next_action(available_actions_range):
    next_action = int(pp.random.choice(available_actions_range,1)) #gives 1 random op from 1-d array
    print(next_action)
    return next_action
##########################################################################################################################
def update(current_state,action, gamma):
    max_index = pp.where(Q[action,] == pp.max(Q[action,]))[1]
    print(max_index)
    if max_index.shape[0] > 1:
        max_index = int(pp.random.choice(max_index, size=1))
    else:
        max_index = int(max_index)
    max_value = Q[action, max_index]
    print(max_value)
    Q[current_state, action] = R[current_state, action] + gamma * max_value
