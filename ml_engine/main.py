import blifReader
import sys
import os
from project import factorofexp
from project import factorofCNFexp
from collections import OrderedDict
import numpy as np
import boolean
from ALS_FI import *
from tt import BooleanExpression
import truths

maxSubStateSize = 10
maxGlobalErrorThreshold = 30
def pruneIfGreaterThanErrorThreshold(expressionList,errorThreshold):
    return filter(lambda x: x[1].errorPercentage < errorThreshold, expressionList)

def getFactoredFormWithMinError(circuitApproxStatesForAllOutputsDict):
    for key,val in circuitApproxStatesForAllOutputsDict.iteritems():
        print 'Output Net: ' + key
        for key1,val1 in val.iteritems():
            print 'SubExpression: ' + str(key1)
            for key2,val2 in val1.iteritems():
                if (val2):
                    val2 = val2[0]
                    print 'Minimum Error Expression ' + str(val2)
                val1[key2] = val2
            print '\n'

def calculateErrorPercentage(originalExpr,approxExpr):
    algebra = boolean.BooleanAlgebra()
    expr = algebra.parse(str(originalExpr))
    possibleLiteralExpressions = []
    possibleLiteralExpressions.append(approxExpr.strip())
    literals = []
    for symbol in BooleanExpression(originalExpr)._symbol_set:
        literals.append(symbol)
    possibleLiteralExpressions.append(originalExpr)
    AllLiteralsOutputValues = truths.Truths(literals,
            possibleLiteralExpressions, ints=True).outputValue()
    error = 0
    for (newBoolValue, baseBoolValue) in zip(AllLiteralsOutputValues[0],
            AllLiteralsOutputValues[-1]):
        if newBoolValue != baseBoolValue:
            error += 1
    return error / float(len(AllLiteralsOutputValues[-1]) + 1) * 100

def main(arguments):
    if len(arguments) != 2:
        print 'usage: main.py -b=<BLIF FilePath> -e=<errorThreshold>'
        sys.exit(1)
    if not '-b=' in arguments[0]:
        print 'usage: main.py -b=<BLIF FilePath> -e=<errorThreshold>'
        sys.exit(1)
    fileInputPath = (arguments[0])[3:]
    if not os.path.isfile(fileInputPath):
        print 'usage: main.py -b=<BLIF FilePath>'
        print "InputFile doesn't exist"
        sys.exit(1)
    if not '-e=' in arguments[1]:
        print 'usage: main.py -b=<BLIF FilePath> -e=<errorThreshold>'
        sys.exit(1)
    errorThreshold = int((arguments[1])[3:])
    fileInputPath = 'temp.blif'
    expressionDict,outputsExpressionInPIsDict = blifReader.parseBlif(fileInputPath)
    if (expressionDict):
        circuitApproxStatesForAllOutputs = OrderedDict()
        for key1,val in outputsExpressionInPIsDict.iteritems():
            circuitApproxStatesForOutput = OrderedDict()
            for expression in val:
                errorDict = factorofCNFexp(expression)
                for key,val in errorDict.iteritems():
                    errorDict[key] = pruneIfGreaterThanErrorThreshold(val,errorThreshold)
                if (len(errorDict)!=10):
                    for i in range(1,11-len(errorDict)):
                        errorDict[i*-1]=[]
                circuitApproxStatesForOutput[expression]= errorDict
            circuitApproxStatesForAllOutputs[key1] = circuitApproxStatesForOutput
        getFactoredFormWithMinError(circuitApproxStatesForAllOutputs)
        circuitApproximatedForAllOutputs = {}
        for outputNet,subExpressions in circuitApproxStatesForAllOutputs.iteritems():
            print '\n Done. Initializing Matrix for output:%s with bounds: [%dx10]x[%dx10]' %(outputNet,len(subExpressions),len(subExpressions))
            selectedStates =  getSelectedStates(len(subExpressions),maxSubStateSize)
            currentErrorRate = calculateErrorPercentage('(' + ') and ('.join([str(x) for x in outputsExpressionInPIsDict[outputNet]]) + ')',getApproximateCircuit(selectedStates,outputNet,subExpressions))
            itrCount = 2
            rotationFactor = 1
            while ((currentErrorRate>maxGlobalErrorThreshold) and (itrCount!=0)):
                newcoeffList = rotate(defaultCoeffList,rotationFactor)
                selectedStates =  getSelectedStates(len(subExpressions),maxSubStateSize,newcoeffList)
                approxCktExpr = getApproximateCircuit(selectedStates,outputNet,subExpressions)
                currentErrorRate = calculateErrorPercentage('(' + ') and ('.join([str(x) for x in outputsExpressionInPIsDict[outputNet]]) + ')',getApproximateCircuit(selectedStates,outputNet,subExpressions))
                itrCount -=1
                rotationFactor+=1
                circuitApproximatedForAllOutputs[outputNet] = approxCktExpr
            print '\n**Final data start***'
            for outputNet,approximatedCircuit in circuitApproximatedForAllOutputs.iteritems():
                print outputNet+'='+approximatedCircuit
            print '\n**Final data end***'
    else:
        print "Error in reading the file."

def rotate(l, n):
    return l[-n:] + l[:-n]

def strictly_increasing(L):
    return all(x<y for x, y in zip(L, L[1:]))

def getApproximateCircuit(selectedStates,outputNet,subExpressions):
    if (len(subExpressions) != len(selectedStates)):
        print "Problem in ML function."
        return
    if (not strictly_increasing(selectedStates)):
        print 'Selected states are incorrect.'
        return
    ApproximatedCircuit = {}
    circuitApproxStateForOutput = []
    i = 0
    for subExpression,approxFactoredForms in subExpressions.iteritems():
        print 'Selecting for subExpressions: ' + str(subExpression)
        index = selectedStates[i] % maxSubStateSize
        if (approxFactoredForms.items()[index][0]>=0):
            circuitApproxStateForOutput.append(approxFactoredForms.items()[index])
        else:
            print 'Selected sub state isn\'t possible, selecting one with 0 saving.'
            circuitApproxStateForOutput.append((0,approxFactoredForms[0]))
        i+=1
    temp = ') and ('.join([x[1][0] for x in circuitApproxStateForOutput])
    temp = '(' + temp + ')'
    return temp

if __name__ == '__main__':
    dir_path = os.path.dirname(os.path.realpath(__file__))
    egg_path='pyrtl.egg'
    sys.path.append(os.path.join(dir_path,egg_path))
    sys.exit(main(sys.argv[1:]))
