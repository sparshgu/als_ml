#!/usr/bin/python
# -*- coding: utf-8 -*-
import truths
import re
import boolean
import itertools
from tt import BooleanExpression
import collections
from collections import OrderedDict

LiteralSavingAndErrorInfo = collections.namedtuple('LiteralSavingAndErrorInfo', 'literalSaving errorPercentage')

def replace_str_index(text, index=0, replacement=''):
    return '%s%s%s' % (text[:index], replacement, text[index + 1:])

def compareOutputValuesAndReturnError(newLiteralValue,
        baseListeralValue):
    error = 0
    for (newBoolValue, baseBoolValue) in zip(newLiteralValue,
            baseListeralValue):
        if newBoolValue != baseBoolValue:
            error += 1
    return error / float(len(baseListeralValue) + 1) * 100

def powerset(L):
  pset = set()
  for n in xrange(len(L) + 1):
    for sset in itertools.combinations(L, n):
        if (sset):
            pset.add(sset)
  return pset

def factorofCNFexp(exp):
    splitSet = [x.strip() for x in (str(exp)).split('or')]
    possibleLiteralExpressions = []
    tempList = powerset(splitSet)
    for temp in tempList:
        try:
            possibleLiteralExpressions.append((' or '.join(temp)).strip())
        except Exception as e:
            print 'Ignoring possible sub-expression as not formatted ' + temp
    possibleLiteralExpressions.remove(str(exp).strip())
    algebra = boolean.BooleanAlgebra()
    expr = algebra.parse(str(exp))
    literals = []
    for symbol in exp._symbol_set:
        literals.append(symbol)
    possibleLiteralExpressions.append(str(exp).strip())
    AllLiteralsOutputValues = truths.Truths(literals,
            possibleLiteralExpressions, ints=True).outputValue()
    maxLiteralSavingFactoredForms = []
    factoredCNFApproximations = {}
    for (LiteralsOutputValue, expr) in zip(AllLiteralsOutputValues,
            possibleLiteralExpressions):
            errorPercentage = compareOutputValuesAndReturnError(LiteralsOutputValue,
                AllLiteralsOutputValues[-1])
            content = LiteralSavingAndErrorInfo(literalSaving=(len(literals)-len(algebra.parse(expr).symbols)),errorPercentage=errorPercentage)
            factoredCNFApproximations[expr] = content
    maxLiteralSavingFactoredForms = sorted(factoredCNFApproximations.items(), key=lambda x: x[1].literalSaving,reverse=True)
    groupOfLiteralSavings = set([x[1].literalSaving for x in factoredCNFApproximations.items()])
    groupOfLiteralSavingsList = []
    for literalSaving in groupOfLiteralSavings:
        groupOfLiteralSavingsList.append(literalSaving)
    groupOfLiteralSavingsList.reverse()
    factoredCNFApproximationsGroupedByliteralSaving = OrderedDict()
    for literalSaving in groupOfLiteralSavingsList:
        factoredCNFApproximationsGroupedByliteralSaving[literalSaving]=sorted(filter(lambda x: x[1].literalSaving == literalSaving, factoredCNFApproximations.items()), key=lambda x: x[1].errorPercentage)
    return factoredCNFApproximationsGroupedByliteralSaving

def factorofexp(exp):
    algebra = boolean.BooleanAlgebra()
    expressionString = exp
    expr = algebra.parse(expressionString)
    literals = [str(symbolVariable) for symbolVariable in
                list(expr.symbols)]
    #print truths.Truths(literals, [expressionString], ints=True)
    possibleLiteralExpressions = []
    for var in literals:
        indiciesOfVar = [m.start() for m in re.finditer(str(var),
                         expressionString)]
        for index in indiciesOfVar:
            index_h = index + len(var)
            index_l = index - 1
            flag = 0
            if (expressionString[index-1]=='(' and expressionString[index+ len(var)]==')'):
                index_h = index_h+1
                flag = 1
                if (index_h >= len(expressionString)):
                    index_h = index_h-1
                    index_l = index_l-4 #for not
                    flag=2
            operatorAtHigh = False
            operatorAtLow = False
            if (not(index_h>=len(expressionString))):
                if expressionString[index_h] == '+' \
                    or expressionString[index_h] == '*' \
                    or expressionString[index_h] == '^':
                    operatorAtHigh = True
            elif expressionString[index_l] == '+' \
                or expressionString[index_l] == '*' \
                or expressionString[index_l] == '^':
                operatorAtLow = True

         # new_str=replace_str_index(expressionString,index)

            if operatorAtHigh:
                new_str = expressionString[:index] \
                    + expressionString[index_h + 1:]

            # new_str=replace_str_index(new_str,index_h)

            if operatorAtLow:
                new_str = expressionString[:index_l] \
                    + expressionString[index + len(var):]
            if (not(operatorAtHigh or operatorAtLow)):
                ## TODO: Work on these problematic sub expressions.
                #print 'Problem with:' + expressionString
                continue
            if (flag==1):
                new_str = new_str[:index-4]+new_str[index:]
            if (flag==2):
                new_str = new_str[:-1]
                try:
                    eval(new_str)
                except Exception as e:
                    new_str=new_str+')'
            # new_str=replace_str_index(new_str,index_l)

            modified_expr = algebra.parse(new_str)
            new_str = new_str.replace('+', ' or ')
            new_str = new_str.replace('*', ' and ')
            possibleLiteralExpressions.append(new_str)
    #print possibleLiteralExpressions
    expressionString = expressionString.replace('+', ' or ')
    expressionString = expressionString.replace('*', ' and ')
    possibleLiteralExpressions.append(expressionString)
    import pdb; pdb.set_trace()
    #possibleLiteralExpressions = ['b and not(c) and not(d)','a and not(c) and not(d)','a and b and not(d)','a and b and not(c)','not(c) and not(d)','a and b','a and not(c)','b and not(d)','a and not(d)','b and not(c)','a and b and not(c) and not(d)']
    #print truths.Truths(literals, possibleLiteralExpressions, ints=True)
    #possibleLiteralExpressions = ['(a and b and not(c) and not(d))or(a and b and not(e) and not(f))or(not(a) and not(b) and c and d)or(not(a) and not(b) and e and f)or(c and d and not(e) and not(f))or(not(c) and not(d) and e and f)','(a and b) or (b and not(e) and not(f)) or (not(a) and not(b))or(e and not(a) and not(b))or(c and d)or(f and not(c) and not(d))']
    AllLiteralsOutputValues = truths.Truths(literals,
            possibleLiteralExpressions, ints=True).outputValue()


    AllLiteralsOutputValues = truths.Truths(literals,
            possibleLiteralExpressions, ints=True).outputValue()
    errorPercentages = {}
    for (LiteralsOutputValue, expr) in zip(AllLiteralsOutputValues,
            possibleLiteralExpressions):
        errorPercentages[expr] = \
            compareOutputValuesAndReturnError(LiteralsOutputValue,
                AllLiteralsOutputValues[-1])
    #print errorPercentages
    return errorPercentages
