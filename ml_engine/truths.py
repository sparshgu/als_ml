#!/usr/bin/env python
import itertools
from prettytable import PrettyTable
import re


class Gob(object):
    pass


class Truths(object):
    def __init__(self, base=None, phrases=None, ints=True):
        if not base:
            raise Exception('Base items are required')
        #base = [s + 'Modifier' for s in base]
        self.base = base
        self.phrases = phrases or []
        self.ints = ints

        # generate the sets of booleans for the bases
        self.base_conditions = list(itertools.product([False, True],
                                                      repeat=len(base)))

        # regex to match whole words defined in self.bases
        # used to add object context to variables in self.phrases
        self.p = re.compile(r'(?<!\w)(' + '|'.join(self.base) + ')(?!\w)')

    def calculate(self, *args):
        # store bases in an object context
        g = Gob()
        #self.base = [s.split('Modifier')[0] for s in self.base]
        for a, b in zip(self.base, args):
            setattr(g, a, b)

        # add object context to any base variables in self.phrases
        # then evaluate each
        eval_phrases = []
        for item in self.phrases:
            item = self.p.sub(r'g.\1', item)
            eval_phrases.append(eval(item))

        # add the bases and evaluated phrases to create a single row
        row = [getattr(g, b) for b in self.base] + eval_phrases
        if self.ints:
            return [int(item) for item in row]
        else:
            return row
    def outputValue(self):
        fieldNames=[]
        for index in range(len(self.base + self.phrases)):
            fieldNames.append('Column'+str(index))
        t = PrettyTable(fieldNames)
        for conditions_set in self.base_conditions:
            t.add_row(self.calculate(*conditions_set))
        outputValues = []
        index = len(self.base + self.phrases)-len(self.phrases)
        for item in self.phrases:
            temp = t.get_string(fields = [fieldNames[index]])
            r = re.compile('([01])')
            temp = [f for f in t.get_string(fields = [fieldNames[index]]).split('\n') if r.search(f)]
            outputValuesForExpression = []
            for val in temp:
                outputValuesForExpression.append(int(r.search(val).group(1)))
            outputValues.append(outputValuesForExpression)
            index +=1
        return outputValues
    def __str__(self):
        t = PrettyTable(self.base + self.phrases)
        for conditions_set in self.base_conditions:
            t.add_row(self.calculate(*conditions_set))
        return str(t)
