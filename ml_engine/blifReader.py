import os
import sys
from project import factorofexp
import boolean
from tt import to_cnf

inputs = []
outputs = []
def parseBlif(fileInputPath):
    f = open(fileInputPath, 'r')
    blif_string = None
    try:
        try:
            f = open(fileInputPath, 'r')
            import pyrtl
            from  pyrtl.pyrtlexceptions import PyrtlInternalError
            pyrtl.input_from_blif(f)
            io_vectors = pyrtl.working_block().wirevector_subset((pyrtl.Input, pyrtl.Output))
            for io_vector in io_vectors:
                temp = str(io_vector.__class__).split('.')[-1].split('>')[0].split("'")[0]
                if ('Input' == temp):
                    inputs.append(io_vector.name)
                elif ('Output' == temp):
                    outputs.append(io_vector.name)
            expressionDict = {}
            outputsExpressionInPIsDict={}
            for expr in pyrtl.working_block().expressions:
                if expr.split('=')[0] in expressionDict:
                    print 'Already added.'
                expressionDict[expr.split('=')[0]]=expr.split('=')[1]
            internalNodesOnly = [str(x) for x in expressionDict.keys() if x not in outputs]
            S1 = set(internalNodesOnly)
            algebra = boolean.BooleanAlgebra()
            for output in outputs:
                temp = expressionDict[output]
                while 1:
                    expr = algebra.parse(temp)
                    literals = [str(symbolVariable) for symbolVariable in
                                list(expr.symbols)]
                    if (not literals):
                        break
                    S2 = set(literals)
                    isPresent = bool(S1 & S2)
                    if (isPresent):
                        for indExpr in S2:
                            if (indExpr in expressionDict):
                                toBeReplaced = str(expressionDict[indExpr])
                                temp = temp.replace(indExpr,toBeReplaced)
                    else:
                        break
                newCNFExpr = to_cnf(str(temp.replace('*',' and ')))
                clauses = []
                for clause in newCNFExpr.iter_clauses():
                    clauses.append(clause)
                outputsExpressionInPIsDict[output]=clauses#algebra.normalize(newExpr, expr.OR)
            return (expressionDict,outputsExpressionInPIsDict)
        except PyrtlInternalError as e:
            print 'Error reading the Inputfile .Exception: %s' \
                % str(e)
    finally:
        f.close()
