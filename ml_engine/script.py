#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import re
import os
import math
import subprocess
import collections
from project import factorofexp

__author__ = 'Sparsh Gupta'
__version__ = '1.0.0'
__maintainer__ = 'Sparsh Gupta'
__email__ = 'sparshgu@usc.edu'

FactoredExpression = collections.namedtuple('FactoredExpression',
        'outputNet outputNetType node')


def calculateApproximateFactor(expressions):
    for expression in expressions:
        if expression.outputNetType == 'NOT':
            print 'Expression ' + expression.outputNet + ' = ' \
                + expression.node + ' can\'t be optimized further.\n'
            continue
        errorDict = factorofexp(expression.node)
        sortedErrorDictExpression = sorted(errorDict.items(),
                key=lambda x: x[1])
        print 'Approximated Expresion:' + expression.outputNet + ' = ' \
            + sortedErrorDictExpression[1][0] \
            + ' with error percentage of ' \
            + str(sortedErrorDictExpression[1][1]) + '%'
        print 'Original Expresion:' + expression.outputNet + ' = ' \
            + expression.node + '\n'


def getNetExpressions(fileInputPath):
    f = open(fileInputPath, 'r')
    try:
        try:
            inputFileLines = f.readlines()
            NodesExpressions = []
            for inputLine in inputFileLines:
                temp = inputLine.split(' ')
                output = temp[3].split('(')[1].split(';')[0].split(','
                        )[0]
                if 'not' == temp[2]:
                    input = temp[3].split('(')[1].split(';'
                            )[0].split(',')[1].split(')')[0]
                    val = FactoredExpression(outputNet=output,
                            outputNetType='NOT', node='not(' + input
                            + ')')
                    notString = output + '=not(' + input + ')'

                    # print notString

                    NodesExpressions.append(val)
                elif 'nand' == temp[2]:
                    numberOfInputs = temp[3].split('(')[0].split('NAND'
                            )[1].split('_')[0]
                    nandString = 'not('
                    for index in range(1, int(numberOfInputs) + 1):
                        nandString += temp[3].split('(')[1].split(';'
                                )[0].split(',')[index].split(')')[0] \
                            + '*'
                    nandString = nandString[:-1]
                    nandString += ')'
                    val = FactoredExpression(outputNet=output,
                            outputNetType='NAND', node=nandString)
                    NodesExpressions.append(val)
                elif 'nor' == temp[2]:

                    # print output,nandString

                    numberOfInputs = temp[3].split('(')[0].split('NOR'
                            )[1].split('_')[0]
                    norString = 'not('
                    for index in range(1, int(numberOfInputs) + 1):
                        norString += temp[3].split('(')[1].split(';'
                                )[0].split(',')[index].split(')')[0] \
                            + '+'
                    norString = norString[:-1]
                    norString += ')'
                    val = FactoredExpression(outputNet=output,
                            outputNetType='NOR', node=norString)
                    NodesExpressions.append(val)
                elif 'xor' == temp[2]:
                    # xorString = '('
                    # xorString += temp[3].split('(')[1].split(';')[0].split(',')[1].split(')')[0]+ '*(not(' + temp[3].split('(')[1].split(';')[0].split(',')[2].split(')')[0]
                    # xorString +='))+'
                    # xorString += temp[3].split('(')[1].split(';')[0].split(',')[2].split(')')[0]+ '*(not(' + temp[3].split('(')[1].split(';')[0].split(',')[1].split(')')[0]
                    # xorString +=')))'
                    # val = FactoredExpression(outputNet=output,
                    #         outputNetType='XOR', node=xorString)
                    # NodesExpressions.append(val)
                    #print output,xorString
                    print 'XOR ignored'
                elif 'and' == temp[2]:
                    numberOfInputs = temp[3].split('(')[0].split('AND'
                            )[1].split('_')[0]
                    andString = '('
                    for index in range(1, int(numberOfInputs) + 1):
                        andString += temp[3].split('(')[1].split(';'
                                )[0].split(',')[index].split(')')[0] \
                            + '*'
                    andString = andString[:-1] + ')'
                    val = FactoredExpression(outputNet=output,
                            outputNetType='AND', node=andString)
                    NodesExpressions.append(val)
                elif 'or' == temp[2]:

                    # print output,andString

                    numberOfInputs = temp[3].split('(')[0].split('OR'
                            )[1].split('_')[0]
                    orString = '('
                    for index in range(1, int(numberOfInputs) + 1):
                        orString += temp[3].split('(')[1].split(';'
                                )[0].split(',')[index].split(')')[0] \
                            + '+'
                    orString = orString[:-1] + ')'
                    val = FactoredExpression(outputNet=output,
                            outputNetType='OR', node=orString)
                    NodesExpressions.append(val)
                else:

                    # print output,orString

                    pass
            return NodesExpressions
        except:
            print 'Error reading the Inputfile .Exception: %s' \
                % sys.exc_info()[0]
    finally:
        f.close()


def main(arguments):
    if len(arguments) != 1:
        print 'usage: script.py -t=<InputCircuitVerilogTemplate>'
        sys.exit(1)
    if not '-t=' in arguments[0]:
        print 'usage: script.py -t=<InputCircuitVerilogTemplate>'
        sys.exit(1)
    fileInputPath = (arguments[0])[3:]
    if not os.path.isfile(fileInputPath):
        print 'usage: script.py -t=<InputCircuitVerilogTemplate>'
        print "InputFile doesn't exist"
        sys.exit(1)
    expressions = getNetExpressions(fileInputPath)
    calculateApproximateFactor(expressions)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
