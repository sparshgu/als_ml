#ifndef WRITE_FUNC_H
#define WRITE_FUNC_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>

void write_ckt_org(BnetNetwork *net);
void write_mvsis_rug(int iIndex, char *node);
void write_ckt_bignode(BnetNetwork *net, char *cnode);
void write_ckt_bignode(BnetNetwork *net, char *cnode, vector<char*> &cutnodes, multimap<double, string> &exdc_set);
void write_ckt_sim_const(BnetNetwork *net, char *cnode, int pol);
void write_ckt_sim(BnetNetwork *net, char *node, vector<string> &sim_node_nodes);
void write_ckt_sim_file(BnetNetwork *net, char *cnode, vector<string> &sim_node_nodes, string &filename);
void gen_ckt_tb();
void write_ckt_comb(BnetNetwork *net);
void rewrite_bigNode_sim(char *cnode);
void write_ckt_whole(BnetNetwork *net);
void write_bignode_pla(BnetNetwork *net, char *cnode);
void write_bignode_pla_sim(BnetNetwork *net, char *cnode, vector<string> &sim_org_pla);

void write_mvsis_rug_MFFC(char *node);
void cluster_ckt(BnetNetwork *net, char *cnode, set<char*> &cMFFC, map<char*, char*> &in_sig);

#endif
