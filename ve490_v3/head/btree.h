#ifndef BTREE_H
#define BTREE_H

#include <iostream>

using namespace std;  


struct btNode{
   // store operator or operand
   string data;
   // only valid for operator
   int precedence;
   //number of product term at each node
   int numPT; 
   //index of leaf node
   int ind;
   //expressions at this node
   vector<string> exp;
   struct btNode* parent;
   struct btNode* left;
   struct btNode* right;
};

btNode * CreateInfixTree(const string& exp);
void PostOrderPrintTree(btNode * node);
void InOrderPrintTree(btNode * node);
void compNumPT(btNode **node, int &num);

void visitleaf(btNode *root, map<int, btNode*> &index_leaf_set, int &ind);
void visitleafnode(btNode *root, multimap<string, btNode*> &node_leaf_set);

void compInvCubeNum(btNode *root, btNode *leaf, int &num_cube);

void removeLeaf(btNode *root, btNode *leaf);
int removeLeafNode(btNode **root, string &leaf_node);
void copyTree(btNode *root, btNode *new_root);
void freeTree(btNode *root);

void InOrderPrintExp0(btNode * node);
void InOrderPrintExp(btNode * node);
void comp_exp(btNode **root);
void get_involve_cubes(btNode *root, btNode *leaf, vector<string> &inv_cubes);

void printVec(vector<string> &cubes);
void get_involve_pla(map<int, btNode*> &leaf_set, map<string, int> &name_pos, int len, string &this_inv_cube, string &this_inv_pla);

#endif
