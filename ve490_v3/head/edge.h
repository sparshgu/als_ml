#ifndef EDGE_H
#define EDGE_H
#include <vector>
#include <iostream>
using namespace std;
class Node;
class Edge
{    
	public:
	int index;
    int f;  //flow
    int c;  //capacity
	bool visit;
	Node *start;
	Node *end;
	Edge();
	Edge(Node *s,Node *d, int ind);
};
#endif
