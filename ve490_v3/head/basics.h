#ifndef BASICS_H
#define BASICS_H
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <climits>
#include <vector>
#include <set>
#include <map>
#include <iostream>
#include "../cudd/bnet.h"

using namespace std; 

double findmin(double p);
void asc_sort(BnetNetwork *net, vector<char*> &cutnodes);
int permute(vector<string> &dont_care, vector<string> &insig_string, vector<char*> &cutnodes);
void permute_v2(vector<string> &dont_care, vector<char*> &insig_string, vector<char*> &cutnodes);
void topSort(BnetNetwork **net, vector<char*> &sort_list);
void get_MFFC(BnetNetwork *net, vector<char*> &sort_list, map<char*, set<char*> > &TFI_set_char, map<char*, set<char*> > &MFFC_set);
void update_ckt(BnetNetwork *net);

#endif
