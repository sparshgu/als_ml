#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <climits>
#include <vector>
#include <map>
#include <cmath>
#include <cassert>
#include <ctime>
#include <sys/timeb.h>
#include "head/queue.h"
#include "head/basics.h"
#include "head/exdc_factor_new.h"
#include "head/helper.h"
#include "head/read_file.h"
#include "head/write_func.h"
#include "../cudd-2.5.0/cudd/cudd.h"
#include "../cudd-2.5.0/cudd/cuddInt.h"
#include "cudd/cudd_build.h"
#include "cudd/cudd_comp.h"
#include "cudd/cudd_dst.h"

using namespace std;

extern int numPI_ini;

/*
functions in this file:

*/

//Global variables and external variables 


/*1. find_ave_sp()*/
void find_ave_sp(BnetNetwork *net, multimap<double, char*> &ave_sp)
{
	BnetNode *nd, *tmp;

	nd = net->nodes;
	while(nd != NULL)
	{
		if(nd->ninp < 2 || nd->ninp > 15)
//		if(nd->ninp < 2)
		{
			nd = nd->next;
			continue;
		}
		double prod = 1;
//		cout << "current node: " << nd->name << endl;
		for(int i = 0; i < nd->ninp; i++)
		{
			char *innode = nd->inputs[i];
			if(!st_lookup(net->hash, innode, &tmp))
			{
				cout << "this node doesn't exist in hash!" << endl;
				exit(1);
			}
			prod = prod * findmin(tmp->rp);
		}
//		cout << "prod = " << prod << endl;
		prod = pow(prod, 1.0/nd->ninp);
		ave_sp.insert(pair<double, char*>(prod, nd->name));
//		cout << "node " << nd->name << ", ave_sp: " << prod << endl;
		nd = nd->next;
	}
}


//2. find_exdc_sim()
char *find_exdc_sim(BnetNetwork *net, DdManager **dd, BnetNetwork *net_comb, DdManager **dd_comb,  vector<string> &final_pla, map<string, struct score_pla> &sim_record, char *last_node, vector<char*> &last_inputs, double &max_score, double threshold, int iIndex)
{
    struct timeb st_es, et_es;
    double total_espresso_bignode = 0, total_exdc = 0;
    BnetNode *nd, *tmp, *last_nd, *auxnd, *nd1, *nd2;
    multimap<double, char*>::iterator itrm_dc; 
    multimap<double, string>::iterator itrmm_ds;
    map<string, struct score_pla>::iterator itrm_ss;
    map<string, multimap<double, string> >:: iterator itrm_cm;
    ifstream fin;
    FILE *fp;
    string str;
    
    char com[100];
    multimap<double, char*> ave_sp;
    find_ave_sp(net, ave_sp);
    itrm_dc = ave_sp.begin();
    char *max_score_node = itrm_dc->second;            
    int index = 0;  
        
    //Obtain global_exdc and global_care
	DdNode *global_exdc, *global_care;
	if(iIndex > 0)
	{			
		cout << "last node: " << last_node << endl;
		cout << "last inputs: ";
		for(int i = 0; i < last_inputs.size(); i++)
			cout << last_inputs[i] << " ";
		cout << endl;
		cout << "condition satisfies" << endl;
//		build_ddnode_sop(net_last, dd_last, &global_exdc, last_inputs, max_exdc_set);
		char *outnode = net_comb->outputs[0];
		st_lookup(net_comb->hash, outnode, &nd);
		global_exdc = nd->dd;
//		cout << "global_exdc: " << endl;
//		Cudd_PrintDebug(*dd_comb, global_exdc, numPI_ini, 2);
		global_care = Cudd_Not(global_exdc);
		Cudd_Ref(global_care);
//		cout << "global_care: " << endl;
//		Cudd_PrintDebug(*dd_comb, global_care, numPI_ini, 2);
	}
	        	
    //start the for loop
    vector<char*> next_last_inputs;
    for(itrm_dc = ave_sp.begin(); itrm_dc != ave_sp.end(); itrm_dc++, index++)
    {
        char *cnode = itrm_dc->second;
        st_lookup(net->hash, cnode, &nd);

		//find exdc for current big node
        cout << endl <<"--------------------------------------------" << endl;  
        cout << "%%exdc for " << cnode << ", index = " << index << ", rp = " << nd->rp << ", average prob = " << itrm_dc->first << endl; 
        int sup = Cudd_SupportSize(*dd, nd->dd);
		cout << "# of fanouts: " << nd->nfo << ", sup: " << sup << endl;    
		
	//	if(strcmp(cnode, "864"))
	//		continue;
		
		//get the factored form of cnode
		cout << endl << "num_lit: ";		
		write_bignode_pla(net, cnode);   			                        
		sprintf(com, "sis -t none -f ./script/print_lit_org.rug > ./output/lit.txt");
		system(com);
		int num_lit = read_num_lit();
		cout << num_lit << endl;
		if(num_lit < 2)
			continue;
		
		
        vector<char*> unsort_cutnodes, cutnodes;  
        for(int i = 0; i < nd->ninp; i++)
        {
        	cout << nd->inputs[i] << " ";
			char *cp1 = new char[50];			
			strcpy(cp1, nd->inputs[i]);
			char *cp2 = new char[50];			
			strcpy(cp2, nd->inputs[i]);
			unsort_cutnodes.push_back(cp1);
 //       	cutnodes.push_back(cp2);
        }
       	cout << endl;
/*      asc_sort(net, cutnodes);
       	cout << "after sort(): " << endl;
		for(int i = 0; i < cutnodes.size(); i++)
		    cout << cutnodes[i] << " ";
		cout << endl;
*/
       	
  	 	//Call MVSIS to obtain the don't cares for the current node
    	cout << endl << "a0. Run mvsis!" << endl;
    	ftime(&st_es);
    	char com_mv[100]; 
    	write_mvsis_rug(iIndex, cnode);                                 
    	sprintf(com_mv, "mvsis -t none -f ./script/mvsis_dc.rug > mvsis.txt");
    	system(com_mv);    
   	 	ftime(&et_es);
    	double rt_mvsis = ((et_es.time - st_es.time)*1000 + (et_es.millitm - st_es.millitm))/1000.0;
    	cout << "@runtime for a0. mvsis: " << rt_mvsis << endl;    
    	vector<string> dont_care;
    	vector<string> org_pla;
    	vector<string> insig_string;
    	BnetTabline *t = nd->f;
		while(t != NULL) 
		{
			string str(t->values);
			org_pla.push_back(str);
			t = t->next;
		}

    	read_mvsis_result(dont_care, insig_string);   
    	cout << "dont_care size: " << dont_care.size() << endl;
    	for(int i = 0; i < dont_care.size(); i++)
	    		cout << dont_care[i] << endl;
	    	cout << endl;

    	if(!dont_care.empty()) 			
	    {	    	
	    	int res = permute(dont_care, insig_string, unsort_cutnodes);
	    	if(res)
	    	{
	    		cout << "mvsis error!" << endl;
	    		dont_care.clear();
	    	}
	 /*   	for(int i = 0; i < dont_care.size(); i++)
	    		cout << dont_care[i] << endl;
	    	cout << endl;
	*/
		}	
		
		//Find exdcs from previous iterations
		cout << "a1. image_computation" << endl;	
		ftime(&st_es);	
		vector<string> local_dc_set;	
		if(iIndex > 0)
		{			
			if(strcmp(cnode, last_node))
		    {
		    	char *cnode_sim = new char[50];
		    	strcpy(cnode_sim, cnode);
		    	strcat(cnode_sim, "sim");
		    	image_computation(net_comb, dd_comb, global_care, cnode_sim, local_dc_set);
		    	delete []cnode_sim;
		    }
		}
		cout << "local_dc_set: " << local_dc_set.size() << endl;
		for(int i = 0; i < local_dc_set.size(); i++)
			cout << local_dc_set[i] << endl;
		cout << endl;	        
		ftime(&et_es);	
		double rt_image = ((et_es.time - st_es.time)*1000 + (et_es.millitm - st_es.millitm))/1000.0;
    	cout << "@runtime for a1. image_computation: " << rt_image << endl;    
    	
	    //Find all qualified exdcs
		cout << endl << "a2. exdc_new_v2: " << endl;
		ftime(&st_es);	
		struct score_pla max_sp;
		exdc_factor_new(net, dd, cnode, unsort_cutnodes, sim_record, org_pla, dont_care, local_dc_set, max_sp, threshold, iIndex);
	//	exdc_factor(net, dd, cnode, unsort_cutnodes, sim_record, org_pla, dont_care, local_dc_set, max_sp, threshold, iIndex);
		double each_score = max_sp.score;
		cout << endl << "$each_score = " << each_score << endl;
        ftime(&et_es);
        double rt_exdc = ((et_es.time - st_es.time)*1000 + (et_es.millitm - st_es.millitm))/1000.0;
        total_exdc += rt_exdc;
		cout << "@runtime for a2. exdc_new_v2: " << rt_exdc << endl;
		
		//Insert the current simplification result into sim_record
/*		cout << "max_sp: " << endl;
		cout << "score : " << max_sp.score << endl;
		cout << "real_er: " << max_sp.real_er << endl;
		cout << "pla: " << endl;
		for(int j = 0; j < max_sp.pla.size(); j++)
			cout << max_sp.pla[j] << endl;
		string cnode_str(cnode);
		itrm_ss = sim_record.find(cnode_str);
		if(itrm_ss == sim_record.end())
  		{
  			cout << "inserted!" << endl;
  			sim_record.insert(pair<string, score_pla>(cnode_str, max_sp));
  		}
*/
		double each_real_er = max_sp.real_er;
		if(each_score > max_score && each_real_er <= threshold)
		{
			max_score = each_score;
            max_score_node = cnode;            
            if(next_last_inputs.size() > 0)
            {
            	for(int i = 0; i < next_last_inputs.size(); i++)
            		delete []next_last_inputs[i];
            	next_last_inputs.clear();
            }
            for(int i = 0; i < cutnodes.size(); i++)
            {
            	char *innode = new char[50];
            	strcpy(innode, cutnodes[i]);
            	next_last_inputs.push_back(innode);
            }
            //store the circuit for bigNode_sim.blif into node_sim_files  
            final_pla = max_sp.pla;
		}		
		               
		for(int i = 0; i < cutnodes.size(); i++)
			delete []cutnodes[i];   
		for(int i = 0; i < unsort_cutnodes.size(); i++)
			delete []unsort_cutnodes[i]; 
		
    }//for loop
    
    //Remove the tfo nodes of current node from sim_record
    tmp = net->nodes;
	while(tmp != NULL)
	{
		tmp->visited = 0;
		tmp = tmp->next;
	}
	set<char*> tfo;
	set<char*>::iterator itrs;
    find_tranfanout(net, max_score_node, tfo);
    for(itrs = tfo.begin(); itrs != tfo.end(); itrs++)
    {
    	char *node = *itrs;
    	string node_str(node);
		itrm_ss = sim_record.find(node_str);
		if(itrm_ss != sim_record.end())
		{
			cout << "tfo node " << node << " is removed!" << endl;
			sim_record.erase(itrm_ss);
		}
    }
    
    //record max_exdc_set, last_node and last_inputs for this loop
//    max_exdc_set = next_max_exdc_set;
    if(last_node != NULL)
    {
    	delete []last_node;
        last_node = new char[50];
    }
    strcpy(last_node, max_score_node);  
    if(last_inputs.size() > 0)
    	for(int i = 0; i < last_inputs.size(); i++)
       		delete []last_inputs[i];    
    last_inputs.clear();
    for(int k = 0; k < next_last_inputs.size(); k++)
    {
    	char *innode = new char[50];
    	strcpy(innode, next_last_inputs[k]);
    	last_inputs.push_back(innode);
    }
    
/*    map<string, struct score_pla>::iterator itrm_css;
    cout << "0. sim_record: " << endl;
    for(itrm_css = sim_record.begin(); itrm_css != sim_record.end(); itrm_css++)
    {
    	string node = itrm_css->first;
    	cout << node << ": real_er: " << itrm_css->second.real_er << ", score: " << itrm_css->second.score << endl;
    }
*/    
	
    cout << endl << "summary: " << endl;
    cout << "max_score_node = " << max_score_node << endl; 
    cout << "max_score = " << max_score << endl;
    cout << "total_exdc = " << total_exdc << endl; 
    cout << "total_espresso_bignode = " << total_espresso_bignode << endl;      
    
    return max_score_node;

}



//3. extract_sim_nodes()
/*void extract_sim_nodes(vector<string> &sim_node_lines, vector<string> &sim_node_nodes)
{    
	int flag_start = 0;
	vector<string> sim_node_nodes_new;
    for(int i = 0; i < sim_node_lines.size(); i++)
    {
        string str = sim_node_lines[i];
        if(str.empty())
            continue;
        istringstream ss(str);
        string s1;
        ss >> s1;
        if(s1 == ".exdc" || s1 == ".end")
            break;
        if(s1 == ".names")
        	flag_start = 1;
        if(flag_start)
            sim_node_nodes.push_back(str);
    } 
    if(sim_node_nodes.size() == 1)
    {
    	stringstream ss(sim_node_nodes[0]);
    	string s;
    	while(ss >> s);
    	string str(".names ");
    	str.append(s);    	
    	sim_node_nodes_new.push_back(str);     //.names node
    	str.clear();
    	str.append(1, '0');
    	sim_node_nodes_new.push_back(str);     //0
    	sim_node_nodes = sim_node_nodes_new;
    }   
    
}
*/

void extract_sim_nodes(vector<string> &sim_node_lines, vector<string> &sim_node_nodes)
{    

	string str, s, s1;
	str = sim_node_lines[0];
	istringstream ss(str);
	ss >> s;
	if(s == ".names")
	{
		sim_node_nodes = sim_node_lines;
		return;
	}
		
	vector<string> sim_node_nodes_new;	
	string input_names(".names ");
	for(int i = 0; i < sim_node_lines.size(); i++)
	{
		string str = sim_node_lines[i];
		istringstream ss(str);
		ss >> s;		
		if(s[0] == '.')
		{
			if(s == ".ilb")
			{					
				while(ss >> s)
				{
					input_names.append(s);
					input_names.append(" ");
				}
			}
			else if(s == ".ob")
			{
				ss >> s;
				input_names.append(s);
				sim_node_nodes.push_back(input_names);
			}
			continue;
		}					
		ss >> s1;
		if(s1 == "2")
			break;
		sim_node_nodes.push_back(str);				
	}
	
	if(sim_node_nodes.size() == 1)
    {
    	stringstream ss(sim_node_nodes[0]);
    	string s;
    	while(ss >> s);
    	string str(".names ");
    	str.append(s);    	
    	sim_node_nodes_new.push_back(str);     //.names node
    	str.clear();
    	str.append(1, '0');
    	sim_node_nodes_new.push_back(str);     //0
    	sim_node_nodes = sim_node_nodes_new;
    }   
    
}
