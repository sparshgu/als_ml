#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <climits>
#include <vector>
#include <map>
#include <cmath>
#include <cassert>
#include <ctime>
#include <sys/timeb.h>
#include "head/queue.h"
#include "head/basics.h"
#include "head/exdc_new_v2.h"
#include "head/helper.h"
#include "head/read_file.h"
#include "head/write_func.h"
#include "/home/wuyi/usr/CUDD/cudd-2.5.0/cudd/cudd.h"
#include "/home/wuyi/usr/CUDD/cudd-2.5.0/cudd/cuddInt.h"
#include "cudd/cudd_build.h"
#include "cudd/cudd_comp.h"
#include "cudd/cudd_dst.h"

using namespace std;

extern int numPI_ini;

/*
functions in this file:

*/

//Global variables and external variables 


/*1. find_ave_sp()*/
void find_ave_sp(BnetNetwork *net, multimap<double, char*> &ave_sp)
{
	BnetNode *nd, *tmp;

	nd = net->nodes;
	while(nd != NULL)
	{
		if(nd->ninp < 2 || nd->ninp > 15)
//		if(nd->ninp < 2)
		{
			nd = nd->next;
			continue;
		}
		double prod = 1;
//		cout << "current node: " << nd->name << endl;
		for(int i = 0; i < nd->ninp; i++)
		{
			char *innode = nd->inputs[i];
			if(!st_lookup(net->hash, innode, &tmp))
			{
				cout << "this node doesn't exist in hash!" << endl;
				exit(1);
			}
			prod = prod * findmin(tmp->rp);
		}
//		cout << "prod = " << prod << endl;
		prod = pow(prod, 1.0/nd->ninp);
		ave_sp.insert(pair<double, char*>(prod, nd->name));
//		cout << "node " << nd->name << ", ave_sp: " << prod << endl;
		nd = nd->next;
	}
}


//2. find_exdc_sim()
char *find_exdc_sim(BnetNetwork *net, DdManager **dd, BnetNetwork *net_comb, DdManager **dd_comb,  map<char*, vector<string> > &node_sim_files, map<string, multimap<double, string> > &sim_record, char *last_node, vector<char*> &last_inputs, double &max_score, double threshold, int iIndex)
{
    struct timeb st_es, et_es;
    double total_espresso_bignode = 0, total_exdc = 0;
    BnetNode *nd, *tmp, *last_nd, *auxnd, *nd1, *nd2;
    multimap<double, char*>::iterator itrm_dc; 
    multimap<double, string>::iterator itrmm_ds;
    map<string, multimap<double, string> >:: iterator itrm_cm;
    ifstream fin;
    FILE *fp;
    string str;
    
    multimap<double, char*> ave_sp;
    find_ave_sp(net, ave_sp);
    itrm_dc = ave_sp.begin();
    char *max_score_node = itrm_dc->second;            
    int index = 0;  
    
    cout << "sim_record: " << endl;
    for(itrm_cm = sim_record.begin(); itrm_cm != sim_record.end(); itrm_cm++)
    {
    	cout << endl << "##node " << itrm_cm->first << endl;
    	multimap<double, string> exdc_set_rec = itrm_cm->second;
    	for(itrmm_ds = exdc_set_rec.begin(); itrmm_ds != exdc_set_rec.end(); itrmm_ds++)
    	{
    		if(itrmm_ds->first == 0)
    			continue;
    		cout << itrmm_ds->second << ": " << itrmm_ds->first << endl; 
    	} 
    }
        
    //Obtain global_exdc and global_care
	DdNode *global_exdc, *global_care;
	if(iIndex > 0)
	{			
		cout << "last node: " << last_node << endl;
		cout << "last inputs: ";
		for(int i = 0; i < last_inputs.size(); i++)
			cout << last_inputs[i] << " ";
		cout << endl;
		cout << "condition satisfies" << endl;
//		build_ddnode_sop(net_last, dd_last, &global_exdc, last_inputs, max_exdc_set);
		char *outnode = net_comb->outputs[0];
		st_lookup(net_comb->hash, outnode, &nd);
		global_exdc = nd->dd;
//		cout << "global_exdc: " << endl;
//		Cudd_PrintDebug(*dd_comb, global_exdc, numPI_ini, 2);
		global_care = Cudd_Not(global_exdc);
		Cudd_Ref(global_care);
//		cout << "global_care: " << endl;
//		Cudd_PrintDebug(*dd_comb, global_care, numPI_ini, 2);
	}
	        	
    //start the for loop
    vector<char*> next_last_inputs;
    for(itrm_dc = ave_sp.begin(); itrm_dc != ave_sp.end(); itrm_dc++, index++)
    {
        char *cnode = itrm_dc->second;
        st_lookup(net->hash, cnode, &nd);

		//find exdc for current big node
        cout << endl <<"--------------------------------------------" << endl;  
        cout << "%%exdc for " << cnode << ", index = " << index << ", rp = " << nd->rp << ", average prob = " << itrm_dc->first << endl; 
        int sup = Cudd_SupportSize(*dd, nd->dd);
		cout << "# of fanouts: " << nd->nfo << ", sup: " << sup << endl;    	
		
        vector<char*> unsort_cutnodes, cutnodes;  
        for(int i = 0; i < nd->ninp; i++)
        {
        	cout << nd->inputs[i] << " ";
			char *cp1 = new char[50];			
			strcpy(cp1, nd->inputs[i]);
			char *cp2 = new char[50];			
			strcpy(cp2, nd->inputs[i]);
			unsort_cutnodes.push_back(cp1);
        	cutnodes.push_back(cp2);
        }
       	cout << endl;
       	asc_sort(net, cutnodes);
       	cout << "after sort(): " << endl;
		for(int i = 0; i < cutnodes.size(); i++)
		    cout << cutnodes[i] << " ";
		cout << endl;
/*		cout << "correlation: " << endl;
		for(int i = 0; i < cutnodes.size(); i++)
			for(int j = i+1; j < cutnodes.size(); j++)
			{
				st_lookup(net->hash, cutnodes[i], &nd1);
				st_lookup(net->hash, cutnodes[j], &nd2);
				DdNode *dep = Cudd_bddConstrain(*dd, nd1->dd, nd2->dd);
				Cudd_Ref(dep);
				double num_dep = Cudd_CountMinterm(*dd, dep, numPI_ini);
				double num_node1 = Cudd_CountMinterm(*dd, nd1->dd, numPI_ini);	
				double num_node2 = Cudd_CountMinterm(*dd, nd2->dd, numPI_ini);				
				double corr = num_dep/num_node1;
				cout << endl << cutnodes[i] << " & " << cutnodes[j] << ": " << corr << endl; 
				cout << "num_dep = " << num_dep << ", num_node1 = " << num_node1 << ", num_node2 = " << num_node2 << endl;
			}
*/
       	
  	 	//Call MVSIS to obtain the don't cares for the current node
    	cout << endl << "a0. Run mvsis!" << endl;
    	ftime(&st_es);
    	char com_mv[100]; 
    	write_mvsis_rug(iIndex, cnode);                                 
    	sprintf(com_mv, "mvsis -t none -f ./script/mvsis_dc.rug > mvsis.txt");
    	system(com_mv);    
   	 	ftime(&et_es);
    	double rt_mvsis = ((et_es.time - st_es.time)*1000 + (et_es.millitm - st_es.millitm))/1000.0;
    	cout << "@runtime for a0. mvsis: " << rt_mvsis << endl;    
    	vector<string> dont_care;
    	vector<string> org_pla;
    	vector<string> insig_string;
    	BnetTabline *t = nd->f;
		while(t != NULL) 
		{
			string str(t->values);
			org_pla.push_back(str);
			t = t->next;
		}

    	read_mvsis_result(dont_care, org_pla, insig_string);   
//    	permute(org_pla, insig_string, unsort_cutnodes);
    	cout << "permute dont_care size: " << dont_care.size() << endl;
    	if(!dont_care.empty()) 			
	    {	    	
	    	permute(dont_care, insig_string, unsort_cutnodes);
//			permute(dont_care, insig_string, cutnodes);
/*	    	for(int i = 0; i < dont_care.size(); i++)
	    		cout << dont_care[i] << endl;
	    	cout << endl;
*/
		}	
		
		//Find exdcs from previous iterations
		cout << "a1. image_computation" << endl;	
		ftime(&st_es);	
		set<string> local_dc_set, sorted_dc_set;
		set<string>::iterator itrs;		
		if(iIndex > 0)
		{			
			if(strcmp(cnode, last_node))
		    {
		    	char *cnode_sim = new char[50];
		    	strcpy(cnode_sim, cnode);
		    	strcat(cnode_sim, "sim");
		    	image_computation(net_comb, dd_comb, global_care, cnode_sim, local_dc_set);
		    	delete []cnode_sim;
		    }
		}
		cout << "local_dc_set: " << local_dc_set.size() << endl;
		for(itrs = local_dc_set.begin(); itrs != local_dc_set.end(); itrs++)
			cout << *itrs << endl;
		cout << endl;	        
		ftime(&et_es);	
		double rt_image = ((et_es.time - st_es.time)*1000 + (et_es.millitm - st_es.millitm))/1000.0;
    	cout << "@runtime for a1. image_computation: " << rt_image << endl;    
    	
	    //Find all qualified exdcs
		cout << endl << "a2. exdc_new_v2: " << endl;
		ftime(&st_es);	
        multimap<double, string> exdc_set; 
        cout << "cnode = " << cnode << endl;
        double include_exdc_er = 0;
//		exdc_new_v2(net, dd, cnode, cutnodes, exdc_set, threshold, dont_care, iIndex); 
		exdc_new_v3(net, dd, cnode, unsort_cutnodes, org_pla, exdc_set, threshold, dont_care, iIndex);
//		exdc_new_v3(net, dd, cnode, unsort_cutnodes, org_pla, local_dc_set, exdc_set, threshold, dont_care, iIndex);
		
/*		//permute exdc_set
		vector<string> exdc_set_vec;
		for(itrmm_ds = exdc_set.begin(); itrmm_ds != exdc_set.end(); itrmm_ds++)
			exdc_set_vec.push_back(itrmm_ds->second);
		permute_v2(exdc_set_vec, cutnodes, unsort_cutnodes);
		int j = 0;
		for(itrmm_ds = exdc_set.begin(); itrmm_ds != exdc_set.end(); itrmm_ds++, j++)
			itrmm_ds->second = exdc_set_vec[j];
*/
        ftime(&et_es);
        double rt_exdc = ((et_es.time - st_es.time)*1000 + (et_es.millitm - st_es.millitm))/1000.0;
        total_exdc += rt_exdc;
		for(itrs = local_dc_set.begin(); itrs != local_dc_set.end(); itrs++)  //insert local_dc_set
	    {
	        exdc_set.insert(pair<double, string>(0, *itrs));
	    }
		cout << "@runtime for a2. exdc_new_v2: " << rt_exdc << endl;

        if(exdc_set.size() == 0)
            continue;

        	    
        //Pick the most useful exdcs for cnode && write bigNode.blif && obtain bigNode_sim_rew.blif 
        cout << endl << "a3. sim_pick: " << endl;
        ftime(&st_es);	
        char com[100];  
        double each_score;
        int num_lit_org = 0;
		cout << "org_pla: " << endl;
		for(int i = 0; i < org_pla.size(); i++)
		{
			string str = org_pla[i];
			cout << str << endl;
		}
		if(nd->p > threshold)
		{
			cout << "nd->p > threshold " << endl;
			multimap<double, string> exdc_record, exdc_record_new;
/*			double include_er = 0;	
			string name(cnode);
			itrm_cm = sim_record.find(name);			
			if(itrm_cm != sim_record.end())
			{
				exdc_record = itrm_cm->second;	
				itrmm_ds = exdc_record.begin();
				string str = itrmm_ds->second;
				cout << "first exdc: " << str << endl;
				if(str.size() == nd->ninp)
				{					
					cout << "exdc_record: " << endl;
			    	for(itrmm_ds = exdc_record.begin(); itrmm_ds != exdc_record.end(); itrmm_ds++)
			    	{			    		
			    		if(itrmm_ds->first == 0)
			    			continue;
			    		include_er += itrmm_ds->first;
			    		exdc_record_new.insert(pair<double, string>(itrmm_ds->first, itrmm_ds->second));
			    		cout << itrmm_ds->second << ": " << itrmm_ds->first << endl;
			    		cout << "include_er = " << include_er << endl;
			    	} 
			    	if(include_er > threshold)
			    		exdc_record.clear();
			    	else
			    		exdc_record = exdc_record_new;
			    }
			    else
			    	exdc_record.clear();
		    }  				
*/
			each_score = sim_pick(net, cnode, cutnodes, exdc_set, exdc_record, iIndex); 
/*			itrm_cm = sim_record.find(cnode);	
			if(itrm_cm != sim_record.end())
				sim_record.erase(itrm_cm);				
			sim_record.insert(pair<string, multimap<double, string> >(name, exdc_set));
*/
			cout << "each_score = " << each_score << endl;      
			write_bignode_pla(net, cnode, exdc_set);			                      
			sprintf(com, "espresso ./pla_files/bigNode.pla > ./pla_files/bigNode_sim.pla");
			system(com);                            
		}   
		else
		{
			cout << "nd->p <= threshold " << endl;
			//write bigNode.blif without exdc
			write_ckt_bignode(net, cnode); 
			sprintf(com, "sis -t none -f ./script/print_fac_one.rug > sis.txt");
			system(com);   
			double this_area_save = read_sis_result(); 
			each_score = this_area_save/nd->p;
			cout << "each_score = " << each_score << endl;     
		} 
	    
	    ftime(&et_es);
        double rt_sim_pick = ((et_es.time - st_es.time)*1000 + (et_es.millitm - st_es.millitm))/1000.0;
		cout << "@runtime for sim_pick: " << rt_sim_pick << endl;
 
		if(each_score > max_score)
		{
			max_score = each_score;
            max_score_node = itrm_dc->second;            
            if(next_last_inputs.size() > 0)
            {
            	for(int i = 0; i < next_last_inputs.size(); i++)
            		delete []next_last_inputs[i];
            	next_last_inputs.clear();
            }
            for(int i = 0; i < cutnodes.size(); i++)
            {
            	char *innode = new char[50];
            	strcpy(innode, cutnodes[i]);
            	next_last_inputs.push_back(innode);
            }
            //store the circuit for bigNode_sim.blif into node_sim_files  
            vector<string> file_lines;
            if(nd->p > threshold)
		    {
		        ifstream fin;
		        string str;
		        fin.open("./pla_files/bigNode_sim.pla", ifstream::in);		        
		        cout << endl << "bigNode_sim.pla: " << endl;
		        while(getline(fin, str))
		        {	        	
		            file_lines.push_back(str);
		            cout << str << endl;
		        }
		        fin.close();
		        node_sim_files.insert(pair<char*, vector<string> >(cnode, file_lines));
		    }
		    else
		    {
		    	string str(".names ");
				char *name = new char[50];
				strcpy(name, cnode);
				str.append(name);
				file_lines.push_back(str);
				if(nd->rp <= 0.5)
				{
					string s("0");
					file_lines.push_back(s);					
				}
				else
				{
					string s("1");
					file_lines.push_back(s);	
				}
			    node_sim_files.insert(pair<char*, vector<string> >(cnode, file_lines));
		    }		    
		}		
		       
        
		for(int i = 0; i < cutnodes.size(); i++)
			delete []cutnodes[i];   
		for(int i = 0; i < unsort_cutnodes.size(); i++)
			delete []unsort_cutnodes[i]; 
		
		if(each_score < max_score)
            continue;     
    }//for loop
    
    //record max_exdc_set, last_node and last_inputs for this loop
//    max_exdc_set = next_max_exdc_set;
    if(last_node != NULL)
    {
    	delete []last_node;
        last_node = new char[50];
    }
    strcpy(last_node, max_score_node);  
    if(last_inputs.size() > 0)
    	for(int i = 0; i < last_inputs.size(); i++)
       		delete []last_inputs[i];    
    last_inputs.clear();
    for(int k = 0; k < next_last_inputs.size(); k++)
    {
    	char *innode = new char[50];
    	strcpy(innode, next_last_inputs[k]);
    	last_inputs.push_back(innode);
    }
    
	
    cout << endl << "summary: " << endl;
    cout << "max_score_node = " << max_score_node << endl; 
    cout << "max_score = " << max_score << endl;
    cout << "total_exdc = " << total_exdc << endl; 
    cout << "total_espresso_bignode = " << total_espresso_bignode << endl;      
    
    return max_score_node;

}



//3. extract_sim_nodes()
/*void extract_sim_nodes(vector<string> &sim_node_lines, vector<string> &sim_node_nodes)
{    
	int flag_start = 0;
	vector<string> sim_node_nodes_new;
    for(int i = 0; i < sim_node_lines.size(); i++)
    {
        string str = sim_node_lines[i];
        if(str.empty())
            continue;
        istringstream ss(str);
        string s1;
        ss >> s1;
        if(s1 == ".exdc" || s1 == ".end")
            break;
        if(s1 == ".names")
        	flag_start = 1;
        if(flag_start)
            sim_node_nodes.push_back(str);
    } 
    if(sim_node_nodes.size() == 1)
    {
    	stringstream ss(sim_node_nodes[0]);
    	string s;
    	while(ss >> s);
    	string str(".names ");
    	str.append(s);    	
    	sim_node_nodes_new.push_back(str);     //.names node
    	str.clear();
    	str.append(1, '0');
    	sim_node_nodes_new.push_back(str);     //0
    	sim_node_nodes = sim_node_nodes_new;
    }   
    
}
*/

void extract_sim_nodes(vector<string> &sim_node_lines, vector<string> &sim_node_nodes)
{    

	string str, s, s1;
	str = sim_node_lines[0];
	istringstream ss(str);
	ss >> s;
	if(s == ".names")
	{
		sim_node_nodes = sim_node_lines;
		return;
	}
		
	vector<string> sim_node_nodes_new;	
	string input_names(".names ");
	for(int i = 0; i < sim_node_lines.size(); i++)
	{
		string str = sim_node_lines[i];
		istringstream ss(str);
		ss >> s;		
		if(s[0] == '.')
		{
			if(s == ".ilb")
			{					
				while(ss >> s)
				{
					input_names.append(s);
					input_names.append(" ");
				}
			}
			else if(s == ".ob")
			{
				ss >> s;
				input_names.append(s);
				sim_node_nodes.push_back(input_names);
			}
			continue;
		}					
		ss >> s1;
		if(s1 == "2")
			break;
		sim_node_nodes.push_back(str);				
	}
	
	if(sim_node_nodes.size() == 1)
    {
    	stringstream ss(sim_node_nodes[0]);
    	string s;
    	while(ss >> s);
    	string str(".names ");
    	str.append(s);    	
    	sim_node_nodes_new.push_back(str);     //.names node
    	str.clear();
    	str.append(1, '0');
    	sim_node_nodes_new.push_back(str);     //0
    	sim_node_nodes = sim_node_nodes_new;
    }   
    
}
