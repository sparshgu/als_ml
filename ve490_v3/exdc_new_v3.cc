#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <climits>
#include <vector>
#include <map>
#include <cmath>
#include <cassert>
#include <ctime>
#include <sys/timeb.h>
#include "head/queue.h"
#include "head/basics.h"
#include "head/helper.h"
#include "head/read_file.h"
#include "head/write_func.h"
#include "/home/wuyi/usr/CUDD/cudd-2.5.0/cudd/cudd.h"
#include "/home/wuyi/usr/CUDD/cudd-2.5.0/cudd/cuddInt.h"
#include "cudd/cudd_build.h"
#include "cudd/cudd_comp.h"
#include "cudd/cudd_dst.h"

using namespace std;

extern int numPI_ini;

/*
functions in this file:

*/

//Global variables and external variables 


/*dc_real_er_v2()*/
void dc_real_er_v2(BnetNetwork *net, DdManager **dd, vector<char*> &cutnodes, map<string, double> &dc_include, int iIndex)
{
	set<string>::iterator itrs, itrs1;
	map<string, double>::iterator itrm_sd;
	
    /*Build bdds for don't cares in dc_include*/    	
    double total_dc_er = 0;
	char com[100];
	BnetNode *auxnd;
	DdNode *tmp;
    DdNode *func, *prod, *var, *var_tmp;
	for(itrm_sd = dc_include.begin(); itrm_sd != dc_include.end(); itrm_sd++)
	{
		prod = Cudd_ReadOne(*dd);
	    Cudd_Ref(prod);
		string dc = itrm_sd->first;	
//		cout << endl << "current dc = " << dc << endl;
		for(int i = 0; i < dc.size(); i++)
		{
			if(dc[i] == '-')
				continue;
			char *cnode = cutnodes[i];
			if(!st_lookup((net)->hash, cnode, &auxnd))
			{
				cout << "current node doesn't exixt in st_table!" << endl;
				exit(1);
			}
//			cout << "auxnd: " << auxnd->name << ", type = " << auxnd->type << endl;

			if (dc[i] == '1')
				var = auxnd->dd;
		    else 
				var = Cudd_Not(auxnd->dd);

			if(var == NULL) 
			{
				cout << "var is NULL!" << endl;
				exit(1);
			}
			
			tmp = Cudd_bddAnd(*dd, prod, var);
			if (tmp == NULL) 
			{
				cout << "tmp is NULL!" << endl;
				exit(1);
			}
			Cudd_Ref(tmp);
			Cudd_IterDerefBdd(*dd, prod);
//			Cudd_IterDerefBdd(*dd, var);
			prod = tmp;
		}
//		func = prod;
		double num_minterm = Cudd_CountMinterm(*dd, prod, numPI_ini);
//		cout << "num_minterm = " << num_minterm << endl;
//		Cudd_IterDerefBdd(*dd, func);
	    Cudd_IterDerefBdd(*dd, prod);
		double this_real_er = num_minterm/pow(2.0, numPI_ini);
		itrm_sd->second = this_real_er;
	}
}




/*exdc_real_er_v2()*/
double exdc_real_er_v2(BnetNetwork *net, DdManager **dd, vector<char*> &cutnodes, string &sexdc, int num_digit,  int iIndex)
{
	double this_real_er;
	BnetNode *nd, *auxnd;	
	DdNode *func;
	if(num_digit == 1)
	{
		char *cnode = cutnodes[0];
		st_lookup(net->hash, cnode, &nd);
		if(sexdc[0] == '1')
			this_real_er = nd->rp;
		else if(sexdc[0] == '0')
			this_real_er = 1 - nd->rp;
		return this_real_er;
	}
	else  
	{
		DdNode *prod = Cudd_ReadOne(*dd);
	    Cudd_Ref(prod);
//	    cout << "sexdc = " << sexdc << endl;
		for(int i = 0; i < sexdc.size(); i++)
		{
			if(sexdc[i] == '-')
				continue;
			char *cnode = cutnodes[i];
//			cout << "cnode: " << cnode << endl;
			if(!st_lookup((net)->hash, cnode, &auxnd))
			{
				cout << "current node doesn't exixt in st_table!" << endl;
				exit(1);
			}
			if(auxnd->dd == NULL)
			{
				cout << "auxnd->dd is NULL!" << endl;
				exit(1);
			}
			DdNode *var;
			if (sexdc[i] == '1')
				var = auxnd->dd;
		    else 
				var = Cudd_Not(auxnd->dd);
			if(var == NULL) 
			{
				cout << "var is NULL!" << endl;
				exit(1);
			}

			DdNode *tmp = Cudd_bddAnd((*dd), prod, var);
			if (tmp == NULL)
			{
				cout << "tmp is NULL!" << endl;
				exit(1);
			}			
			Cudd_Ref(tmp);
			Cudd_IterDerefBdd(*dd, prod);
//			Cudd_IterDerefBdd(*dd, var);
			prod = tmp;
		}

		double num_minterm = Cudd_CountMinterm(*dd, prod, numPI_ini);
//		cout << "num_minterm = " << num_minterm << endl;		
	    Cudd_IterDerefBdd(*dd, prod);
		double this_real_er = num_minterm/pow(2.0, numPI_ini);		
		return this_real_er;
	}
}


void exdc_new_min(BnetNetwork *net, DdManager **dd, char *cnode, double sp, multimap<double, string> &exdc_set, vector<string> &dont_care, vector<string> &org_pla)
{
    //iterators and variables
    multimap<double, string>::iterator itrmm_ds;
    
    struct timeb st, et;    
 
    map<int, int>::iterator itrmi, itrmi1;
    map<string, string>::iterator itrm_ss;
    map<int, Node*>::iterator itrm_in, itrm_in1;   
    map<string, double>::iterator itrm_sd, itrm_sd1;   
    BnetNode *nd, *tmp, *auxnd;
    map<string, double> dc_include;
        
	for(int i = 0; i < org_pla.size(); i++)
		exdc_set.insert(pair<double, string>(0, org_pla[i]));

    //Append the CODC to the exdc set.
    if (!dont_care.empty()) 
	{
		for(int i = 0; i < dont_care.size(); i++)
			exdc_set.insert(pair<double, string>(0, dont_care[i]));
   	} 
   	cout << "in exdc_new_min, exdc for this node: " << endl;
   	for(itrmm_ds = exdc_set.begin(); itrmm_ds != exdc_set.end(); itrmm_ds++)
   		cout  << itrmm_ds->second << endl;
   	cout << endl;
}



//exdc_new_v2
void exdc_new_v3(BnetNetwork *net, DdManager **dd, char *cnode, vector<char*> &cutnodes, multimap<double, string> &exdc_set, double threshold, vector<string> &dont_care, int iIndex)
{
//    cout << endl << "Start exdc_new: " << endl;
    //iterators and variables
    multimap<double, string>::iterator itrmm_ds;
    
    struct timeb st, et;    
 
    map<int, int>::iterator itrmi, itrmi1;
    map<string, string>::iterator itrm_ss;
    map<int, Node*>::iterator itrm_in, itrm_in1;   
    map<string, double>::iterator itrm_sd, itrm_sd1;   
    int i, j = 0, k = 0;
    double p = 1.0;
    int codc_size;
    BnetNode *nd, *tmp, *auxnd;
    map<string, double> dc_include;
    

    //Print cutnodes
    cout << "cut nodes: " <<endl;
    for(int i = 0; i < cutnodes.size(); i++)
    {
    	st_lookup(net->hash, cutnodes[i], &nd);
        cout << cutnodes[i] << " " << nd->p << endl;
    }
    cout << endl;	
    
    //Initialize outtype and codc_size
    char outtype[cutnodes.size()];
    for (i = 0; i < cutnodes.size(); i++) 
        outtype[i] = '-';    
    
    if (!dont_care.empty( )) 
		codc_size = dont_care.size();
    else 
    {
        codc_size = 1;
        cout << "There exists no don't cares for this node!" << endl;
    }
    bool ifmatch[codc_size];
    bool ifrealmatch[codc_size];
    for (i = 0; i < codc_size; i++)// Initialization
    {
        ifrealmatch[i] = 0;
        ifmatch[i] = 0;
    }

    //First loop determines dc_include
    j = 0;
    while (1)
    {
    	st_lookup(net->hash, cutnodes[j], &nd);
        outtype[j] = 48 + (nd->p == nd->rp); 
        if (!dont_care.empty())
        {
	        for(i = 0; i < codc_size; i++)
	        {	   
				string cdc = dont_care[i];
	            bool ckpattern = 1;
	            if (!ifrealmatch[i])// Check if it matches the pattern
	            {
	            	for(k = 0; k <= j; k++)
	                	if (outtype[k] != cdc[k]) 
							ckpattern=0;
	               		if(ckpattern)
	                    {
							ifmatch[i] = 1;
							dc_include.insert(pair<string, double>(cdc, -1));	
						}
	            }
	        }
	    }

		string sexdc;
        for(i = 0; i < cutnodes.size(); i++)
			sexdc.append(1, outtype[i]);
        if(outtype[j] == '0') 
        	outtype[j] = '1';
        else 
            outtype[j] = '0';                 
        j = j + 1;
        for(i = 0; i < codc_size; i++)
        {
        	if(ifmatch[i] == 1) 
				ifrealmatch[i] = 1;
            ifmatch[i] = 0;
        }
        if (j == cutnodes.size()) break;
    }//while(1)

    //Compute real er for each pattern in dc_include
//    cout << "Compute real error rate for dc_include:" << endl;
    dc_real_er_v2(net, dd, cutnodes, dc_include, iIndex);

    //Second while loop determines the final exdc_set
    for(i = 0; i < cutnodes.size(); i++) 
        outtype[i] = '-';
    j = 0;
    set<string> dc_matched_all;
    set<string>::iterator itrs;
    double last_sexdc_er = 1;
    multimap<double, string> exdc_set;
    map<int, string> same_er_exdc;
    while (1)
    {
    	st_lookup(net->hash, cutnodes[j], &nd);
        outtype[j] = 48 + (nd->p == nd->rp); 
		vector<string> dc_matched;
        if (!dont_care.empty())
        {
        	for(i = 0; i < codc_size; i++)
        	{	   
				string cdc = dont_care[i];
            	bool ckpattern=1;
                for(k = 0; k <= j; k++)
                	if (outtype[k] != cdc[k]) 
						ckpattern=0;
               		if(ckpattern)      //match
						dc_matched.push_back(cdc);
        	}
        }
		//obtain the real_er for this exdc  
//		cout << endl << "last_sexdc_er = " << last_sexdc_er << endl; 
        string sexdc;
		int num_digit = 0;
        for(i = 0; i < cutnodes.size(); i++)
        {
			if(outtype[i] == '0' || outtype[i] == '1')
				num_digit++;
			sexdc.append(1, outtype[i]);
	 	}
		cout << "$$sexdc: " << sexdc << endl;	
		double this_real_er;
		itrm_sd = dc_include.find(sexdc);
		if(itrm_sd != dc_include.end())
			this_real_er = itrm_sd->second;
		else	
			this_real_er = exdc_real_er_v2(net, dd, cutnodes, sexdc, num_digit, iIndex);
		cout << "this_real_er = " << this_real_er << endl;
		double this_real_er_org = this_real_er;
    	
		//Minus from this_real_er the signal probabilities of the included sdcs or odcs
		double include_dc_er = 0;
        for(int i = 0; i < dc_matched.size(); i++)
		{
			string dc = dc_matched[i];
			itrm_sd = dc_include.find(dc);
			include_dc_er += itrm_sd->second;
		}
		this_real_er -= include_dc_er;
		cout << "final this_real_er = " << this_real_er << endl;

		//check the real_er of another sexdc
        {           
	        string sexdc_v2(sexdc);
	        if(sexdc_v2[j] == '0')
	        	sexdc_v2[j] = '1';
	        else
	        	sexdc_v2[j] = '0';
	        cout << "$$sexdc_v2: " << sexdc_v2 << endl;		
	        double this_real_er_v2;
	        	        
			itrm_sd1 = dc_include.find(sexdc_v2);
			if(itrm_sd1 != dc_include.end())
				this_real_er_v2 = itrm_sd1->second;
			else	
				this_real_er_v2 = last_sexdc_er - this_real_er_org;
			cout << "this_real_er_v2 = " << this_real_er_v2 << endl;
			double this_real_er_v2_org = this_real_er_v2;
				
			vector<string> dc_matched_v2;
		    if (!dont_care.empty())
		    {
		    	for(int i = 0; i < codc_size; i++)
		        {	   
					string cdc = dont_care[i];
		            bool ckpattern=1;
		            for(k = 0; k <= j; k++)
		            	if (sexdc_v2[k] != cdc[k]) 
							ckpattern=0;
		               	if(ckpattern)      //match
							dc_matched_v2.push_back(cdc);
		        }
		    }
				
			double include_dc_er_v2 = 0;
		    for(int i = 0; i < dc_matched_v2.size(); i++)
			{
				string dc = dc_matched_v2[i];
				itrm_sd1 = dc_include.find(dc);
				include_dc_er_v2 += itrm_sd1->second;
			}
			this_real_er_v2 -= include_dc_er_v2;
			cout << "final this_real_er_v2 = " << this_real_er_v2 << endl;
			
			if(this_real_er == this_real_er_v2)
				cout << "they are the same!" << endl;
			
			if(this_real_er_v2 < this_real_er)
			{
				if(this_real_er_v2 <= threshold)
				{
					cout << "sexdc_v2 pushed!" << endl;
					last_sexdc_er = this_real_er_org;
			    	exdc_set.insert(pair<double, string>(this_real_er_v2, sexdc_v2));
				    dc_matched_all.insert(dc_matched_v2.begin(), dc_matched_v2.end());
				    threshold = threshold - this_real_er_v2;  
			        if(sexdc_v2[j] == '0') 
			            outtype[j] = '1';
			        else 
			            outtype[j] = '0';                 
			    }
			    else
			    	last_sexdc_er = this_real_er_org;
			}	
			else
			{
				if(this_real_er <= threshold)
				{
					cout << "sexdc pushed!" << endl;
					last_sexdc_er = this_real_er_v2_org;
			        exdc_set.insert(pair<double, string>(this_real_er, sexdc));
				    dc_matched_all.insert(dc_matched.begin(), dc_matched.end());
				    threshold = threshold - this_real_er;  
			        if(sexdc[j] == '0') 
			            outtype[j] = '1';
			        else 
			            outtype[j] = '0';       
			    } 
			    else
			    	last_sexdc_er = this_real_er_org;      
			}		
	    }
		j = j + 1;
        if (j == cutnodes.size()) break;
    }//while(1)
    

    //Append the CODC to the exdc set.
    if (!dont_care.empty()) 
    	for(i = 0; i < codc_size;i++)
    	{
        	itrs = dc_matched_all.find(dont_care[i]);
			if(itrs == dc_matched_all.end())
			{
				exdc_set.insert(pair<double, string>(0, dont_care[i]));
	//			cout << "dont_care " << dont_care[i] << " is pushed in." << endl;
			}
   		}   

}



//exdc_new_v2
void exdc_new_v3(BnetNetwork *net, DdManager **dd, char *cnode, vector<char*> &unsort_cutnodes, vector<string> &org_pla, vector<string> &dont_care, multimap<double, string> &exdc_set, double threshold, int iIndex)
{
    //iterators and variables
    multimap<double, string>::iterator itrmm_ds;
    
    cout << "org_pla: " << endl;
    for(int i = 0; i < org_pla.size(); i++)
    	cout << org_pla[i] << endl;
    cout << endl;
       
    multimap<double, string> can_exdc_set;
    int num_input = unsort_cutnodes.size();
    int num_minterm = (int)pow(2.0, num_input);
    vector<int> bin;
    for(int i = 0; i < num_minterm; i++)
    {
    	bin.clear();
    	int2bin(i, bin, num_input);
    	string sexdc;
    	for(int j = 0; j < num_input; j++)
    		if(bin[j])
    			sexdc.append(1, '1');
    		else
    			sexdc.append(1, '0');
    	
    	if(isIncludeVec(dont_care, sexdc))
    		continue;
    	if(isIncludeVec(org_pla, sexdc))
    		continue;
    	int num_digit = num_input;
    	double this_real_er = exdc_real_er_v2(net, dd, unsort_cutnodes, sexdc, num_digit, iIndex);
//    	cout << "this_real_er: " << this_real_er << endl;
		can_exdc_set.insert(pair<double, string>(this_real_er, sexdc));
    }
    
    for(itrmm_ds = can_exdc_set.begin(); itrmm_ds != can_exdc_set.end(); itrmm_ds++)
    {
    	if(threshold <= 0)
    		break;
    	if(itrmm_ds->first < threshold)
    	{
    		exdc_set.insert(pair<double, string>(itrmm_ds->first, itrmm_ds->second));
    		threshold -= itrmm_ds->first;
    	}
    }
    

    //Append dont_care to the exdc set.
    for(int i = 0; i < dont_care.size();i++)
		exdc_set.insert(pair<double, string>(0, dont_care[i])); 

}
