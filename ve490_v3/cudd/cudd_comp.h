#ifndef CUDD_COMP_H
#define CUDD_COMP_H

#include "../../cudd-2.5.0/cudd/cudd.h"
#include "../../cudd-2.5.0/cudd/cuddInt.h"

int cudd_comp(DdManager *dd, DdNode *node, int num_inputs, int *assign);

#endif
