#ifndef _CUDD_DST_H
#define _CUDD_DST_H

#include "../../cudd-2.5.0/cudd/cudd.h"
#include "../../cudd-2.5.0/cudd/cuddInt.h"

void cudd_dst(DdManager *dd);

#endif
