#ifndef _CUDD_BUILD_H
#define _CUDD_BUILD_H

#include "../../cudd-2.5.0/cudd/cudd.h"
#include "../../cudd-2.5.0/cudd/cuddInt.h"
#include "bnet.h"


void cudd_build(BnetNetwork **net, DdManager **dd, DdNode **dnode, const char *filename);

#endif
