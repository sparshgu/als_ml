#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <climits>
#include <vector>
#include <map>
#include <cmath>
#include <cassert>
#include <ctime>
#include <sys/timeb.h>
#include "head/queue.h"
#include "head/basics.h"
#include "head/write_func.h"
#include "head/comp_real_er.h"
#include "head/call_abc.h"
#include "head/helper.h"
#include "head/sim_new.h"
#include "cudd/bnet.h"
#include "cudd/cudd_build_v2.h"
#include "../cudd-2.5.0/cudd/cudd.h"
#include "../cudd-2.5.0/cudd/cuddInt.h"


using namespace std;

extern int numPI_ini;
int sample_num;

/*
functions in this file:

*/

//Global variables and external variables



/*1. loc_sim_ite*/
int loc_sim_ite(BnetNetwork *net, BnetNetwork *&net_comb, DdManager **dd_comb, map<string, struct score_pla> &sim_record, char *last_node, vector<char*> &last_inputs, double threshold, double &real_er, int &iIndex)
{
    //iterators and variables
    struct timeb startTime, endTime; 
    BnetNode *nd;
    multimap<double, char*> ave_sp;
    multimap<double, char*>::iterator itrm_dc;
    FILE *fp;
    
    cout <<"step0. net && net_comb: " << endl;
    cout << "net: " << endl;
    Bnet_PrintNetwork(net);
/*    cout << "net_comb: " << endl;
    if(iIndex > 0)
    	Bnet_PrintNetwork(net_comb);
*/
    
    /*Build BDD for the entire circuit: ckt_org.blif or ckt_sim.blif and compute signal probabilities*/
    cout << "****************************" << endl;
    cout << "step1. Build BDD for current network and compute signal probabilities: " << endl;
    ftime(&startTime);
    string path_bdd = "./blif_files/";
    string filename_bdd;
    if(iIndex == 0)
		filename_bdd = "ckt_org.blif";
    else
		filename_bdd = "ckt_sim.blif";
    path_bdd.append(filename_bdd);
    filename_bdd = path_bdd;
    DdManager *dd = NULL;
    dd = Cudd_Init(0, 0, CUDD_UNIQUE_SLOTS, CUDD_CACHE_SLOTS, 0);
    if (dd == NULL) return -1;      
    cudd_build_v2(net, &dd, filename_bdd.c_str(), BNET_GLOBAL_DD);
    nd = net->nodes;
    multimap<double, char*> node_sp;
    multimap<double, char*>::iterator itrmm_dc;
    while(nd != NULL)
    {
    	double num_minterm = Cudd_CountMinterm(dd, nd->dd, numPI_ini);
    	double sp = num_minterm/pow(2.0, numPI_ini);
    	nd->rp = sp;
    	nd->p = findmin(sp);
    	node_sp.insert(pair<double, char*>(nd->p, nd->name));
//    	int sup = Cudd_SupportSize(dd, nd->dd);
//    	cout << "node " << nd->name << ", sp: " << sp << ", # of fanouts: " << nd->nfo << ", sup: " << sup << endl;    	
    	nd = nd->next;
    }
    cout << "node_sp: " << endl;
    for(itrmm_dc = node_sp.begin(); itrmm_dc != node_sp.end(); itrmm_dc++)
    	cout << "node << " << itrmm_dc->second << ", p: " << itrmm_dc->first << endl;
    ftime(&endTime);
    double rt_step1 = ((endTime.time - startTime.time)*1000 + (endTime.millitm - startTime.millitm))/1000.0;
    cout << "runtime for compute signal probabilities: " << rt_step1 << endl;
    
       

    //Find exdcs for each big node and simplify them using these exdcs. Obtain 
    //the one with maximum node save
    cout << "****************************" << endl;
    cout << "step2. find_exdc_sim: " << endl;	
    ftime(&startTime);    
    vector<string> final_pla;  //store the files for the simplifed big node
    double max_score = -1;
    char *max_save_node = find_exdc_sim(net, &dd, net_comb, dd_comb, final_pla, sim_record, last_node, last_inputs, max_score, threshold, iIndex);     
    ftime(&endTime);
    double rt_step4 = ((endTime.time - startTime.time)*1000 + (endTime.millitm - startTime.millitm))/1000.0;
    cout << "runtime for find_exdc_sim: " << rt_step4 << endl;     
    Cudd_Quit(dd);
    

    if(iIndex > 0)  
    {
    	Bnet_FreeNetwork(net_comb);
    	Cudd_Quit(*dd_comb);
    } 
     
    if(max_score <= 0)
    {
	    return -1;
    }
    

    //Write the whole simplified circuit into ckt_sim.blif
    cout << "****************************" << endl;
    cout << "step5. write_ckt_sim && sweep && map: " << endl;
    ftime(&startTime);
    write_ckt_sim(net, max_save_node, final_pla);
	cout << "call SIS to sweep ckt_sim further" << endl;  
	char com[100];      
    sprintf(com, "sis -t none -f ./script/sim_ckt_v2.rug > sis_ckt.txt");  //sweep
    system(com); 
    cout <<  "Current stats by SIS: " << endl;        
//    sprintf(com, "sis -t none -f ./script/map.rug");
	sprintf(com, "sis -t none -f ./script/map0.rug");
    system(com);
    Bnet_FreeNetwork(net);
    update_ckt(net);      
    ftime(&endTime);
    double rt_step6 = ((endTime.time - startTime.time)*1000 + (endTime.millitm - startTime.millitm))/1000.0;
    cout << "runtime for step5: " << rt_step6 << endl;  

    //Build bdd to compute the real error rate
	cout << "****************************" << endl;
    cout << "step6. Write ckt_org_sim.blif and build bdd to compute real error rate: " << endl;
    ftime(&startTime);
    string filename = "./blif_files/ckt_org_sim.blif";
    write_ckt_comb(net);
    fp = fopen(filename.c_str(), "r");
    cout << "read combined network: " << endl;
    net_comb = Bnet_ReadNetwork(fp); 
    fclose(fp);  
    *dd_comb = NULL;
    *dd_comb = Cudd_Init(0, 0, CUDD_UNIQUE_SLOTS, CUDD_CACHE_SLOTS, 0);    
	cudd_build_v2(net_comb, dd_comb, filename.c_str(), BNET_GLOBAL_DD);
	char *out_node = net_comb->outputs[0];
	st_lookup(net_comb->hash, out_node, &nd);
	double num_diff = Cudd_CountMinterm(*dd_comb, nd->dd, numPI_ini);
//	cout << "erroneous input patterns: " << endl;
//	int res = Cudd_PrintDebug(*dd_comb, nd->dd, numPI_ini, 2);	
	real_er = num_diff/pow(2.0, numPI_ini);
	ftime(&endTime);
    double rt_step7 = ((endTime.time - startTime.time)*1000 + (endTime.millitm - startTime.millitm))/1000.0;
    cout << "runtime for step6: " << rt_step7 << endl;  
    

    
    return 0;
}





/*2. loc_sim_main()*/
void loc_sim_main(BnetNetwork *net, string &filename, double threshold)
{
    struct timeb st, et; 
	double threshold_org = threshold;
    int iIndex = 0;
    char com[100];
    string str;
        
    write_ckt_org(net);   //add buffers to output nodes
    sprintf(com, "cp ./blif_files/ckt_org.blif ./blif_files/ckt_org0.blif");
    system(com);
//    sprintf(com, "sis -t none -f ./script/sim_part_org.rug > sis.txt");
//    system(com);   
    
    ifstream fin;
    fin.open("./blif_files/ckt_org.blif", ios::in);
    cout << "modified ckt_org: " << endl;
    while(getline(fin, str))
    	cout << str << endl;
    cout << endl;    
    fin.close();
    
    Bnet_FreeNetwork(net);
    FILE *fp;
    fp = fopen("./blif_files/ckt_org.blif", "r");
    net = Bnet_ReadNetwork(fp);
    fclose(fp);
    
    //If this is the first iteration, write the inital circuit into ckt_org.blif
/*    char new_filename[100] = "./blif_files/ckt_org.blif";    
    sprintf(com, "cp %s %s", filename.c_str(), new_filename);
    system(com);
    sprintf(com, "sed -i '/^.model/d' ./blif_files/ckt_org.blif");
    system(com);
    sprintf(com, "sed -i '1i .model ckt_org' ./blif_files/ckt_org.blif");
    system(com);
*/
    
    
    //start the iteration of local simplification
    double real_er = 0;
    cout << "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$" << endl;
    cout << "i" << iIndex << ". iterations start: " << endl;
    cout << "threshold = " << threshold << endl;
	char *last_node = new char[50];
	vector<char*> last_inputs;
	BnetNetwork *net_comb;
	DdManager *dd_comb;
	map<string, struct score_pla> sim_record;
	map<string, struct score_pla>::iterator itrm_ss;
    ftime(&st);  
    int res = loc_sim_ite(net, net_comb, &dd_comb, sim_record, last_node, last_inputs, threshold, real_er, iIndex);
    ftime(&et);
    double rt_loc_sim_ite = ((et.time - st.time)*1000 + (et.millitm - st.millitm))/1000.0;
    cout << "i" << iIndex << ". runtime for loc_sim_ite: " << rt_loc_sim_ite << endl;

    if(res == -1)
    {
    	cout << "max_save = 0. No more optimization!" << endl;
    	delete []last_node;
    	for(int i = 0; i < last_inputs.size(); i++)
    		delete []last_inputs[i];
    	return;
    }
    cout << endl << "##############################################" << endl;
    cout << "report: " << endl;
    cout << "real_er = " << real_er << endl;    
	
    
    iIndex++;
    threshold =  threshold_org -  real_er; 
//	cout << "updated threshold = " << threshold << endl;
    
    		
    while(real_er < threshold_org)
    {
        cout << "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$" << endl;
    	cout << "i" << iIndex << ". iterations start: " << endl;
        ftime(&st);  
        cout << "sim_record: " << sim_record.size() << endl;
        for(itrm_ss = sim_record.begin(); itrm_ss != sim_record.end(); itrm_ss++)
        	cout << itrm_ss->first << ": real_er: " << itrm_ss->second.real_er << ", score: " << itrm_ss->second.score << endl;
        res = loc_sim_ite(net, net_comb, &dd_comb, sim_record, last_node, last_inputs, threshold, real_er, iIndex);
	    ftime(&et);
    	rt_loc_sim_ite = ((et.time - st.time)*1000 + (et.millitm - st.millitm))/1000.0;
    	cout << "i" << iIndex << ". runtime for loc_sim_ite: " << rt_loc_sim_ite << endl;
    	    
    	if(res == -1)
	    {
			cout << "max_save = 0. No more optimization!" << endl;
			delete []last_node;
			for(int i = 0; i < last_inputs.size(); i++)
    			delete []last_inputs[i];
			return;
	    }	
	       
        cout << endl << "##############################################" << endl;
		cout << "report: " << endl;
		cout << "real_er = " << real_er << endl;    

	                        	    
	    iIndex++;
	    threshold = threshold_org -  real_er; 
//	    cout << "updated threshold = " << threshold << endl;
	    	    	    		    	
    }
        
    delete []last_node;
    return;

}
