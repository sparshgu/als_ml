#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <climits>
#include <vector>
#include <map>
#include <cmath>
#include <cassert>
#include <ctime>
#include <sys/timeb.h>
#include "head/queue.h"
#include "head/basics.h"
#include "head/helper.h"
#include "head/read_file.h"
#include "head/write_func.h"
#include "head/exdc_helper.h"
#include "/home/wuyi/usr/CUDD/cudd-2.5.0/cudd/cudd.h"
#include "/home/wuyi/usr/CUDD/cudd-2.5.0/cudd/cuddInt.h"
#include "cudd/cudd_build.h"
#include "cudd/cudd_comp.h"
#include "cudd/cudd_dst.h"

using namespace std;

extern int numPI_ini;

/*
functions in this file:

*/

//Global variables and external variables 


/*dc_real_er_v2()*/
void dc_real_er_v2(BnetNetwork *net, DdManager **dd, vector<char*> &cutnodes, map<string, double> &dc_include)
{
	set<string>::iterator itrs, itrs1;
	map<string, double>::iterator itrm_sd;
	
    /*Build bdds for don't cares in dc_include*/    	
    double total_dc_er = 0;
	char com[100];
	BnetNode *auxnd;
	DdNode *tmp;
    DdNode *func, *prod, *var, *var_tmp;
	for(itrm_sd = dc_include.begin(); itrm_sd != dc_include.end(); itrm_sd++)
	{
		prod = Cudd_ReadOne(*dd);
	    Cudd_Ref(prod);
		string dc = itrm_sd->first;	
//		cout << endl << "current dc = " << dc << endl;
		for(int i = 0; i < dc.size(); i++)
		{
			if(dc[i] == '-')
				continue;
			char *cnode = cutnodes[i];
			if(!st_lookup((net)->hash, cnode, &auxnd))
			{
				cout << "current node doesn't exixt in st_table!" << endl;
				exit(1);
			}
//			cout << "auxnd: " << auxnd->name << ", type = " << auxnd->type << endl;

			if (dc[i] == '1')
				var = auxnd->dd;
		    else 
				var = Cudd_Not(auxnd->dd);

			if(var == NULL) 
			{
				cout << "var is NULL!" << endl;
				exit(1);
			}
			
			tmp = Cudd_bddAnd(*dd, prod, var);
			if (tmp == NULL) 
			{
				cout << "tmp is NULL!" << endl;
				exit(1);
			}
			Cudd_Ref(tmp);
			Cudd_IterDerefBdd(*dd, prod);
//			Cudd_IterDerefBdd(*dd, var);
			prod = tmp;
		}
//		func = prod;
		double num_minterm = Cudd_CountMinterm(*dd, prod, numPI_ini);
//		cout << "num_minterm = " << num_minterm << endl;
//		Cudd_IterDerefBdd(*dd, func);
	    Cudd_IterDerefBdd(*dd, prod);
		double this_real_er = num_minterm/pow(2.0, numPI_ini);
		itrm_sd->second = this_real_er;
	}
}




/*exdc_real_er_v2()*/
double exdc_real_er_v2(BnetNetwork *net, DdManager **dd, vector<char*> &cutnodes, string &sexdc, int num_digit)
{
	double this_real_er;
	BnetNode *nd, *auxnd;	
	DdNode *func;
	if(num_digit == 1)
	{
		char *cnode = cutnodes[0];
		st_lookup(net->hash, cnode, &nd);
		if(sexdc[0] == '1')
			this_real_er = nd->rp;
		else if(sexdc[0] == '0')
			this_real_er = 1 - nd->rp;
		return this_real_er;
	}
	else  
	{
		DdNode *prod = Cudd_ReadOne(*dd);
	    Cudd_Ref(prod);
//	    cout << "sexdc = " << sexdc << endl;
		for(int i = 0; i < sexdc.size(); i++)
		{
			if(sexdc[i] == '-')
				continue;
			char *cnode = cutnodes[i];
//			cout << "cnode: " << cnode << endl;
			if(!st_lookup((net)->hash, cnode, &auxnd))
			{
				cout << "current node doesn't exixt in st_table!" << endl;
				exit(1);
			}
			if(auxnd->dd == NULL)
			{
				cout << "auxnd->dd is NULL!" << endl;
				exit(1);
			}
			DdNode *var;
			if (sexdc[i] == '1')
				var = auxnd->dd;
		    else 
				var = Cudd_Not(auxnd->dd);
			if(var == NULL) 
			{
				cout << "var is NULL!" << endl;
				exit(1);
			}

			DdNode *tmp = Cudd_bddAnd((*dd), prod, var);
			if (tmp == NULL)
			{
				cout << "tmp is NULL!" << endl;
				exit(1);
			}			
			Cudd_Ref(tmp);
			Cudd_IterDerefBdd(*dd, prod);
//			Cudd_IterDerefBdd(*dd, var);
			prod = tmp;
		}

		double num_minterm = Cudd_CountMinterm(*dd, prod, numPI_ini);
//		cout << "num_minterm = " << num_minterm << endl;		
	    Cudd_IterDerefBdd(*dd, prod);
		double this_real_er = num_minterm/pow(2.0, numPI_ini);		
		return this_real_er;
	}
}


void exdc_new_min(BnetNetwork *net, DdManager **dd, char *cnode, double sp, multimap<double, string> &exdc_set, vector<string> &dont_care, vector<string> &org_pla)
{
    //iterators and variables
    multimap<double, string>::iterator itrmm_ds;
    
    struct timeb st, et;    
 
    map<string, double>::iterator itrm_sd, itrm_sd1;   
    BnetNode *nd, *tmp, *auxnd;
    map<string, double> dc_include;
        
	for(int i = 0; i < org_pla.size(); i++)
		exdc_set.insert(pair<double, string>(0, org_pla[i]));

    //Append the CODC to the exdc set.
    if (!dont_care.empty()) 
	{
		for(int i = 0; i < dont_care.size(); i++)
			exdc_set.insert(pair<double, string>(0, dont_care[i]));
   	} 
   	cout << "in exdc_new_min, exdc for this node: " << endl;
   	for(itrmm_ds = exdc_set.begin(); itrmm_ds != exdc_set.end(); itrmm_ds++)
   		cout  << itrmm_ds->second << endl;
   	cout << endl;
}



//exdc_new_v2
void exdc_new_v2(BnetNetwork *net, DdManager **dd, char *cnode, vector<char*> &cutnodes, multimap<double, string> &exdc_set, double threshold, vector<string> &dont_care)
{
//    cout << endl << "Start exdc_new: " << endl;
    //iterators and variables
    multimap<double, string>::iterator itrmm_ds;
    
    struct timeb st, et;    
 
    map<int, int>::iterator itrmi, itrmi1;
    map<string, string>::iterator itrm_ss;
    map<int, Node*>::iterator itrm_in, itrm_in1;   
    map<string, double>::iterator itrm_sd, itrm_sd1;   
    int i, j = 0, k = 0;
    double p = 1.0;
    int codc_size;
    BnetNode *nd, *tmp, *auxnd;
    map<string, double> dc_include;
    

    //Print cutnodes
    cout << "cut nodes: " <<endl;
    for(int i = 0; i < cutnodes.size(); i++)
    {
    	st_lookup(net->hash, cutnodes[i], &nd);
        cout << cutnodes[i] << " " << nd->p << endl;
    }
    cout << endl;	
    
    //Initialize outtype and codc_size
    char outtype[cutnodes.size()];
    for (i = 0; i < cutnodes.size(); i++) 
        outtype[i] = '-';    
    
    if (!dont_care.empty( )) 
		codc_size = dont_care.size();
    else 
    {
        codc_size = 1;
        cout << "There exists no don't cares for this node!" << endl;
    }
    bool ifmatch[codc_size];
    bool ifrealmatch[codc_size];
    for (i = 0; i < codc_size; i++)// Initialization
    {
        ifrealmatch[i] = 0;
        ifmatch[i] = 0;
    }

    //First loop determines dc_include
    j = 0;
    while (1)
    {
    	st_lookup(net->hash, cutnodes[j], &nd);
        outtype[j] = 48 + (nd->p == nd->rp); 
        if (!dont_care.empty())
        {
	        for(i = 0; i < codc_size; i++)
	        {	   
				string cdc = dont_care[i];
	            bool ckpattern = 1;
	            if (!ifrealmatch[i])// Check if it matches the pattern
	            {
	            	for(k = 0; k <= j; k++)
	                	if (outtype[k] != cdc[k]) 
							ckpattern=0;
	               		if(ckpattern)
	                    {
							ifmatch[i] = 1;
							dc_include.insert(pair<string, double>(cdc, -1));	
						}
	            }
	        }
	    }

		string sexdc;
        for(i = 0; i < cutnodes.size(); i++)
			sexdc.append(1, outtype[i]);
        if(outtype[j] == '0') 
        	outtype[j] = '1';
        else 
            outtype[j] = '0';                 
        j = j + 1;
        for(i = 0; i < codc_size; i++)
        {
        	if(ifmatch[i] == 1) 
				ifrealmatch[i] = 1;
            ifmatch[i] = 0;
        }
        if (j == cutnodes.size()) break;
    }//while(1)

    //Compute real er for each pattern in dc_include
//    cout << "Compute real error rate for dc_include:" << endl;
    dc_real_er_v2(net, dd, cutnodes, dc_include);

    //Second while loop determines the final exdc_set
    for(i = 0; i < cutnodes.size(); i++) 
        outtype[i] = '-';
    j = 0;
    set<string> dc_matched_all;
    set<string>::iterator itrs;
    double last_sexdc_er = 1;
    while (1)
    {
    	st_lookup(net->hash, cutnodes[j], &nd);
        outtype[j] = 48 + (nd->p == nd->rp); 
		vector<string> dc_matched;
        if (!dont_care.empty())
        {
        	for(i = 0; i < codc_size; i++)
        	{	   
				string cdc = dont_care[i];
            	bool ckpattern=1;
                for(k = 0; k <= j; k++)
                	if (outtype[k] != cdc[k]) 
						ckpattern=0;
               		if(ckpattern)      //match
						dc_matched.push_back(cdc);
        	}
        }
		//obtain the real_er for this exdc  
		cout << endl << "last_sexdc_er = " << last_sexdc_er << endl; 
        string sexdc;
		int num_digit = 0;
        for(i = 0; i < cutnodes.size(); i++)
        {
			if(outtype[i] == '0' || outtype[i] == '1')
				num_digit++;
			sexdc.append(1, outtype[i]);
	 	}
		cout << "$$sexdc: " << sexdc << endl;	
		double this_real_er;
		itrm_sd = dc_include.find(sexdc);
		if(itrm_sd != dc_include.end())
			this_real_er = itrm_sd->second;
		else	
			this_real_er = exdc_real_er_v2(net, dd, cutnodes, sexdc, num_digit);
		cout << "this_real_er = " << this_real_er << endl;
		double this_real_er_org = this_real_er;
    	
		//Minus from this_real_er the signal probabilities of the included sdcs or odcs
		double include_dc_er = 0;
        for(int i = 0; i < dc_matched.size(); i++)
		{
			string dc = dc_matched[i];
			itrm_sd = dc_include.find(dc);
			include_dc_er += itrm_sd->second;
		}
		this_real_er -= include_dc_er;
		cout << "final this_real_er = " << this_real_er << endl;

		if(this_real_er == 0)
	    {
	    	cout << "pushed!" << endl;
	      //  exdc_set.push_back(sexdc);
	      	exdc_set.insert(pair<double, string>(this_real_er, sexdc));
		    dc_matched_all.insert(dc_matched.begin(), dc_matched.end());
		    threshold = threshold - this_real_er;  
	        if(sexdc[j] == '0') 
	        	outtype[j] = '1';
	        else 
	        	outtype[j] = '0';                 
	    }
		else  //check the real_er of another sexdc
        {           
	        string sexdc_v2(sexdc);
	        if(sexdc_v2[j] == '0')
	        	sexdc_v2[j] = '1';
	        else
	        	sexdc_v2[j] = '0';
	        cout << "$$sexdc_v2: " << sexdc_v2 << endl;		
	        double this_real_er_v2;
	        
	        
			itrm_sd1 = dc_include.find(sexdc_v2);
			if(itrm_sd1 != dc_include.end())
				this_real_er_v2 = itrm_sd1->second;
			else	
//				this_real_er_v2 = exdc_real_er_v2(net, dd, cutnodes, sexdc_v2, num_digit);
				this_real_er_v2 = last_sexdc_er - this_real_er_org;
			cout << "this_real_er_v2 = " << this_real_er_v2 << endl;
			double this_real_er_v2_org = this_real_er_v2;
				
			vector<string> dc_matched_v2;
		    if (!dont_care.empty())
		    {
		    	for(int i = 0; i < codc_size; i++)
		        {	   
					string cdc = dont_care[i];
		            bool ckpattern=1;
		            for(k = 0; k <= j; k++)
		            	if (sexdc_v2[k] != cdc[k]) 
							ckpattern=0;
		               	if(ckpattern)      //match
							dc_matched_v2.push_back(cdc);
		        }
		    }
				
			double include_dc_er_v2 = 0;
		    for(int i = 0; i < dc_matched_v2.size(); i++)
			{
				string dc = dc_matched_v2[i];
				itrm_sd1 = dc_include.find(dc);
				include_dc_er_v2 += itrm_sd1->second;
			}
			this_real_er_v2 -= include_dc_er_v2;
			cout << "final this_real_er_v2 = " << this_real_er_v2 << endl;
			
			if(this_real_er == this_real_er_v2)
				cout << "they are the same!" << endl;
			
			if(this_real_er_v2 < this_real_er)
			{
				if(this_real_er_v2 <= threshold)
				{
					cout << "sexdc_v2 pushed!" << endl;
					last_sexdc_er = this_real_er_org;
			    //    exdc_set.push_back(sexdc_v2);
			    	exdc_set.insert(pair<double, string>(this_real_er_v2, sexdc_v2));
//			        include_exdc_er += this_real_er_v2;
//				    dc_matched_all.insert(dc_matched_v2.begin(), dc_matched_v2.end());
				    threshold = threshold - this_real_er_v2;  
			        if(sexdc_v2[j] == '0') 
			            outtype[j] = '1';
			        else 
			            outtype[j] = '0';                 
			    }
			    else
			    	last_sexdc_er = this_real_er_org;
			}	
			else
			{
				if(this_real_er <= threshold)
				{
					cout << "sexdc pushed!" << endl;
					last_sexdc_er = this_real_er_v2_org;
			   //     exdc_set.push_back(sexdc);
			        exdc_set.insert(pair<double, string>(this_real_er, sexdc));
//			        include_exdc_er += this_real_er;
				    dc_matched_all.insert(dc_matched.begin(), dc_matched.end());
				    threshold = threshold - this_real_er;  
			        if(sexdc[j] == '0') 
			            outtype[j] = '1';
			        else 
			            outtype[j] = '0';       
			    } 
			    else
			    	last_sexdc_er = this_real_er_org;      
			}		
	    }
		j = j + 1;
        if (j == cutnodes.size()) break;
    }//while(1)


    //Append the CODC to the exdc set.
    if (!dont_care.empty()) 
    	for(i = 0; i < codc_size;i++)
    	{
        	itrs = dc_matched_all.find(dont_care[i]);
			if(itrs == dc_matched_all.end())
			{
				exdc_set.insert(pair<double, string>(0, dont_care[i]));
	//			cout << "dont_care " << dont_care[i] << " is pushed in." << endl;
			}
   		}   

}



//exdc_new_v3
void exdc_new_v3(BnetNetwork *net, DdManager **dd, char *cnode, vector<char*> &unsort_cutnodes, vector<string> &org_pla, multimap<double, string> &exdc_set, double threshold, vector<string> &dont_care)
{
//    cout << endl << "Start exdc_new: " << endl;
    //iterators and variables
    multimap<double, string>::iterator itrmm_ds;
    
    struct timeb st, et;    
 
    map<string, double>::iterator itrm_sd, itrm_sd1;   
    int i, j = 0, k = 0;
    double p = 1.0;
    int codc_size;
    BnetNode *nd, *tmp, *auxnd;
    map<string, double> dc_include;
    
    cout << "unsort_cutnodes: " << endl;
	for(int i = 0; i < unsort_cutnodes.size(); i++)
	    cout << unsort_cutnodes[i] << " ";
	cout << endl;
    
     
    st_lookup(net->hash, cnode, &nd);
    int num_input = nd->ninp;
    int num_total = (int)pow(2.0, num_input);
    map<string, double> exdc_cand;
    string sexdc;
    vector<int> bin;
    set<int> dont_care_int;
    set<int>::iterator itrs;
    for(int i = 0; i < dont_care.size(); i++)
    {
    	string str = dont_care[i];
    	int int_value = 0;
    	for(int j = num_input-1; j >= 0; j--)
    		if(str[j] == '1')
    		{
    			int this_value = pow(2.0, (num_input-1-j));
    			int_value += this_value;
    		}
    	dont_care_int.insert(int_value);
//    	cout << "dc: " << str << ", int_value = " << int_value << endl;
    }
    for(int i = 0; i < num_total; i++)
    {
    	
    	itrs = dont_care_int.find(i);
    	if(itrs != dont_care_int.end())         //ignore the exdcs in dont_care
    		continue;
    	bin.clear();
//    	cout << "i = " << i << endl;
    	int2bin(i, bin, num_input);
    	sexdc.clear();
//    	cout << "bin: " << endl;
    	for(int j = 0; j < bin.size(); j++)
    	{
//    		cout << bin[j];
    		if(bin[j] == 0)
    			sexdc.append(1, '0');
    		else
    			sexdc.append(1, '1');
    	}
//    	cout << endl;
//		cout << "sexdc = " << sexdc << endl;
/*		if(isIncludeVec(org_pla, sexdc))
		{
			continue;
		}
*/
    	double this_real_er = exdc_real_er_v2(net, dd, unsort_cutnodes, sexdc, num_input);
    	cout << "sexdc = " << sexdc << ", this_real_er = " << this_real_er << endl;
    	if(this_real_er <= threshold)
    		exdc_cand.insert(pair<string, double>(sexdc, this_real_er));
    }
    
    multimap<double, string> exdc_score;
    multimap<double, string>::iterator itrm_ds;
    k = 0;
    for(itrm_sd = exdc_cand.begin(); itrm_sd != exdc_cand.end(); itrm_sd++, k++)
    {
    	string sexdc = itrm_sd->first;
    	double this_real_er = itrm_sd->second;
    	double dist = 0;
    	for(int i = 0; i < org_pla.size(); i++)
    	{
    		string cube = org_pla[i];
    		int each_dist = measure_dist(cube, sexdc);
    		dist += each_dist;    		
    	}
    	double score = this_real_er * dist;
    	exdc_score.insert(pair<double, string>(score, sexdc));
    	cout << k << ". " << sexdc << ": dist = " << dist << ", real_er = " << this_real_er << ", score = " << score << endl;
    }
	for(itrm_ds = exdc_score.begin(); itrm_ds != exdc_score.end(); itrm_ds++)
	{
		double score = itrm_ds->first;
		string sexdc = itrm_ds->second;
		itrm_sd = exdc_cand.find(sexdc);
		double this_real_er = itrm_sd->second;
		if(this_real_er < threshold)
		{
			exdc_set.insert(pair<double, string>(this_real_er, sexdc));
//			cout << "(" << sexdc  << ", " << this_real_er<< ") is inserted." << endl;
			threshold -= this_real_er;
		}
	}
	

    //Append the don't cares to the exdc set.
    if (!dont_care.empty()) 
    	for(int i = 0; i < dont_care.size(); i++)
			exdc_set.insert(pair<double, string>(0, dont_care[i]));
}



//exdc_new_v4: MSEOP
void exdc_new_v4(BnetNetwork *net, DdManager **dd, char *cnode, vector<char*> &unsort_cutnodes, vector<string> &org_pla, vector<string> &dont_care, vector<string> &local_dc_set, multimap<double, string> &exdc_set, double threshold)
{
//    cout << endl << "Start exdc_new: " << endl;
    //iterators and variables
    struct timeb st, et;   
    multimap<double, string>::iterator itrmm_ds; 
    map<string, double>::iterator itrm_sd, itrm_sd1;   
    int i, j = 0, k = 0;
    int this_area_save;
    double score;
    BnetNode *nd, *tmp, *auxnd;
        
    cout << endl << "unsort_cutnodes: " << endl;
	for(int i = 0; i < unsort_cutnodes.size(); i++)
	{
	    cout << unsort_cutnodes[i] << " ";
	    st_lookup(net->hash, unsort_cutnodes[i], &nd);
	    cout << nd->rp << endl;
	}
	cout << endl;
	
	cout << endl << "org_pla: " << endl;
	for(int i = 0; i < org_pla.size(); i++)
	    cout << org_pla[i] << " ";
	cout << endl;
	
	//set ini_exdc_set	
	multimap<double, string> ini_exdc_set;	
	for(int i = 0; i < dont_care.size(); i++)
		ini_exdc_set.insert(pair<double, string>(0, dont_care[i]));
	for(int i = 0; i < local_dc_set.size(); i++)
		ini_exdc_set.insert(pair<double, string>(0, local_dc_set[i]));

/*	//step1: call espresso to simplify the node with only dont_care and local_dc_set
	exdc_set = ini_exdc_set;
	int this_area_save = get_save(net, cnode, exdc_set);
	cout << "0. this_area_save = " << this_area_save << endl;
	if(this_area_save > 0)
		return;	
	else
*/	
	{		
		multimap<double, multimap<double, string> > score_map;
		multimap<double, multimap<double, string> >::iterator itrm_dm;
		st_lookup(net->hash, cnode, &nd);
    	int num_input = nd->ninp;
    	{
    		if(org_pla.size() == 1)
    		{
    			cout << "num_pla = 1" << endl;    	
    			cout << "factor form of bignode: " << endl;		
    			exdc_set.clear();
				write_bignode_pla(net, cnode, exdc_set);
				char com[100];   			                        
				sprintf(com, "sis -t none -f ./script/print_factor_org.rug");
				system(com);
    			if(nd->rp <= threshold)
				{
				    exdc_set = ini_exdc_set;
				    exdc_set.insert(pair<double, string>(nd->rp, org_pla[0]));
				    this_area_save = get_save(net, cnode, exdc_set);
					score = this_area_save / nd->rp;
					cout << "exdc: " << endl;
					cout << org_pla[0] << ": " << nd->rp << endl;
					cout << "this_area_save = " << this_area_save << endl;   					
					cout << "score = " << score << endl; 
					score_map.insert(pair<double, multimap<double, string> >(score, exdc_set));  
				}
    			find_cand_one(net, dd, cnode, unsort_cutnodes, score_map, ini_exdc_set, org_pla[0], nd->rp, threshold);    	
    			cout << "score_map: " << endl;
    			for(itrm_dm = score_map.begin(); itrm_dm != score_map.end(); itrm_dm++)
    			{
    				score = itrm_dm->first;
    				cout << "this_score = " << score << endl;
    				multimap<double, string> this_exdc_set = itrm_dm->second;
    				for(itrmm_ds = this_exdc_set.begin(); itrmm_ds != this_exdc_set.end(); itrmm_ds++)
    					cout << itrmm_ds->second << ": " << itrmm_ds->first << endl;
    			}
    		}
    		else
	    	{
	    		cout << "num_pla > 1" << endl;
	    		//sort org_pla
	    		multimap<int, string> sorted_pla;
	    		cout << "sorted_pla: " << endl;	    		
	    		for(int i = 0; i < org_pla.size(); i++)
	    		{
	    			string str = org_pla[i];
	    			int num_digit = 0;
	    			for(int j = 0; j < str.size(); j++)
	    				if(str[j] != '-')
	    					num_digit++;
	    			sorted_pla.insert(pair<int, string>(num_digit, str));
	    			cout << str << endl;
	    		}
	    		cout << "factor form of bignode: " << endl;
	    		exdc_set.clear();
				write_bignode_pla(net, cnode, exdc_set);
				char com[100];   			                        
				sprintf(com, "sis -t none -f ./script/print_factor_org.rug");
				system(com);
				vector<string> unique_minterms;
				//find unique minterm for each pla and if the error rate is within the threshold, make it a candidate
	    		find_unique_minterms(sorted_pla, dont_care, local_dc_set, unique_minterms);
	    		for(int i = 0; i < unique_minterms.size(); i++)
	    		{
	    			string cstr = unique_minterms[i];
	    			cout << "current um = " << cstr << endl; 	    			
	    			double this_real_er = exdc_real_er_v2(net, dd, unsort_cutnodes, cstr, num_input);
	    			cout << "this_real_er = " << this_real_er << endl;
	    	//		if(this_real_er <= threshold)
	    			{
	    				exdc_set = ini_exdc_set;
		    			exdc_set.insert(pair<double, string>(this_real_er, cstr));
		    			cout << "this_exdc_set: " << endl;
		    			for(itrmm_ds = exdc_set.begin(); itrmm_ds != exdc_set.end(); itrmm_ds++)
    						cout << itrmm_ds->second << ": " << itrmm_ds->first << endl;
		    			this_area_save = get_save(net, cnode, exdc_set);
			    		score = this_area_save / this_real_er;
						cout << "this_area_save = " << this_area_save << endl;   					
						cout << "score = " << score << endl; 
						score_map.insert(pair<double, multimap<double, string> >(score, exdc_set)); 
	    			}
	    		}
	    		//
/*	    		for(int i = 0; i < org_pla.size(); i++)
	    		{
	    		//	find_cand_cube(net, dd, cnode, unsort_cutnodes, score_map, ini_exdc_set, org_pla[i], nd->rp, threshold); 
	    		}
*/
	    	}
    	}
	}//end of else
	  
}

