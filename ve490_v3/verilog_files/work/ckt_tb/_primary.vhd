library verilog;
use verilog.vl_types.all;
entity ckt_tb is
    generic(
        M               : integer := 50;
        N               : integer := 22;
        snum            : integer := 18
    );
end ckt_tb;
