// Benchmark "ckt_sim" written by ABC on Mon Jul 27 02:18:13 2015

module ckt_sim ( 
    n1, n8, n13, n17, n26, n29, n36, n42, n51, n55, n59, n68, n72, n73,
    n74, n75, n80, n85, n86, n87, n88, n89, n90, n91, n96, n101, n106,
    n111, n116, n121, n126, n130, n135, n138, n143, n146, n149, n152, n153,
    n156, n159, n165, n171, n177, n183, n189, n195, n201, n207, n210, n219,
    n228, n237, n246, n255, n259, n260, n261, n267, n268,
    n388, n389, n390, n391, n418, n419, n420, n421, n422, n423, n446, n447,
    n448, n450, n767, n768, n850, n863, n864, n865, n866, n874, n878, n879,
    n880  );
  input  n1, n8, n13, n17, n26, n29, n36, n42, n51, n55, n59, n68, n72,
    n73, n74, n75, n80, n85, n86, n87, n88, n89, n90, n91, n96, n101, n106,
    n111, n116, n121, n126, n130, n135, n138, n143, n146, n149, n152, n153,
    n156, n159, n165, n171, n177, n183, n189, n195, n201, n207, n210, n219,
    n228, n237, n246, n255, n259, n260, n261, n267, n268;
  output n388, n389, n390, n391, n418, n419, n420, n421, n422, n423, n446,
    n447, n448, n450, n767, n768, n850, n863, n864, n865, n866, n874, n878,
    n879, n880;
  wire n86_1, n87_1, n88_1, n89_1, n90_1, n91_1, n92, n93, n94, n95, n96_1,
    n97, n98, n99, n100, n101_1, n102, n103, n104, n105, n106_1, n107,
    n108, n109, n110, n111_1, n112, n113, n114, n115, n116_1, n117, n118,
    n119, n120, n121_1, n122, n123, n124, n125, n126_1, n127, n128, n129,
    n130_1, n131, n132, n133, n134, n135_1, n136, n137, n138_1, n139, n140,
    n141, n142, n143_1, n144, n145, n146_1, n147, n148, n149_1, n150, n151,
    n152_1, n153_1, n154, n155, n156_1, n157, n158, n159_1, n160, n161,
    n162, n163, n164, n165_1, n166, n167, n168, n169, n170, n171_1, n172,
    n173, n174, n175, n176, n177_1, n178, n179, n180, n181, n182, n183_1,
    n184, n185, n186, n187, n188, n189_1, n190, n191, n192, n193, n194,
    n195_1, n196, n197, n198, n199, n200, n201_1, n202, n203, n204, n205,
    n206, n207_1, n208, n209, n210_1, n211, n212, n213, n214, n215, n216,
    n217, n218, n219_1, n220, n221, n222, n223, n224, n225, n226, n227,
    n228_1, n229, n230, n231, n232, n233, n234, n259_1, n260_1, n261_1,
    n262, n263, n264, n265, n266, n267_1, n268_1, n269, n270, n271, n272,
    n273, n274, n275, n276, n277, n278, n279, n280, n281, n282, n283, n284,
    n285, n286, n287, n288, n289, n290, n291;
  assign n86_1 = n1 & n8;
  assign n87_1 = n13 & n86_1;
  assign n88_1 = n1 & n26;
  assign n89_1 = n13 & n17;
  assign n90_1 = n88_1 & n89_1;
  assign n91_1 = n36 & n59;
  assign n92 = ~n101 & ~n106;
  assign n93 = n101 & n106;
  assign n94 = n121 & ~n126;
  assign n95 = ~n121 & n126;
  assign n96_1 = ~n94 & ~n95;
  assign n97 = n263 & ~n96_1;
  assign n98 = ~n97 & ~n264;
  assign n99 = ~n91 & ~n96;
  assign n100 = n91 & n96;
  assign n101_1 = ~n99 & ~n100;
  assign n102 = n130 & n135;
  assign n103 = ~n265 & ~n102;
  assign n104 = ~n111 & ~n116;
  assign n105 = ~n104 & ~n266;
  assign n106_1 = n103 & ~n105;
  assign n107 = ~n103 & n105;
  assign n108 = ~n106_1 & ~n107;
  assign n109 = ~n101_1 & n108;
  assign n110 = ~n267_1 & ~n109;
  assign n111_1 = ~n98 & ~n110;
  assign n112 = ~n171 & ~n177;
  assign n113 = n171 & n177;
  assign n114 = n195 & ~n201;
  assign n115 = ~n195 & n201;
  assign n116_1 = n269 & ~n270;
  assign n117 = ~n269 & n270;
  assign n118 = ~n130 & ~n207;
  assign n119 = n130 & n207;
  assign n120 = n159 & n165;
  assign n121_1 = ~n273 & ~n120;
  assign n122 = n183 & n189;
  assign n123 = ~n274 & ~n122;
  assign n124 = ~n121_1 & n123;
  assign n125 = ~n275 & ~n124;
  assign n126_1 = n272 & ~n125;
  assign n127 = ~n272 & n125;
  assign n128 = ~n126_1 & ~n127;
  assign n129 = ~n271 & ~n128;
  assign n130_1 = n219 & n261;
  assign n131 = ~n201 & ~n130_1;
  assign n132 = n201 & n261;
  assign n133 = ~n228 & ~n277;
  assign n134 = ~n131 & ~n133;
  assign n135_1 = n121 & n210;
  assign n136 = n255 & n267;
  assign n137 = ~n135_1 & ~n136;
  assign n138_1 = ~n1 & n143;
  assign n139 = ~n183 & ~n138_1;
  assign n140 = n183 & n138_1;
  assign n141 = ~n139 & ~n140;
  assign n142 = ~n1 & n146;
  assign n143_1 = ~n1 & n149;
  assign n144 = n17 & n51;
  assign n145 = n121 & n144;
  assign n146_1 = n86_1 & n145;
  assign n147 = ~n143_1 & ~n146_1;
  assign n148 = n132 & ~n279;
  assign n149_1 = n189 & n142;
  assign n150 = n195 & ~n147;
  assign n151 = ~n149_1 & ~n150;
  assign n152_1 = ~n148 & n151;
  assign n153_1 = n141 & n280;
  assign n154 = ~n141 & ~n280;
  assign n155 = n219 & ~n154;
  assign n156_1 = ~n153_1 & n155;
  assign n157 = n228 & n141;
  assign n158 = n106 & n210;
  assign n159_1 = ~n281 & ~n158;
  assign n160 = ~n157 & n159_1;
  assign n161 = ~n278 & ~n149_1;
  assign n162 = n161 & ~n282;
  assign n163 = ~n161 & n282;
  assign n164 = ~n162 & n283;
  assign n165_1 = n228 & n161;
  assign n166 = n246 & n142;
  assign n167 = n111 & n210;
  assign n168 = n255 & n259;
  assign n169 = ~n167 & ~n168;
  assign n170 = ~n166 & n169;
  assign n171_1 = ~n165_1 & n170;
  assign n172 = ~n279 & ~n150;
  assign n173 = ~n132 & ~n172;
  assign n174 = n132 & n172;
  assign n175 = n219 & ~n174;
  assign n176 = ~n173 & n175;
  assign n177_1 = n228 & n172;
  assign n178 = n246 & ~n147;
  assign n179 = n116 & n210;
  assign n180 = ~n179 & ~n284;
  assign n181 = ~n178 & n180;
  assign n182 = ~n177_1 & n181;
  assign n183_1 = n8 & n138;
  assign n184 = n159 & n183_1;
  assign n185 = n51 & n138;
  assign n186 = n165 & n185;
  assign n187 = ~n139 & ~n285;
  assign n188 = n17 & n138;
  assign n189_1 = ~n171 & ~n188;
  assign n190 = n138 & n152;
  assign n191 = ~n177 & ~n190;
  assign n192 = ~n189_1 & ~n191;
  assign n193 = n187 & n192;
  assign n194 = n171 & n188;
  assign n195_1 = ~n189_1 & n286;
  assign n196 = ~n194 & ~n195_1;
  assign n197 = ~n193 & n196;
  assign n198 = ~n186 & n197;
  assign n199 = ~n165 & ~n185;
  assign n200 = ~n159 & ~n183_1;
  assign n201_1 = ~n199 & ~n200;
  assign n202 = ~n198 & n201_1;
  assign n203 = ~n191 & ~n286;
  assign n204 = n187 & n203;
  assign n205 = ~n187 & ~n203;
  assign n206 = n246 & n190;
  assign n207_1 = n101 & n210;
  assign n208 = ~n206 & ~n207_1;
  assign n209 = ~n289 & n208;
  assign n210_1 = n210 & n268;
  assign n211 = n246 & n183_1;
  assign n212 = ~n210_1 & ~n211;
  assign n213 = ~n186 & ~n199;
  assign n214 = n197 & ~n213;
  assign n215 = ~n197 & n213;
  assign n216 = n219 & ~n215;
  assign n217 = ~n214 & n216;
  assign n218 = n228 & n213;
  assign n219_1 = n246 & n185;
  assign n220 = n91 & n210;
  assign n221 = ~n219_1 & ~n220;
  assign n222 = ~n218 & n221;
  assign n223 = ~n189_1 & ~n194;
  assign n224 = ~n187 & ~n286;
  assign n225 = ~n191 & ~n224;
  assign n226 = n223 & n225;
  assign n227 = ~n223 & ~n225;
  assign n228_1 = n219 & ~n227;
  assign n229 = ~n226 & n228_1;
  assign n230 = n228 & n223;
  assign n231 = n246 & n188;
  assign n232 = n96 & n210;
  assign n233 = ~n231 & ~n232;
  assign n234 = ~n230 & n233;
  assign n388 = n75 & n259_1;
  assign n389 = n80 & n260_1;
  assign n390 = n42 & n260_1;
  assign n391 = n85 & n86;
  assign n418 = n17 & n87_1;
  assign n419 = n390 | ~n90_1;
  assign n420 = ~n80 | ~n261_1;
  assign n421 = ~n80 | ~n91_1;
  assign n422 = ~n42 | ~n91_1;
  assign n423 = n90 & ~n262;
  assign n446 = ~n390 | ~n90_1;
  assign n447 = n51 & n88_1;
  assign n450 = n89 & ~n262;
  assign n767 = ~n268_1 & ~n111_1;
  assign n768 = ~n276 & ~n129;
  assign n850 = n134 | ~n137;
  assign n863 = n156_1 | ~n160;
  assign n864 = n164 | ~n171_1;
  assign n865 = n176 | ~n182;
  assign n866 = n184 | n202;
  assign n874 = n288 | ~n209;
  assign n878 = n291 | ~n212;
  assign n879 = n217 | ~n222;
  assign n880 = n229 | ~n234;
  assign n259_1 = n29 & n42;
  assign n260_1 = n29 & n36;
  assign n261_1 = n59 & n75;
  assign n262 = ~n87 & ~n88;
  assign n263 = ~n92 & ~n93;
  assign n264 = ~n263 & n96_1;
  assign n265 = ~n130 & ~n135;
  assign n266 = n111 & n116;
  assign n267_1 = n101_1 & ~n108;
  assign n268_1 = n98 & n110;
  assign n269 = ~n112 & ~n113;
  assign n270 = ~n114 & ~n115;
  assign n271 = ~n116_1 & ~n117;
  assign n272 = ~n118 & ~n119;
  assign n273 = ~n159 & ~n165;
  assign n274 = ~n183 & ~n189;
  assign n275 = n121_1 & ~n123;
  assign n276 = n271 & n128;
  assign n277 = n219 & ~n132;
  assign n278 = ~n189 & ~n142;
  assign n279 = ~n195 & n147;
  assign n280 = ~n278 & ~n152_1;
  assign n281 = n246 & n138_1;
  assign n282 = ~n148 & ~n150;
  assign n283 = n219 & ~n163;
  assign n284 = n255 & n260;
  assign n285 = ~n140 & ~n280;
  assign n286 = n177 & n190;
  assign n287 = n219 & ~n205;
  assign n288 = ~n204 & n287;
  assign n289 = n228 & n203;
  assign n290 = n228 & ~n184;
  assign n291 = ~n200 & n290;
  assign n448 = 1'b0;
endmodule


