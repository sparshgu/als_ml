library verilog;
use verilog.vl_types.all;
entity ckt_sim is
    port(
        n0              : in     vl_logic;
        n1              : in     vl_logic;
        n2              : in     vl_logic;
        n3              : in     vl_logic;
        n4              : in     vl_logic;
        n5              : in     vl_logic;
        n6              : in     vl_logic;
        n5000           : out    vl_logic
    );
end ckt_sim;
