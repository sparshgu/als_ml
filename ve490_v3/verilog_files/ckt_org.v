// Benchmark "ckt_org" written by ABC on Mon Aug 31 19:50:41 2015

module ckt_org ( 
    i0, i1, i2, i3, i4, i5, i6,
    f  );
  input  i0, i1, i2, i3, i4, i5, i6;
  output f;
  wire n9, n10, n11, n12, n13, n14, n15, n16, n17, n18;
  assign f = (i1 & ((i0 & i4 & n10 & ~n12) | (~i0 & ~i6 & n12 & n18))) | (i0 & ((~i1 & ~i2 & ((~i4 & ~i5) | (i6 & n12))) | (i2 & n11 & ~n12 & n18))) | (i4 & ((~i0 & i2 & ~i5 & ~n12) | (i5 & n16))) | (~i0 & ((i6 & n12 & n13) | (~i2 & ~i4 & ~n12 & ~n14 & ~n16))) | (i2 & ((n11 & n15) | (i5 & ~n16 & n17))) | (~n10 & n12) | (~n11 & ~n12 & n14);
  assign n9 = (i1 & i4) | (~i0 & i2 & n17);
  assign n10 = (~n9 & (~i0 | n17)) | (i2 & n15) | (~i2 & ~n13 & ~n15) | (i1 & n18);
  assign n11 = ~i2 | ~i6;
  assign n12 = i5 ^ i6;
  assign n13 = ~i3 & ~i4;
  assign n14 = ~i1 & i3;
  assign n15 = i4 & n14;
  assign n16 = i0 ? (i3 & ~n11) : (~i3 & (~n11 | (i1 & ~i2 & ~i6)));
  assign n17 = ~i1 & n13;
  assign n18 = i3 & ~i4;
endmodule


