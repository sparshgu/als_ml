// Benchmark "ckt_sim" written by ABC on Mon Aug 31 19:50:41 2015

module ckt_sim ( 
    i0, i1, i2, i3, i4, i5, i6,
    f  );
  input  i0, i1, i2, i3, i4, i5, i6;
  output f;
  wire n9, n10, n11, n12, n13, n14, n15, n16, n17, n18;
  assign f = (i1 & ((i0 & i4 & n9 & ~n11) | (~i0 & ~i6 & n11 & n17))) | (i0 & ((~i1 & ~i2 & ((~i4 & ~i5) | (i6 & n11))) | (i2 & n10 & ~n11 & n17))) | (i4 & ((~i0 & i2 & ~i5 & ~n11) | (i5 & n15))) | (~i0 & ((i6 & n11 & n12) | (~i2 & ~i4 & ~n11 & ~n13 & ~n15))) | (i2 & ((n10 & n14) | (i5 & ~n15 & n16))) | (~n9 & n11) | (~n10 & ~n11 & n13);
  assign n9 = (~n18 & (~i0 | n16)) | (i2 & n14) | (~i2 & ~n12 & ~n14) | (i1 & n17);
  assign n10 = ~i2 | ~i6;
  assign n11 = i5 ^ i6;
  assign n12 = ~i3 & ~i4;
  assign n13 = ~i1 & i3;
  assign n14 = n13 & i4;
  assign n15 = n10 ? (i2 & ~i6) : (~i2 & (~i6 | (i0 & ~i1 & ~i3)));
  assign n16 = ~n12 & i1;
  assign n17 = i3 & ~i4;
  assign n18 = (i1 & i4) | (i0 & i2);
endmodule


