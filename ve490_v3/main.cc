#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <climits>
#include <vector>
#include <map>
#include <cassert>
#include <ctime>
#include <sys/timeb.h>
#include "cudd/bnet.h"
#include "head/loc_sim_main.h"
#include "head/basics.h"
#include "../cudd-2.5.0/cudd/cudd.h"
#include "../cudd-2.5.0/cudd/cuddInt.h"

int numPI_ini, numPO_ini;

using namespace std;
int main(int argc, char **argv)
{
    struct timeb startTime, endTime;                         //Record the computing time.
    
    if(argc < 3)
    {
        cout << "Correct usage: ./main input_file threshold > output_file " << endl;
        exit(1);
    }

	ftime(&startTime);

    //*************************************//
    //*****Initialize the circuit**********//
    //*************************************//
    cout << endl << "**************************************************************" << endl;
    cout << "Read the Boolean network: " << endl;
    char com[100];
    sprintf(com, "rm -rf ./blif_files/ckt_sim.blif");
    system(com);
    ftime(&startTime);    
    string filename = argv[1]; 
    FILE *fp;
    fp = fopen(filename.c_str(), "r");
    BnetNetwork *net = Bnet_ReadNetwork(fp);
    numPI_ini = net->npis;
    numPO_ini = net->npos;
//    Bnet_PrintNetwork(net);
    fclose(fp);
    ftime(&endTime);
    double runtime_init = ((endTime.time - startTime.time)*1000 + (endTime.millitm - startTime.millitm))/1000.0;
    cout << "runtime for initialize: " << runtime_init << endl;
    
    if (net == NULL) {
        cout << "Syntax error in " << filename << endl;
		exit(2);
    }
   
    
    double threshold = atof(argv[2]);
    loc_sim_main(net, filename, threshold);
    
/*    cout << endl << "##############################################" << endl;
	cout << "Finall, call SIS to simplify ckt_sim." << endl;        
    sprintf(com, "sis -t none -f ./script/sim_ckt.rug > sis_ckt.txt");
//	sprintf(com, "abc -t none -f ./script/sim_ckt_abc.rug > sis_ckt.txt");
    system(com); 
*/    
    cout << endl << "##############################################" << endl;
	cout <<  "Final result: " << endl;
	sprintf(com, "sis -t none -f ./script/map.rug");
	system(com);
	
	ftime(&endTime);
	
	double rt_locsim = ((endTime.time - startTime.time)*1000 + (endTime.millitm - startTime.millitm))/1000.0;
	cout << "runtime for locsim: " << rt_locsim << endl;
    
    
    
/*    cout << endl << "**************************************************************" << endl;
    cout << "Calculate all signal probabilities in the graph: " << endl;
    ftime(&startTime);  
    WAA(sort_list);
    ftime(&endTime);
    double runtime_WAA = ((endTime.time - startTime.time)*1000 + (endTime.millitm - startTime.millitm))/1000.0;
	cout << "runtime for WAA: " << runtime_WAA << endl;
*/    
//	Bnet_FreeNetwork(net);
    return 0;
}
