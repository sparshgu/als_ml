library verilog;
use verilog.vl_types.all;
entity partA_sim_free is
    port(
        n0              : in     vl_logic;
        n1              : in     vl_logic;
        n2              : in     vl_logic;
        n3              : in     vl_logic;
        n4              : in     vl_logic;
        n5              : in     vl_logic;
        n6              : in     vl_logic;
        n271            : out    vl_logic;
        n22             : out    vl_logic;
        n32             : out    vl_logic;
        n181            : out    vl_logic;
        n19             : out    vl_logic;
        n241            : out    vl_logic
    );
end partA_sim_free;
