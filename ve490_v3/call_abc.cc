#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

////////////////////////////////////////////////////////////////////////
///                        DECLARATIONS                              ///
////////////////////////////////////////////////////////////////////////

// procedures to start and stop the ABC framework
// (should be called before and after the ABC procedures are called)
extern "C"
{
extern void   Abc_Start();
extern void   Abc_Stop();

// procedures to get the ABC framework and execute commands in it
extern void * Abc_FrameGetGlobalFrame();
extern int    Cmd_CommandExecute( void * pAbc, char * sCommand );
}

int call_abc(int flag)
{

    // variables
    void * pAbc;
    FILE *fp;
    char Command1[500], Command2[500], Command3[500], Command4[500], Command5[500], Command6[500], Command7[500];
    
     //////////////////////////////////////////////////////////////////////////
     // start the ABC framework
     Abc_Start();
     pAbc = Abc_FrameGetGlobalFrame();
     
    if(flag == 1)  //strash bigNode_sim.blif
    {
	    sprintf( Command1, "read_blif ./blif_files/bigNode_sim.blif");
	    if ( Cmd_CommandExecute( pAbc, Command1 ) )
	    {
	        fprintf( stdout, "Cannot execute command \"%s\".\n", Command1 );
	        return 1;
	    }

	    

        sprintf( Command1, "balance; rewrite; refactor; balance; rewrite; rewrite -z; balance; refactor -z; rewrite -z; balance");
	    if ( Cmd_CommandExecute( pAbc, Command1 ) )
	    {
	        fprintf( stdout, "Cannot execute command \"%s\".\n", Command1 );
	        return 1;
	    }
	    
	    
	    sprintf( Command1, "strash");
	    if ( Cmd_CommandExecute( pAbc, Command1 ) )
	    {
	        fprintf( stdout, "Cannot execute command \"%s\".\n", Command1 );
	        return 1;
	    }


	    sprintf( Command1, "write_blif ./blif_files/bigNode_sim.blif");
	    if ( Cmd_CommandExecute( pAbc, Command1 ) )
	    {
	        fprintf( stdout, "Cannot execute command \"%s\".\n", Command1 );
	        return 1;
	    }
    }
    else if(flag == 2)   //blif to verilog
    {
        //////////////////////////////////////////////////////////////////////////
        // read_blif
        sprintf( Command1, "read_blif ./blif_files/ckt_org.blif");
	    if ( Cmd_CommandExecute( pAbc, Command1 ) )
	    {
	        fprintf( stdout, "Cannot execute command \"%s\".\n", Command1 );
	        return 4;
	    }
	
	    //////////////////////////////////////////////////////////////////////////
        // write_verilog
        sprintf( Command2, "write_verilog ./verilog_files/ckt_org.v");
	    if ( Cmd_CommandExecute( pAbc, Command2 ) )
	    {
	        fprintf( stdout, "Cannot execute command \"%s\".\n", Command2 );
	        return 5;
	    }
	    
	     //////////////////////////////////////////////////////////////////////////
        // read_blif
        sprintf( Command3, "read_blif ./blif_files/ckt_sim.blif");
	    if ( Cmd_CommandExecute( pAbc, Command3 ) )
	    {
	        fprintf( stdout, "Cannot execute command \"%s\".\n", Command3 );
	        return 6;
	    }
	
	    //////////////////////////////////////////////////////////////////////////
        // write_verilog
        sprintf( Command4, "write_verilog ./verilog_files/ckt_sim.v");
	    if ( Cmd_CommandExecute( pAbc, Command4 ) )
	    {
	        fprintf( stdout, "Cannot execute command \"%s\".\n", Command4 );
	        return 7;
	    }
    }
    else if(flag == 3)  //resyn2 and strash ckt_sim.blif
    {
	    //////////////////////////////////////////////////////////////////////////
        // read_blif
        sprintf( Command1, "read_blif ./blif_files/ckt_sim.blif");
	    if ( Cmd_CommandExecute( pAbc, Command1 ) )
	    {
	        fprintf( stdout, "Cannot execute command \"%s\".\n", Command1 );
	        return 1;
	    }
	
	    //////////////////////////////////////////////////////////////////////////
        // strash
        sprintf( Command2, "balance; rewrite; refactor; balance; rewrite; rewrite -z; balance; refactor -z; rewrite -z; balance;");
//	sprintf( Command2, "strash");
	    if ( Cmd_CommandExecute( pAbc, Command2 ) )
	    {
	        fprintf( stdout, "Cannot execute command \"%s\".\n", Command2 );
	        return 2;
	    }
	
	    //////////////////////////////////////////////////////////////////////////
        // write_blif
        sprintf( Command3, "write_blif ./blif_files/ckt_sim.blif");
	    if ( Cmd_CommandExecute( pAbc, Command3 ) )
	    {
	        fprintf( stdout, "Cannot execute command \"%s\".\n", Command3 );
	        return 3;
	    }
    }
    else if(flag == 4)  //resyn2 and strash ckt_sim.blif
    {
	    //////////////////////////////////////////////////////////////////////////
        // read_library
        sprintf( Command1, "read_library mcnc.genlib");
	    if ( Cmd_CommandExecute( pAbc, Command1 ) )
	    {
	        fprintf( stdout, "Cannot execute command \"%s\".\n", Command1 );
	        return 1;
	    }
	
	    //////////////////////////////////////////////////////////////////////////
        // read_blif
        sprintf( Command2, "read_blif ./blif_files/ckt_org.blif");
	    if ( Cmd_CommandExecute( pAbc, Command2 ) )
	    {
	        fprintf( stdout, "Cannot execute command \"%s\".\n", Command2 );
	        return 2;
	    }
	
	    //////////////////////////////////////////////////////////////////////////
        // map & print_stats
        sprintf( Command3, "map; print_stats");
	    if ( Cmd_CommandExecute( pAbc, Command3 ) )
	    {
	        fprintf( stdout, "Cannot execute command \"%s\".\n", Command3 );
	        return 3;
	    }
	    
	    //////////////////////////////////////////////////////////////////////////
        // read_blif
        sprintf( Command2, "read_blif ./blif_files/ckt_sim.blif");
	    if ( Cmd_CommandExecute( pAbc, Command2 ) )
	    {
	        fprintf( stdout, "Cannot execute command \"%s\".\n", Command2 );
	        return 2;
	    }
	
	    //////////////////////////////////////////////////////////////////////////
        // map & print_stats
        sprintf( Command3, "map; print_stats");
	    if ( Cmd_CommandExecute( pAbc, Command3 ) )
	    {
	        fprintf( stdout, "Cannot execute command \"%s\".\n", Command3 );
	        return 3;
	    }
    }
    else if(flag == 5)   //blif to pla
    {
        //////////////////////////////////////////////////////////////////////////
        // read_blif
        sprintf( Command1, "read_blif ./blif_files/bigNode_sim.blif");
	    if ( Cmd_CommandExecute( pAbc, Command1 ) )
	    {
	        fprintf( stdout, "Cannot execute command \"%s\".\n", Command1 );
	        return 5;
	    }
	    
	    //////////////////////////////////////////////////////////////////////////
        // write_verilog
        sprintf( Command2, "collapse");
	    if ( Cmd_CommandExecute( pAbc, Command2 ) )
	    {
	        fprintf( stdout, "Cannot execute command \"%s\".\n", Command2 );
	        return 5;
	    }
	
	    //////////////////////////////////////////////////////////////////////////
        // write_verilog
        sprintf( Command2, "write_pla ./pla_files/bigNode_sim.pla");
	    if ( Cmd_CommandExecute( pAbc, Command2 ) )
	    {
	        fprintf( stdout, "Cannot execute command \"%s\".\n", Command2 );
	        return 5;
	    }
    }
    //////////////////////////////////////////////////////////////////////////
    // stop the ABC framework
    Abc_Stop();
    return 0;   
}

