library verilog;
use verilog.vl_types.all;
entity ckt_sim_simu is
    port(
        n0              : in     vl_logic;
        n1              : in     vl_logic;
        n2              : in     vl_logic;
        n3              : in     vl_logic;
        n4              : in     vl_logic;
        n7              : out    vl_logic;
        n8              : out    vl_logic;
        n9              : out    vl_logic;
        n10             : out    vl_logic;
        n11             : out    vl_logic;
        n12             : out    vl_logic;
        n13             : out    vl_logic;
        n14             : out    vl_logic;
        n20             : out    vl_logic;
        n1008           : out    vl_logic;
        n1010           : out    vl_logic;
        n1091           : out    vl_logic;
        n5000           : out    vl_logic
    );
end ckt_sim_simu;
