#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <climits>
#include <vector>
#include <cmath>
#include <cassert>
#include "head/graph.h"
#include "head/node.h"
#include "head/edge.h"
#include "head/HashTable.h"
#include "head/call_abc.h"
#include "head/basics.h"
#include "head/helper.h"
#include "head/write_func.h"
#include "head/read_file.h"
#include "head/sim.h"
#include "../cudd-2.5.0/cudd/cudd.h"
#include "../cudd-2.5.0/cudd/cuddInt.h"
#include "cudd/cudd_build_v2.h"
#include "cudd/cudd_comp.h"
#include "cudd/cudd_dst.h"
#include "cudd/bnet.h"
#include "cudd/ntr.h"

using namespace std;

//Global variables and external variables
extern int numPI_ini, numPO_ini;

/*
functions in this file
1. int extNum(string &s, int flag, int &flag_hyphen)
2. bool remove_vector(vector<Edge*> &NumVec, vector<int> & FlagVec)
3. void find_tranfanin_hash(map<int, Node*> &nodes, HashTable &tfi, int node)
4. void find_tranfanout_hash(map<int, Node*> &nodes, HashTable &tranFanOut, int lineNumber)
*/

/*4. find_tranfanout_hash()*/
void find_tranfanout(BnetNetwork *net, char *cnode, set<char*> &tfo)
{
//	cout << "find tfo for node " << cnode << endl;

	BnetNode *nd, *tmp;
	st_lookup(net->hash, cnode, &nd);
	tfo.insert(nd->name);
	
	//If corrent line is a PO, then return;
	if (nd->type == BNET_OUTPUT_NODE) 
		return;
	
//	cout << "nfo = " << nd->nfo << endl;
	for(int i = 0; i < nd->nfo; i++)
	{	
		char *outnode = nd->fanouts[i];
//		cout << "outnode: " << outnode << endl;
		st_lookup(net->hash, outnode, &tmp);
		if(!tmp->visited)
		{
			tfo.insert(tmp->name);
			tmp->visited = 1;
			find_tranfanout(net, outnode, tfo);
		}
	}
}




void int2bin(int d, vector<int> &bin, int numBit)
{
	int mod = 0;
	vector<int> tmpstr;
	
	if(d == 0)
	{
		for(int i = 0; i < numBit; i++)
			bin.push_back(0);
		return;	
	}

	while(d > 0)
	{
		mod = d%2;
		d /= 2;
		tmpstr.push_back(mod);
	}
	unsigned int len = tmpstr.size();
	int minus = numBit - len;
	if(minus > 0)
		for(int i = 0; i < minus; i++)
			bin.push_back(0);	
	for(int i = len - 1; i >= 0; i--)
		bin.push_back(tmpstr[i]);
}




string int2grey(int indexHor, int nVarsHor)
{
//	cout << "indexHor = " << indexHor << ", nVarsHor = " << nVarsHor << endl;
	vector<int> bin;
	int2bin(indexHor, bin, nVarsHor);
	vector<int> grey;
	for(int i = 0; i < bin.size(); i++)
	{
		int bit;
		if(i > 0) 
			bit = bin[i] ^ bin[i-1];
		else
			bit = bin[i];
		grey.push_back(bit);
	}

	string grey_str;
	char s[1];
	for(int i = 0; i < grey.size(); i++)
	{
		sprintf(s, "%d", grey[i]);
		string str(s);
		grey_str.append(str);
	}
//	cout << "grey_str = " << grey_str << endl;
	return grey_str;
}



int call_vcs()
{
    //delete the old comp.txt first
    char com[100];
    sprintf(com, "rm -rf comp.txt");
    system(com);

    char command[100];
    sprintf(command, "vcs +v2k  ./verilog_files/ckt_org.v ./verilog_files/ckt_sim.v ./verilog_files/ckt_tb.v");
    system(command);
    sprintf(command, "./simv");
    system(command);

    ifstream fin;
    fin.open("comp.txt", ifstream::in);
    string str;
    int num_diff = 0;
    cout << "comp.txt: " << endl;
    while(getline(fin, str))
    {
		cout << str << endl;
		num_diff++;
    }
    fin.close();
    return num_diff;
}



void exp_cube(vector<string> &new_pla, string &s)
{
	set<string>::iterator itrs;

	cout << "in exp_cube: s = " << s << endl;
	int num_dc_bit = 0;
    for(int i = 0; i < s.size(); i++)
    {
    	if(s[i] == '-')
        	num_dc_bit++;
    }
    int num_dc = pow(2.0, num_dc_bit);    
	for(int i = 0; i < num_dc; i++)
    {
    	int index_dc_bit = 0;
    	string exp_this_cube;
        for(int j = 0; j < s.size(); j++)
        {
        	if(s[j] != '-')
            	exp_this_cube.append(1, s[j]);
            else
            {
            	vector<int> bin;
            	int2bin(i, bin, num_dc_bit);
            	char cc = bin[index_dc_bit++] + 48;
            	exp_this_cube.append(1, cc);
            }
        }
        new_pla.push_back(exp_this_cube);
    }  
    
}


void build_ddnode_cube(BnetNetwork *net, DdManager **dd, DdNode *prod, vector<char*> &cutnodes, string &sexdc)
{
	BnetNode *auxnd;
	
		for(int i = 0; i < sexdc.size(); i++)
		{
			if(sexdc[i] == '-')
				continue;
			char *cnode = cutnodes[i];
			if(!st_lookup((net)->hash, cnode, &auxnd))
			{
				cout << "current node doesn't exixt in st_table!" << endl;
				exit(1);
			}
			DdNode *var;
			if (sexdc[i] == '1')
				var = auxnd->dd;
		    else 
				var = Cudd_Not(auxnd->dd);
			DdNode *tmp = Cudd_bddAnd((*dd), prod, var);	
			Cudd_Ref(tmp);
			Cudd_IterDerefBdd(*dd, prod);
			prod = tmp;
		}
}


//exdc_minterm
void build_ddnode_sop(BnetNetwork *net, DdManager **dd, DdNode **func, vector<char*> &cutnodes, vector<string> &exdc_set)
{
	BnetNode *auxnd;	
	DdNode *prod, *tmp;
	set<string>::iterator itrs;

	*func = Cudd_ReadLogicZero(*dd);
	Cudd_Ref(*func);
	
	for(int i = 0; i < exdc_set.size(); i++)
	{
//		cout << "build ddnode for cube " << exdc_set[i] << endl;
		prod = Cudd_ReadOne(*dd);
		Cudd_Ref(prod);
		string sexdc = exdc_set[i];
		for(int j = 0; j < sexdc.size(); j++)
		{
			if(sexdc[j] == '-')
				continue;
			char *cnode = cutnodes[j];
//			cout << "current node: " << cnode << endl;
			if(!st_lookup((net)->hash, cnode, &auxnd))
			{
				cout << "current node doesn't exixt in st_table!" << endl;
				exit(1);
			}
			DdNode *var;
			if (sexdc[i] == '1')
				var = auxnd->dd;
		    else 
				var = Cudd_Not(auxnd->dd);
			if(var == NULL)
			{
				cout << "var = NULL!" << endl;
				exit(1);
			}
			DdNode *tmp = Cudd_bddAnd((*dd), prod, var);	
			Cudd_Ref(tmp);
			Cudd_IterDerefBdd(*dd, prod);
			prod = tmp;
		}		
		tmp = Cudd_bddOr(*dd, *func, prod);
	    if (tmp == NULL)
	    {
			cout << "0. tmp is NULL!" << endl;
			exit(1);
		}	
	    Cudd_Ref(tmp);
	    Cudd_IterDerefBdd(*dd, *func);
	    Cudd_IterDerefBdd(*dd, prod);
	    *func = tmp;
	}
	return;
}



//exdc_minterm
double exdc_minterm(BnetNetwork *net, DdManager **dd, vector<char*> &cutnodes, set<string> &diff_pla)
{
	double this_real_er;
	BnetNode *auxnd;	
	DdNode *prod, *func, *tmp;
	set<string>::iterator itrs;

	func = Cudd_ReadLogicZero(*dd);
	Cudd_Ref(func);
	
	for(itrs = diff_pla.begin(); itrs != diff_pla.end(); itrs++)
	{
		string sexdc = *itrs;
		build_ddnode_cube(net, dd, prod, cutnodes, sexdc);		
		tmp = Cudd_bddOr(*dd, func, prod);
	    if (tmp == NULL)
	    {
			cout << "1. tmp is NULL!" << endl;
			exit(1);
		}	
	    Cudd_Ref(tmp);
	    Cudd_IterDerefBdd(*dd,func);
	    Cudd_IterDerefBdd(*dd,prod);
	    func = tmp;
	}
	
	double num_minterm = Cudd_CountMinterm(*dd, func, numPI_ini);
	this_real_er = num_minterm/pow(2.0, numPI_ini);
	return this_real_er;
}




double comp_each_real_er(BnetNetwork *net, DdManager **dd, vector<char*> &cutnodes, vector<string> &input_nodes, vector<string> &ori_pla_v0)
{
	vector<string> new_pla_v0;
	set<string> diff_pla;
	set<string>::iterator itrs;
	map<string, int> new_pla, ori_pla;
	map<string, int>::iterator itrm_si, itrm_si1;  

	ifstream fin;
	fin.open("./pla_files/bigNode_sim.pla", ifstream::in);
	string str;
	int flag_start = 0;
	cout << "read new_pla: " << endl;
	while(getline(fin, str))
	{
		cout << str << endl;
		istringstream ss(str);
	    string s, s_first;
	    ss >> s;
	    s_first = s;
	    if(s == ".p")
	    {
	    	flag_start = 1;
	    	continue;
	    }
	    if(flag_start == 1)
	    {
//	    	cout << "0. s = " << s << endl;
	    	while(ss >> s);
	    	if(s == "2")
	    		break;
//	    	cout << "1. s = " << s << endl;
//	    	cout << "s_first: " << s_first << endl;
	    	exp_cube(new_pla_v0, s_first);    
	    }
    }   	    		
       
    //sort new_pla_v0
    permute(new_pla_v0, input_nodes, cutnodes);
       
    //Find out diff_pla         
    for(int i = 0; i < new_pla_v0.size(); i++)
    	new_pla.insert(pair<string, int>(new_pla_v0[i], -1));
    for(int i = 0; i < ori_pla_v0.size(); i++)
    	ori_pla.insert(pair<string, int>(ori_pla_v0[i], -1));
        
	cout << "new_pla: " << endl;
    for(itrm_si = new_pla.begin(); itrm_si != new_pla.end(); itrm_si++)
    	cout << itrm_si->first << endl;    
    
    cout << endl << "ori_pla: " << endl;

    for(itrm_si = ori_pla.begin(); itrm_si != ori_pla.end(); itrm_si++)
    {
    	cout << itrm_si->first << endl;
        itrm_si1 = new_pla.find(itrm_si->first);
        if(itrm_si1 != new_pla.end())
        {
        	itrm_si->second = 1;
        	itrm_si1->second = 1;	
        }
    }
       
    //Obtain diff_pla 
	for(itrm_si = ori_pla.begin(); itrm_si != ori_pla.end(); itrm_si++)
    {
        if(itrm_si->second == -1)
        	diff_pla.insert(itrm_si->first);
    }
    for(itrm_si = new_pla.begin(); itrm_si != new_pla.end(); itrm_si++)
    {
    	if(itrm_si->second == -1)
        	diff_pla.insert(itrm_si->first);
    }
                	
	//Build bdd to find the minterms corresponding to cubes in diff_pla     	
    cout << "diff_pla: " << endl;
    for(itrs = diff_pla.begin(); itrs != diff_pla.end(); itrs++)
     	cout << *itrs << endl;
     		
    double each_real_er;
    if(diff_pla.empty())
    	each_real_er = 0;
    else
		each_real_er = exdc_minterm(net, dd, cutnodes, diff_pla);

    return each_real_er;
}


void image_computation(BnetNetwork *net, DdManager **dd, DdNode *global_care, char *cnode, vector<string> &local_dc_set)
{
	map<int, int>::iterator itrmi;
	set<string> local_care_set;
	vector<int> index_set;	
	BnetNode *nd, *auxnd;
	st_lookup(net->hash, cnode, &nd);
	
//	cout << "In image_computation: " << endl;
//	cout << "current node: " << nd->name << endl;
/*	cout << "global_care: " << endl;
	Cudd_PrintDebug(*dd, global_care, numPI_ini, 2);
*/	
	int num_var = nd->ninp;
	DdNode **array_funcs = ALLOC(DdNode*, num_var);
		
	for(int i = 0; i < num_var; i++)
	{
		index_set.push_back(i);
		char *innode = nd->inputs[i];
//		cout << "input node: " << innode << endl;
		if(!st_lookup(net->hash, innode, &auxnd))
		{
			cout << "current input is not in hash!" << endl;
			exit(1);
		}		
		if(auxnd->dd == NULL)
		{
			cout << "current input's dd is NULL!" << endl;
			exit(2);
		}			
/*		cout << "input node's dd: " << endl;
		Cudd_PrintDebug(*dd,auxnd->dd, numPI_ini, 2);
		cout << "global_care: " << endl;
		Cudd_PrintDebug(*dd, global_care, numPI_ini, 2);
*/
		DdNode *local_care = Cudd_bddConstrain(*dd, auxnd->dd, global_care);	
		Cudd_Ref(local_care);
//		cout << "constrained node " << i << endl;
//		Cudd_PrintDebug(*dd, local_care, numPI_ini, 2);
		array_funcs[i] = local_care;				
	}
	vector<map<int, int> > lit_cubes;
	image_computation_recur(dd, array_funcs, num_var, index_set, lit_cubes);
//	cout << endl << "finally, lit_cubes: " << endl;
	for(int i = 0; i < lit_cubes.size(); i++)
	{
		string care_str;
		map<int, int> lit_phase = lit_cubes[i];
/*		for(itrmi = lit_phase.begin(); itrmi != lit_phase.end(); itrmi++)
			cout << "(" << itrmi->first << ", " << itrmi->second << ") ";
		cout << endl;
*/
		for(int j = 0; j < nd->ninp; j++)
		{
			itrmi = lit_phase.find(j);
			if(itrmi == lit_phase.end())
				care_str.append(1, '-');
			else if(itrmi->second == 1)
				care_str.append(1, '1');
			else if(itrmi->second == 0)
				care_str.append(1, '0');
		}
//		cout << "care_str: " << care_str << endl;
		local_care_set.insert(care_str);
	}
	
//	cout << "complement_cover: " << endl;
	complement_cover(local_care_set, local_dc_set, nd->ninp);	
	
//	FREE(array_funcs);
}

void image_computation_recur(DdManager **dd, DdNode **array_funcs, int &num_var, vector<int> &index_set, vector<map<int, int> >&lit_cubes)
{

//	cout << endl << "Come into image_computation_recur!" << endl;
	
/*	cout << "index_set: " << endl;
	for(int i = 0; i < index_set.size(); i++)
		cout << index_set[i] << " ";
	cout << endl;
*/
	set<int> const_index;
	set<int>::iterator itrs;
	vector<int> new_index_set;
	map<int, int> const_lit_phase;
	int thisone = -1;
	int count = 0;
		
	//Check constant bdd nodes	
	for(int i = 0; i < num_var; i++)
	{
		DdNode *this_func = array_funcs[i];
		if(this_func == Cudd_ReadLogicZero(*dd) || this_func == Cudd_ReadOne(*dd))
		{
			if(this_func == Cudd_ReadLogicZero(*dd))
			{
				const_lit_phase.insert(pair<int, int>(index_set[i], 0));
//				cout << "node " << index_set[i] << "'s bdd node is constant 0!" << endl;	
			}
			else if(this_func == Cudd_ReadOne(*dd))
			{
				const_lit_phase.insert(pair<int, int>(index_set[i], 1));
//				cout << "node " << index_set[i]<< "'s bdd node is constant 1!" << endl;
			}
			this_func = NULL;
			const_index.insert(index_set[i]);
			continue;
		}
		count++;
		if (thisone == -1 ) 
            thisone = i;         
	}
	lit_cubes.push_back(const_lit_phase);
	
	for(int i = 0; i < index_set.size(); i++)
	{
		itrs = const_index.find(index_set[i]);
		if(itrs == const_index.end())
			new_index_set.push_back(index_set[i]);
	}
	index_set = new_index_set;
	
/*	cout << "new_index_set: " << endl;
	for(int i = 0; i < index_set.size(); i++)
		cout << index_set[i] << " ";
	cout << endl;
*/
	
	//Update array_funcs to new_array_funcs
	int new_num_var = count;
	DdNode **pos_array_funcs = ALLOC(DdNode*, new_num_var);
	DdNode **neg_array_funcs = ALLOC(DdNode*, new_num_var);
//	DdNode **new_array_funcs = ALLOC(DdNode*, count);
	int j = 0;
	for(int i = 0; i < num_var; i++)
	{
/*		itrs = const_index.find(i);
		if(itrs == const_index.end())
		{
			new_array_funcs[j] = array_funcs[i];
			j++;
		}
*/
		if((array_funcs[i] != Cudd_ReadLogicZero(*dd)) && (array_funcs[i] != Cudd_ReadOne(*dd)))
		{
		//	new_array_funcs[j] = array_funcs[i];
			pos_array_funcs[j] = array_funcs[i];
			neg_array_funcs[j] = array_funcs[i];
			j++;
		}
	}
	num_var = new_num_var;	

	//If only one function is non-null, return;
	if (count <= 1) 
	{
		FREE(pos_array_funcs);
		FREE(neg_array_funcs);
		return;
	}
		

	//Output cofactoring: first positive cofactoring then negative cofactoring	
/*	for(int i = 0; i < num_var; i++)
	{
		pos_array_funcs[i] = new_array_funcs[i];
		neg_array_funcs[i] = new_array_funcs[i];
	}
*/
	int pos_num_var = num_var;
	int neg_num_var = num_var;
	vector<int> pos_index_set = index_set;
	vector<int> neg_index_set = index_set;
	vector<map<int, int> > pos_lit_cubes, neg_lit_cubes;
	map<int, int>::iterator itrmi;	
	DdNode *this_func, *new_func;
	//1. positive cofactor
//	cout << "-------------------------------" << endl;
//	cout << "Positive cofactoring over node " << pos_index_set[0] << ": " << endl;
	DdNode *co_func = pos_array_funcs[0];
	Cudd_Ref(co_func);	
	for(int i = 0; i < pos_num_var; i++)
	{
		this_func = pos_array_funcs[i];
//		cout << "node " << pos_index_set[i] << ":" << endl;
//		Cudd_PrintDebug(*dd, this_func, numPI_ini, 3);
		new_func = Cudd_bddConstrain(*dd, this_func, co_func);
		Cudd_Ref(new_func);
//		Cudd_IterDerefBdd(*dd, pos_array_funcs[i]);
		pos_array_funcs[i] = new_func;	
	}
//	pos_array_funcs[0] = Cudd_ReadOne(*dd);
	image_computation_recur(dd, pos_array_funcs, pos_num_var, pos_index_set, pos_lit_cubes);

	//2. negtative cofactor
//	cout << "-------------------------------" << endl;
//	cout << "Negative cofactoring over node " << neg_index_set[0] << ": " << endl;
	DdNode *co_func_neg = Cudd_Not(neg_array_funcs[0]);
	Cudd_Ref(co_func_neg);	
	for(int i = 0; i < neg_num_var; i++)
	{
		this_func = neg_array_funcs[i];
//		cout << "node " << neg_index_set[i] << ":" << endl;
//		Cudd_PrintDebug(*dd, this_func, numPI_ini, 3);
		new_func = Cudd_bddConstrain(*dd, this_func, co_func_neg);
		Cudd_Ref(new_func);
//		Cudd_IterDerefBdd(*dd, neg_array_funcs[i]);
		neg_array_funcs[i] = new_func;	
	}
//	neg_array_funcs[0] = Cudd_ReadLogicZero(*dd);
	image_computation_recur(dd, neg_array_funcs, neg_num_var, neg_index_set, neg_lit_cubes);
	
	FREE(pos_array_funcs);
	FREE(neg_array_funcs);
	

/*	cout << "after this recursion, const_lit_phase: " << endl;
	for(itrmi = const_lit_phase.begin(); itrmi != const_lit_phase.end(); itrmi++)
		cout << "(" << itrmi->first << ", " << itrmi->second << ")" << endl;
	cout << endl;
*/
	lit_cubes.clear();
//	cout << "after this recursion, pos_lit_cubes: " << endl;
	for(int i = 0; i < pos_lit_cubes.size(); i++)
	{
		map<int, int> lit_phase = pos_lit_cubes[i];
/*		for(itrmi = lit_phase.begin(); itrmi != lit_phase.end(); itrmi++)
			cout << "(" << itrmi->first << ", " << itrmi->second << ") ";
		cout << endl;
*/
		map<int, int> new_phase = lit_phase;
		for(itrmi = const_lit_phase.begin(); itrmi != const_lit_phase.end(); itrmi++)
			new_phase.insert(pair<int, int>(itrmi->first, itrmi->second));
		lit_cubes.push_back(new_phase);
	}
//	cout << "after this recursion, neg_lit_cubes: " << endl;
	for(int i = 0; i < neg_lit_cubes.size(); i++)
	{
		map<int, int> lit_phase = neg_lit_cubes[i];
/*		for(itrmi = lit_phase.begin(); itrmi != lit_phase.end(); itrmi++)
			cout << "(" << itrmi->first << ", " << itrmi->second << ") ";
		cout << endl;
*/
		map<int, int> new_phase = lit_phase;
		for(itrmi = const_lit_phase.begin(); itrmi != const_lit_phase.end(); itrmi++)
			new_phase.insert(pair<int, int>(itrmi->first, itrmi->second));
		lit_cubes.push_back(new_phase);
	}
//	cout << "after this recursion, lit_cubes: " << endl;
	for(int i = 0; i < lit_cubes.size(); i++)
	{
		map<int, int> lit_phase = lit_cubes[i];
/*		for(itrmi = lit_phase.begin(); itrmi != lit_phase.end(); itrmi++)
			cout << "(" << itrmi->first << ", " << itrmi->second << ") ";
		cout << endl;
*/
	}
	
	

	
}	


void complement_cover(set<string> &local_care_set, vector<string> &local_dc_set, int num_input)
{
	set<string>::iterator itrs;

	ofstream fout;
	fout.open("./blif_files/care_set.blif", ios::out);
	fout << ".model care_set" << endl;
	fout << ".inputs ";
	for(int i = 0; i < num_input; i++)
		fout << "i" << i << " ";
	fout << endl;
	fout << ".outputs nn" << endl;
	fout << ".names ";
	for(int i = 0; i < num_input; i++)
		fout << "i" << i << " ";
	fout << " nn" << endl;
	for(itrs = local_care_set.begin(); itrs != local_care_set.end(); itrs++)
		fout << *itrs << " 1" << endl;
	fout << ".end" << endl;
	fout.close();
	
	FILE *fp;
	fp = fopen("./blif_files/care_set.blif", "r");
	BnetNetwork *net = Bnet_ReadNetwork(fp);
	fclose(fp);
//	Bnet_PrintNetwork(net);
	
	DdManager *dd = NULL;
    dd = Cudd_Init(0, 0, CUDD_UNIQUE_SLOTS, CUDD_CACHE_SLOTS, 0);
    if (dd == NULL) exit(1);   
    cudd_build_v2(net, &dd, "./blif_files/care_set.blif", BNET_GLOBAL_DD);
    
    BnetNode *nd;
    char *output = net->outputs[0];
//    cout << "output node: " << output << endl;
    st_lookup(net->hash, output, &nd);
//    Cudd_PrintDebug(dd, nd->dd, num_input, 2);

    fp = fopen("dc_set.log", "w");
    dd->out = fp;
    DdNode *dc = Cudd_Not(nd->dd);
    Cudd_Ref(dc);
    Cudd_PrintMinterm(dd, dc);
    fclose(fp);
    
    Bnet_FreeNetwork(net);
    Cudd_Quit(dd);
    
    ifstream fin;
    fin.open("dc_set.log", ios::in);
    string str, s;
//    cout << "dc_set: " << endl;
    while(getline(fin, str))
    {
//    	cout << str << endl;
    	istringstream ss(str);
    	ss >> s;
    	local_dc_set.push_back(s);
    }

}




/*double sim_pick(BnetNetwork *net, char *cnode, vector<char*> &cutnodes, multimap<double, string> &exdc_set, multimap<double, string> &exdc_record, int iIndex)
{
	multimap<double, string>::iterator n_iter;
	BnetNode *nd;
    
    double this_score = 0, max_score = 0, include_er = 0, max_include_er = 0;
    int this_area_save;
    multimap<double, string> tmp_exdc_set;
    multimap<double, string> max_exdc_set;
    ifstream fin;
    
    st_lookup(net->hash, cnode, &nd);
    int nfo = nd->nfo;
    
    int count = 0;
    for(n_iter = exdc_set.begin(); n_iter != exdc_set.end(); n_iter++)
    {
    	max_include_er += n_iter->first;
    	if(n_iter->first > 0)
    		count++;
    }
//    cout << "max_include_er = " << max_include_er << endl;
//    cout << "count = " << count << endl;
    int j = 0, flag_index = exdc_set.size()-1;
    int flag_index_limit = exdc_set.size()- count;
//    cout << "limit for flag_index = " << flag_index_limit << endl;
    while(1)
    {
    	cout << endl << "j = " << j << endl;
    	include_er = 0;
    	for(n_iter = exdc_set.begin(); n_iter != exdc_set.end(); n_iter++)
    	{
    		include_er += n_iter->first;
    		if(n_iter->first == 0)
    			continue;
    		cout << n_iter->second << ": " << n_iter->first << endl;
    	}    	
    	cout << "include_er = " << include_er << endl;

		//write bigNode.pla
		write_bignode_pla(net, cnode, exdc_set);	
		char com[100];   	                        
		sprintf(com, "espresso ./pla_files/bigNode.pla > ./pla_files/bigNode_sim.pla");
		system(com); 

		sprintf(com, "sis -t none -f ./script/print_fac.rug > sis.txt");
	    system(com);
	//	this_area_save = read_sis_result();   
		read_sis_result(this_area_save);                 
	    cout << "this_area_save = " << this_area_save << endl;
	    
	    if(this_area_save <= 0 && include_er == max_include_er)
	    {
			if(!exdc_record.empty())
	    	{
	    		cout << "try exdc_record: " << endl;
	    		exdc_set = exdc_record;
	    		flag_index = exdc_set.size()-1;
	    		write_bignode_pla(net, cnode, exdc_set);	  	                        
				sprintf(com, "espresso ./pla_files/bigNode.pla > ./pla_files/bigNode_sim.pla");
				system(com); 
				sprintf(com, "sis -t none -f ./script/print_fac.rug > sis.txt");
			    system(com);
		//		this_area_save = read_sis_result(); 
				read_sis_result(this_area_save);               
			    cout << "this_area_save = " << this_area_save << endl;
			    this_score = this_area_save/include_er;
	    	}
	    	else
	    		break;
	    }	    	
	    else if(include_er > 0)
	    	this_score = this_area_save/include_er;
	    else if(include_er == 0 && this_area_save > 0)
	    	this_score = 10000000;
	    else
	    	this_score = 0;

	    	
	    cout << "this_score = " << this_score << endl;
//	    cout << "max_score = " << max_score << endl;
	    
	    if(this_score < max_score)
	    {
//	    	cout << " < max_score !" << endl;
	    	if(include_er == 0)
	    		break;
	    	n_iter = tmp_exdc_set.begin();
//		    cout << "retrieve exdc: " << n_iter->second << endl;
		    exdc_set.insert(pair<double, string>(n_iter->first, n_iter->second));
		    tmp_exdc_set.erase(n_iter);    	
	    		
	    	if(flag_index < flag_index_limit)
	    		break;	    		
		    int k = 0;
	    	for(n_iter = exdc_set.begin(); n_iter != exdc_set.end(); n_iter++, k++)
	    	{
	    		if(k == flag_index)
	    		{
	    			exdc_set.erase(n_iter);
	    			flag_index -= 1;
	    			break;
	    		}
	    	}	    
//	    	cout << "remove exdc: " << n_iter->second << endl;
	    	tmp_exdc_set.insert(pair<double, string>(n_iter->first, n_iter->second));    		    
	    }
	    else if(this_score >= max_score)
	    {
//	    	cout << " >= max_score !" << endl;
	    	max_score = this_score;
	    	max_exdc_set = exdc_set;
	    	if(flag_index < flag_index_limit)
	    		break;
	    	int k = 0;
	    	for(n_iter = exdc_set.begin(); n_iter != exdc_set.end(); n_iter++, k++)
	    	{
	    		if(k == flag_index)
	    		{
	    			exdc_set.erase(n_iter);
	    			flag_index -= 1;
	    			break;
	    		}
	    	} 	    
//	    	cout << "remove exdc: " << n_iter->second << endl;
	    	tmp_exdc_set.insert(pair<double, string>(n_iter->first, n_iter->second));
	    }
	    
	    j++;
	    if(j == pow(2.0, count))
	    	break;
    }
    exdc_set = max_exdc_set;
    cout << "after sim_pick, exdc_set: " << endl;
    for(n_iter = exdc_set.begin(); n_iter != exdc_set.end(); n_iter++)
    {
    	if(n_iter->first == 0)
    		continue;
    	cout << n_iter->second << ": " << n_iter->first << endl;
    }  
    cout << endl;  	
    return max_score;
		        
}
*/



bool isInclude(string &dc_sim, string &dc_org)
{
	for(int i = 0; i < dc_sim.size(); i++)
	{
		if(dc_sim[i] != dc_org[i] && dc_sim[i] != '-')
			return 0;
	}
	return 1;
}


bool isIncludeVec(vector<string> &dc_set, string &dc)
{
	for(int i = 0; i < dc_set.size(); i++)
	{
		if(isInclude(dc_set[i], dc))
			return 1;
	}
	return 0;
}

bool isIncludeSet(set<string> &dc_set, string &dc)
{
	set<string>::iterator itrs;
	for(itrs = dc_set.begin(); itrs != dc_set.end(); itrs++)
	{
		string str = *itrs;
		if(isInclude(str, dc))
			return 1;
	}
	return 0;
}


int factorial(int x)
{
	if(x == 0)
		return 1;
	else
		return (x == 1? x : x * factorial(x-1));
}


int search_in_vec(vector<string> &org_pla, string str)
{
	for(int i = 0; i < org_pla.size(); i++)
		if(str == org_pla[i])
			return 1;
			
	return 0;
}


int search_array_int(int *vec, int num, int d)
{
	for(int i = 0; i < num; i++)
		if(vec[i] == d)
			return 1;
			
	return 0;
}
