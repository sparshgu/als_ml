#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <climits>
#include <vector>
#include <map>
#include <cmath>
#include <cassert>
#include <ctime>
#include <sys/timeb.h>
#include "head/queue.h"
#include "../cudd-2.5.0/cudd/cudd.h"
#include "../cudd-2.5.0/cudd/cuddInt.h"
#include "cudd/bnet.h"

using namespace std;

/*
functions in this file:
1. Graph::Graph(){}
2. Graph::~Graph()
3. void Graph::init(ifstream &fin, int &N, int &PO, int ite)
4. void Graph::addelement(int ni1, int ni2, int no, string ipattern1, string ipattern2, int &indEdge)
5. void Graph::traverse(int flag)
6. void Graph::topSort(vector<int> &sort_list)
7. double Graph::COP(int target, vector<int> &sort_list)
8. void Graph::WAA(vector<int> &sort_list)
9. double Graph::findmin(double p)
10. void Graph::sort_cutnodes()
11. void Graph::MFFC(vector<int> &sort_list)
*/

//Global variables and external variables


/*findmin()*/
double findmin(double p)
{
	if (p <= 0.5) return p;
	else return (1-p);
}

/*sort()*/
void asc_sort(BnetNetwork *net, vector<char*> &cutnodes)
{

	BnetNode *tmp1, *tmp2, *tmp3;
	char *temp1, *temp2;
	int i, j;
	for(i = 1; i < cutnodes.size(); i++) // Insertion Sort
	{
	    temp1 = cutnodes[i];
	    st_lookup(net->hash, temp1, &tmp1);
	    j = i - 1;
	    temp2 = cutnodes[j];
	    st_lookup(net->hash, temp2, &tmp2);
	    double sp_v0 = tmp1->p;
	    double sp_v1 = tmp2->p;
	    while(j >= 0 && (sp_v0 < sp_v1))
	    {	    
		    strcpy(cutnodes[j+1], cutnodes[j]);
		    j = j-1;		 
		    if(j >= 0) 
		    {
		    	st_lookup(net->hash, cutnodes[j], &tmp3);
		    	sp_v1 = tmp3->p;  
		    }
		    else
		    	break;
	    }
	    strcpy(cutnodes[j+1], tmp1->name);
	}

}


//permute()
int permute(vector<string> &dont_care, vector<string> &insig_string, vector<char*> &cutnodes)
{
	cout << endl << "Coming into permute!" << endl;
	map<string, int> insig_map;
	map<string, int>::iterator itrm_si;
	map<char*, int> insig_org_map;
	map<char*, int>::iterator itrm_ci;
	
	
	cout << "insig_string: " << endl;
	for(int i = 0; i < insig_string.size(); i++)
		cout << insig_string[i] << endl;
	cout << "cutnodes: " << endl;
	for(int i = 0; i < cutnodes.size(); i++)
		cout << cutnodes[i] << endl;
	
	int index;
	for(int i = 0; i < insig_string.size(); i++)
	{
		string str = insig_string[i];
		if(str == "(null)")
			return 1;
		insig_map.insert(pair<string, int>(str, i));
//		cout << str << ": " << i << endl;		
	}
	
	vector<int> sort_order;
	for(int i = 0; i < cutnodes.size(); i++)
	{
		string str(cutnodes[i]);
		itrm_si = insig_map.find(str);
		if(itrm_si == insig_map.end())
			return 1;
		int index = itrm_si->second;
		sort_order.push_back(index);
	}
	
	vector<string> dont_care_p1;
	for(int i = 0; i < dont_care.size(); i++)
	{
		string dc = dont_care[i];
		string dc_p1;
		for(int j = 0; j < dc.size(); j++)
		{
			int order = sort_order[j];
//			cout << "order = " << order << endl;
			char ch = dc[order];
			dc_p1.append(1, ch);
		}
		dont_care_p1.push_back(dc_p1);
	}

	dont_care = dont_care_p1;
	
	return 0;
}


//permute()
void permute_v2(vector<string> &dont_care, vector<char*> &insig_string, vector<char*> &cutnodes)
{
//	cout << endl << "Coming into permute!" << endl;
	map<string, int> insig_map;
	map<string, int>::iterator itrm_si;
	map<char*, int> insig_org_map;
	map<char*, int>::iterator itrm_ci;
	
	
	int index;
	for(int i = 0; i < insig_string.size(); i++)
	{
		char *name = insig_string[i];
		string str(name);
		if(str == "(null)")
			return;
		insig_map.insert(pair<string, int>(str, i));
//		cout << str << ": " << i << endl;		
	}
	
	vector<int> sort_order;
	for(int i = 0; i < cutnodes.size(); i++)
	{
		string str(cutnodes[i]);
		itrm_si = insig_map.find(str);
		int index = itrm_si->second;
		sort_order.push_back(index);
	}

	
	vector<string> dont_care_p1;
	for(int i = 0; i < dont_care.size(); i++)
	{
		string dc = dont_care[i];
		string dc_p1;
		for(int j = 0; j < dc.size(); j++)
		{
			int order = sort_order[j];
//			cout << "order = " << order << endl;
			char ch = dc[order];
			dc_p1.append(1, ch);
		}
		dont_care_p1.push_back(dc_p1);
	}

	dont_care = dont_care_p1;
}




/*topSort()*/
void topSort(BnetNetwork **net, vector<char*> &sort_list)
{
	BnetNode *nd, *auxnd, *tmp;
    queue sort_queue;
    map<char*, int> indegree_list; 
    map<char*, int>::iterator itrm_si;
    
    nd = (*net)->nodes;
    int i = 0;
    string name_str;
    while (nd != NULL) 
    {
    	indegree_list.insert(pair<char*, int>(nd->name, nd->ninp));
    	if(nd->ninp == 0)
    		sort_queue.push(nd->name);
		nd = nd->next;
    }
    
    while(!sort_queue.empty())
    {
    	sort_queue.traverse();
        char *pnode = sort_queue.pop(); 
        cout << endl << "pop: " << pnode << endl;
        st_lookup((*net)->hash, pnode, &tmp);
        sort_list.push_back(tmp->name);
        for(int i = 0; i < tmp->nfo; i++)
        {        	
            char *outnode = tmp->fanouts[i];
            st_lookup((*net)->hash, outnode, &auxnd);
            cout << "output: " << auxnd->name;
            itrm_si = indegree_list.find(auxnd->name);
            if(itrm_si == indegree_list.end())
            {
            	cout << " not exist!";
            	exit(1);
            }
            else 
            {
            	itrm_si->second = itrm_si->second - 1;
            	cout << ", num: " << itrm_si->second << endl;
            	if(itrm_si->second == 0)
            	{
                	sort_queue.push(auxnd->name);
                	cout << "node " << auxnd->name << " is enqueued. " << endl;
            	}
            }
        }         
    }     
}





/*6. update_ckt()*/
void update_ckt(BnetNetwork *net)
{
	cout << "update_ckt : " << endl;
	
	ifstream fin;
	ofstream fout;
	FILE *fp;
    string str;
	BnetNode *nd;
	BnetTabline *f;
	
	char com[100];
//	sprintf(com, "sis -t none -f ./script/sim_part_sim.rug > sis.txt");
//    system(com);
	

	//0. delete "_out" in ckt_sim.blif
    sprintf(com, "sed -i 's/_out/ /g' ./blif_files/ckt_sim.blif");
    system(com);
    

    //1. read ckt_sim.blif and store it in net   
    fp = fopen("./blif_files/ckt_sim.blif", "r");
    net = Bnet_ReadNetwork(fp);
    fclose(fp);

    
    //2. write net to ckt_sim_buffer.blif: add buffers to output nodes    
    fout.open("./blif_files/ckt_sim_buffer.blif", iostream::out);
    fout << ".model ckt_sim" << endl;
    fout << ".inputs ";
    for(int i = 0; i < net->npis; i++)
        fout << net->inputs[i] <<  " ";
    fout  << endl;    
    fout << ".outputs ";
    for(int i = 0; i < net->npos; i++)
    {
        fout << net->outputs[i] <<  "_out ";
    }
    fout  << endl;        
    nd = net->nodes;
    while(nd != NULL)
    {
        if(nd->type == BNET_INPUT_NODE)
        {
        	nd = nd->next;
            continue;
        }
        fout << ".names ";
        for(int i = 0; i < nd->ninp; i++)
            fout << nd->inputs[i] << " ";
        fout << nd->name << endl;
        f = nd->f;
        while(f != NULL)
        {
        	if (f->values != NULL) 
		    	fout << f->values << " " << 1 - nd->polarity << endl;
			else 
			    fout <<  1 - nd->polarity << endl;
        	f = f->next;
        }
        nd = nd->next;
    }    
    fout  << endl;
    for(int i = 0; i < net->npos; i++)
    {
    	fout << ".names " << net->outputs[i] <<  " " << net->outputs[i] << "_out" << endl;
    	fout << "1 1" << endl;
    }        
    fout << ".end" << endl;
    fout.close();
    
    Bnet_FreeNetwork(net);
           
    sprintf(com, "rm -rf ckt_sim.blif");
    system(com);
	sprintf(com, "cp ./blif_files/ckt_sim_buffer.blif ./blif_files/ckt_sim.blif");
    system(com);
        
    //read ckt_sim_buffer and store it in net    
    fp = fopen("./blif_files/ckt_sim.blif", "r");
    net = Bnet_ReadNetwork(fp);
    fclose(fp);
    
/*    cout << "in update: " << endl;
    Bnet_PrintNetwork(net);
*/    
//    net = net_new;
  
}


