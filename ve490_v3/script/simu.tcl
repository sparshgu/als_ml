rm -rf myLib_simu
vlib myLib_simu
vmap work myLib_simu

vlog ./verilog_files/ckt_sim_simu.v
vlog ./verilog_files/ckt_tb_simu.v

vsim -novopt -c ckt_tb_simu

run -all
quit

