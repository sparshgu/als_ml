rm -rf myLib
vlib myLib
vmap work myLib


vlog ./verilog_files/ckt_org.v 
vlog ./verilog_files/ckt_sim.v
vlog ./verilog_files/ckt_tb.v


vsim -novopt -c ckt_tb


run -all
quit

