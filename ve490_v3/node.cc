#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <climits>
#include <vector>
#include <deque>
#include <list>
#include <cassert>
#include "head/graph.h"
#include "head/edge.h"
#include "head/node.h"

using namespace std;

Node::Node(){
	index=INT_MIN;
	isInput = 0;
	isOutput = 0;
	gate.type=0;
	gate.p=0;
	gate.rp=0;
	explored=0;
	ifcross=0;
}
Node::Node(int ind){
	index=ind;
	visit = 0;
	isInput = 0;
	isOutput = 0;
	gate.type=0;
	gate.p=0;
	gate.rp=0;
	explored=0;
	ifcross=0;
}

