#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <climits>
#include <vector>
#include <deque>
#include <list>
#include <cmath>
#include <cassert>
#include "head/graph.h"
#include "head/node.h"
#include "head/edge.h"
#include "head/HashTable.h"
#include "head/helper.h"
#include "head/stack.h"

using namespace std;



/*int read_sis_result( )
{
//    cout << "In read_sis_result: " << endl;
    ifstream fin;
    string str;
    string filename = "sis.txt";
    fin.open(filename.c_str(), iostream::in);
    int num[2];
    int i = 0;
    cout << "area result: " << endl;
    while(getline(fin, str))
    {
    	cout << str << endl;
        if(i >= 2)
            break;
        istringstream ss(str);
        string s;
        ss >> s;
//		if(s == "Total")
//		if(s == "lits(sop)=")
		if(s == "lits(fac)=")
	    {
			while(ss >> s);
//			cout << "area: " << s << endl;
			num[i] = atoi(s.c_str());
        	i++;
		}
    }
    if(i == 1)
        num[1] = 0;
    int area_save = num[0] - num[1];
    cout << "num[0] = " << num[0] << ", num[1] = " << num[1] << endl;
    fin.close();
    return area_save; 
}   
*/


int read_espresso_result(int num_lit_org)
{
    ifstream fin;
    string str;
    string filename = "./pla_files/bigNode_sim.pla";
    fin.open(filename.c_str(), iostream::in);
    int num[2];
    int i = 0;
//    cout << "area result: " << endl;
    int num_lit_sim = 0;
    int flag_start = 0;
    while(getline(fin, str))
    {
//    	cout << str << endl;
        istringstream ss(str);
        string s, s1;
        ss >> s;
        if(s[0] == '.')
        	continue;
	    ss >> s1;
	    if(s1 == "2")
	    	break;
		for(int i = 0; i < s.size(); i++)
		{
			if(s[i] != '-')
				num_lit_sim++;
		}
    }

    int area_save = num_lit_org - num_lit_sim;
    cout << "num_lit_org = " << num_lit_org << ", num_lit_sim = " << num_lit_sim << endl;
    fin.close();
    return area_save; 
}   


//read lits(fac)
int read_sis_result(int &area_save)
{
//    cout << "In read_sis_result: " << endl;
    ifstream fin;
    string str;
    string filename = "sis.txt";
    fin.open(filename.c_str(), iostream::in);
    int num[2];
    int i = 0;
//    cout << "area result: " << endl;
    while(getline(fin, str))
    {
//    	cout << str << endl;
        if(i >= 2)
            break;
        int flag = 0;
        istringstream ss(str);
        istringstream ss1(str);
        string s;
        int num_s = 0;
        while(ss >> s)
        	num_s++;
        if(num_s == 4)
	    {
	        while(ss1 >> s)
				if(s == "lits(fac)=")
			    {
			    	flag = 1;
					break;				
				}
			if(flag == 1)
			{
				ss1 >> s;
				num[i] = atoi(s.c_str());
			    i++;
			}
		}
		else if(num_s == 2)
		{
			while(ss1 >> s);
			string::size_type n = 0;
			n = s.find("lits(fac)=");
			int size_num = s.size() - 10; 
			string num_sim_lit = s.substr(n+10, size_num);
			num[i] = atoi(num_sim_lit.c_str());
			i++;
		}
    }
    if(i == 1)
        num[1] = 0;
    area_save = num[0] - num[1];
//    cout << "num[0] = " << num[0] << ", num[1] = " << num[1] << endl;
    fin.close();
    
    if(num[1] == 0)
    	return 1;
    else
    	return 0; 
}    
   
   
//read lits(fac)
int read_num_lit()
{
    ifstream fin;
    string str, s;
    string filename = "./output/lit.txt";
    fin.open(filename.c_str(), iostream::in);
    int num;
//    cout << "lit result: " << endl;
    while(getline(fin, str))
    {
//    	cout << str << endl;
    	if(str.find("lits(fac)=") == string::npos)
    		continue;
        int flag = 0;
        istringstream ss(str);
        istringstream ss1(str);
        int num_s = 0;
        while(ss >> s)
        	num_s++;
        if(num_s == 4)
	    {
	        while(ss1 >> s)
				if(s == "lits(fac)=")
			    {
			    	flag = 1;
					break;				
				}
			if(flag == 1)
			{
				ss1 >> s;
				num = atoi(s.c_str());
			}
		}
		else if(num_s == 2)
		{
			while(ss1 >> s);
			string::size_type n = 0;
			n = s.find("lits(fac)=");
			int size_num = s.size() - 10; 
			string num_sim_lit = s.substr(n+10, size_num);
			num = atoi(num_sim_lit.c_str());
		}
    }
	
	return num;

}
   
   
   

/*read_mvsis_result*/
void read_mvsis_result(vector<string> &dont_care,  vector<string> &insig_string)
{
//	cout << endl << "Coming into read_mvsis_result!" << endl;
	ifstream fin;
    string str;
    string filename = "mvsis.txt";
    fin.open(filename.c_str(), iostream::in);
	vector<string> varVerStr, varHorStr;
	int nVarsVer = 0, nVarsHor = 0;
	int flag_start = 0, first_doll = 0;
	while(getline(fin, str))
	{
//		cout << str << endl;
		if(str.empty())
			continue;
//		cout << "flag_start = " << flag_start << endl;
//		cout << "str = " << str << endl;
		istringstream ss(str);
		string s;
		ss >> s;
		if(s == "$$")
		{		
			first_doll++;
			if(first_doll == 1)
			{				
				int flag = 0;
				while(ss >> s)
				{
					if(flag == 0)
					{	
						if(s != "\\")
							varVerStr.push_back(s);
						else
							flag = 1;
					}
					else
						varHorStr.push_back(s);
				}
				nVarsVer = varVerStr.size();
				nVarsHor = varHorStr.size();
			}		
		}
		
		if(first_doll == 1)
		{
			string::size_type position = str.find('|');
			if(position == string::npos)
				continue;
			else
			{
				//cout << "| is here!" << endl;
				string ver = s;
				vector<int> indexHor;
				int ind = 0;
				while(ss >> s)
				{
					if(s == "1")
						indexHor.push_back(ind);
					if(s == "0" || s == "1" || s == "-")
						ind++;
				}
				for(int i = 0; i < indexHor.size(); i++)
				{
					string hor = int2grey(indexHor[i], nVarsHor);
					string mint = ver;
					mint.append(hor);
				}
			}
		}				
		else if(first_doll == 2)
		{
			string::size_type position = str.find('|');
			//cout << "position = " << position << endl;
			if(position == string::npos)
				continue;
			else
			{
				//cout << "| is here!" << endl;
				string ver = s;
				//cout << "ver = " << ver << endl;
				vector<int> indexHor;
				int ind = 0;
				while(ss >> s)
				{
					if(s == "-")
						indexHor.push_back(ind);
					if(s== "0" || s == "1" || s == "-")
						ind++;
				}
				for(int i = 0; i < indexHor.size(); i++)
				{
					string hor = int2grey(indexHor[i], nVarsHor);
					string exdc = ver;
					exdc.append(hor);
					dont_care.push_back(exdc);
//					cout << "exdc = " << exdc << endl;
				}
			}
		}
	}
	fin.close();

	insig_string = varVerStr;
	vector<string>::iterator it = insig_string.end();
	insig_string.insert(it, varHorStr.begin(), varHorStr.end());
/*	cout << "insig_string: " << endl;
	for(int i = 0; i < insig_string.size(); i++)
		cout << insig_string[i] << " ";
	cout << endl;	
*/
}


void read_factor(vector<string> &lit_unit)
{
	ifstream fin;
	fin.open("factor.txt", ios::in);
	string str, s;
	
	string lit_set;
	int start_store = 0;
	int flag_start = 0;
	int line = 0;
	int flag_quit = 0;
	stack factor_stack(200);
	cout << "factor.txt: " << endl;
	while(getline(fin, str))
	{
		istringstream ss(str);
//		cout << "str = " << str << endl;
		if(factor_stack.empty())
			lit_set.clear();
		if(line == 0)
			flag_start = 0;
		else
			flag_start = 1;
		while(ss >> s)
		{		
			if(s == ".model")
			{
				flag_quit = 1;
				break;
			}	
			if(s == " ")
				continue;
			if(line == 0 && s == "=")
				flag_start = 1;			
			if(flag_start)
			{
//				cout << "s: " << s << endl;
				if(s == "+" && factor_stack.empty())
				{					
//					cout << "lit_set: " << lit_set << endl;	
					if(!lit_set.empty())				
						lit_unit.push_back(lit_set);
					lit_set.clear();
				}
				else
				{
					string lit = s;
					if(lit[0] == '(')
						factor_stack.push(1);
					else if(lit[lit.size()-1] == ')')
						int right = factor_stack.pop();
					if(s == "=")
						continue;
					lit_set.append(s);
					lit_set.append(" ");
//					cout << "lit_set: " << lit_set << endl;	
				}
			}
		}
		if(!lit_set.empty() && factor_stack.empty())	
			lit_unit.push_back(lit_set);
		line++;
		if(flag_quit)
			break;
	}
	
	cout << "lit_unit: " << endl;
	for(int i = 0; i < lit_unit.size(); i++)
		cout << lit_unit[i] << endl;
	cout << endl;
}	



void read_factor_v2(string &factor)
{
	ifstream fin;
	fin.open("factor.txt", ios::in);
	string str, s;
	
	int flag_start = 0;
	int line = 0;
	int flag_quit = 0;
	while(getline(fin, str))
	{
		istringstream ss(str);
		if(line == 0)
			flag_start = 0;
		else
			flag_start = 1;
		while(ss >> s)
		{		
			if(s == ".model")
			{
				flag_quit = 1;
				break;
			}	
			if(s == " ")
				continue;
			if(line == 0 && s == "=")
				flag_start = 1;			
			if(flag_start)
			{
				if(s == "=")
					continue;
				factor.append(s);
			}
		}
		line++;
		if(flag_quit)
			break;
	}
	
	cout << "factor = " << factor << endl;
}
