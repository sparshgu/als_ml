

#include "ApproxLogicSynthesis.hpp"
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <string>
using namespace std;

ApproxLogicSynthesis::ApproxLogicSynthesis(){}

void ApproxLogicSynthesis::PythonCall(){
    char cwd[1024];
    if (getcwd(cwd, sizeof(cwd)) != NULL) {
        fprintf(stdout, "Current working dir: %s\n", cwd);
        char Command[10000];
        sprintf(Command, "python %s/../ml_engine/main.py -b=%s/../ml_engine/%s -e=%d", cwd,cwd,
                fileName.c_str(),100);
        char buf[128];
        FILE *fp;
        if ((fp = popen(Command, "r")) == NULL) {
            printf("Error opening pipe!\n");        }
        while (fgets(buf, 128, fp) != NULL) {
            // Do whatever you want here...
            printf("OUTPUT: %s", buf);
        }
        if (pclose(fp)) {
            printf("Command not found or exited with error status\n");
        }
    }else{
        
    }
}

void ApproxLogicSynthesis::welcome(){
   cout << "Now you have entered the section of Approximate Synthesis"<<endl;
}

ApproxLogicSynthesis::ApproxLogicSynthesis(std::string file_Name) { 
    fileName = file_Name;
}

   
