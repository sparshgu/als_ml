
#ifndef ApproxLogicSynthesis_hpp
#define ApproxLogicSynthesis_hpp

#include <stdio.h>
#include <iostream>
#include "LogicSynthesis.hpp"
using namespace std;

class ApproxLogicSynthesis:public LogicSynthesis{
private:
    std::string fileName;
public:
    ApproxLogicSynthesis();
    ApproxLogicSynthesis(std::string file_Name);
    void PythonCall();
    void welcome();
};
#endif /* ApproxLogicSynthesis_hpp */
