

#ifndef LogicSynthesis_hpp
#define LogicSynthesis_hpp

#include <stdio.h>
#include <iostream>
using namespace std;

class LogicSynthesis{
private:
    std::string fileName;
public:
    LogicSynthesis();
    virtual void welcome();
};
#endif /* LogicSynthesis_hpp */
