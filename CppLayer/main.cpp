

#include <iostream>
#include "LogicSynthesis.hpp"
#include "ApproxLogicSynthesis.hpp"
using namespace std;
int main() {
    ApproxLogicSynthesis o1("temp.blif");
    int select;
    cout << "Enter 0 for Complete logic Synthesis, and 1 for Approximate Logic Synthesis"<<endl;
    cin>> select;
    if(select == 0)
    {
      cout << "Selection success, chosen 0"<<endl;
      LogicSynthesis l;
      l.welcome();  
    }
    if(select == 1)
    {
      cout << "Selection success, chosen 1"<< endl;
      o1.welcome();
      cout << "Now Calling Python Script" << endl;
      cout << "Script Executed"<< endl;
      o1.PythonCall();
    }
    
    return 0;
}
